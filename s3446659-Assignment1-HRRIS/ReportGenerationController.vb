﻿Option Explicit On
Option Strict On

Imports System.IO
Imports System.Data.OleDb


' Name:        ReportGenerationController.vb
' Description: Class to control database, publish to HTML
' Author:      Do Khanh Hoang
' Date:        28/04/2017



Public Class ReportGenerationController
	'Connect to database
	Public Const CONNECTION_STRING As String =
	 "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=HRRISDB.accdb"

	'Call CustomerID
	Public Function CallCustomerID() As List(Of Hashtable)

		Dim oConnection As OleDbConnection = New OleDbConnection(CONNECTION_STRING)
		Dim lsData As New List(Of Hashtable)

		Try
			Debug.Print("Connection string: " & oConnection.ConnectionString)

			oConnection.Open()
			Dim oCommand As OleDbCommand = New OleDbCommand
			oCommand.Connection = oConnection

			oCommand.CommandText =
				"SELECT customer_id FROM customer ORDER BY customer_id;"

			oCommand.Prepare()
			Dim oDataReader = oCommand.ExecuteReader()

			Dim htTempData As Hashtable
			Do While oDataReader.Read() = True
				htTempData = New Hashtable
				htTempData("CustomerID") = CStr(oDataReader("customer_id"))
				lsData.Add(htTempData)
			Loop

		Catch ex As Exception
			Debug.Print("ERROR: " & ex.Message)
		Finally
			oConnection.Close()
		End Try

		Return lsData

	End Function

	'Call room_number
	Public Function CallRoomNumber() As List(Of Hashtable)

		Dim oConnection As OleDbConnection = New OleDbConnection(CONNECTION_STRING)
		Dim lsData1 As New List(Of Hashtable)

		Try
			Debug.Print("Connection string: " & oConnection.ConnectionString)

			oConnection.Open()
			Dim oCommand As OleDbCommand = New OleDbCommand
			oCommand.Connection = oConnection

			oCommand.CommandText =
				"SELECT room_number FROM room ORDER BY room_number;"

			oCommand.Prepare()
			Dim oDataReader = oCommand.ExecuteReader()

			Dim htTempData As Hashtable
			Do While oDataReader.Read() = True
				htTempData = New Hashtable
				htTempData("RoomNumber") = CInt(oDataReader("room_number"))
				lsData1.Add(htTempData)
			Loop

		Catch ex As Exception
			Debug.Print("ERROR: " & ex.Message)
		Finally
			oConnection.Close()
		End Try

		Return lsData1

	End Function
	'SQL: query to take data based on given parameter
	'First Report: customer_id, booking_date, number of day
	Public Function firstReport(sCustomerID As String) As List(Of Hashtable)

		Dim oConnection As OleDbConnection = New OleDbConnection(CONNECTION_STRING)
		Dim lsData As New List(Of Hashtable)

		Try
			Debug.Print("Connection string: " & oConnection.ConnectionString)

			oConnection.Open()
			Dim oCommand As OleDbCommand = New OleDbCommand
			oCommand.Connection = oConnection

			oCommand.CommandText =
				"SELECT TOP 1 booking_date, customer_id, num_days FROM booking WHERE customer_id=? ORDER BY booking_date DESC;"
			oCommand.Parameters.Add("customer_id", OleDbType.VarChar, 8)
			oCommand.Parameters("customer_id").Value = CStr(sCustomerID)
			oCommand.Prepare()
			Dim oDataReader = oCommand.ExecuteReader()

			Dim htTempData As Hashtable
			Do While oDataReader.Read() = True
				htTempData = New Hashtable
				htTempData("CustomerID") = CStr(oDataReader("customer_id"))
				htTempData("Date") = CDate(oDataReader("booking_date"))
				htTempData("NumberOfDays") = CInt(oDataReader("num_days"))
				lsData.Add(htTempData)
			Loop

		Catch ex As Exception
			Debug.Print("ERROR: " & ex.Message)
		Finally
			oConnection.Close()
		End Try

		Return lsData

	End Function

	'Second Report: room number, booking_date, total price
	Public Function secondReport(sRoomNumber As String) As List(Of Hashtable)

		Dim oConnection As OleDbConnection = New OleDbConnection(CONNECTION_STRING)
		Dim lsData As New List(Of Hashtable)

		Try
			Debug.Print("Connection string: " & oConnection.ConnectionString)

			oConnection.Open()
			Dim oCommand As OleDbCommand = New OleDbCommand
			oCommand.Connection = oConnection

			oCommand.CommandText =
				"SELECT TOP 1 booking_date, booking.room_id, room_number, total_price FROM booking, room WHERE room_number=? AND room.room_id = booking.room_id ORDER BY booking_date DESC; "
			oCommand.Parameters.Add("room_number", OleDbType.Integer, 8)
			oCommand.Parameters("room_number").Value = CInt(sRoomNumber)
			oCommand.Prepare()
			Dim oDataReader = oCommand.ExecuteReader()

			Dim htTempData As Hashtable
			Do While oDataReader.Read() = True
				htTempData = New Hashtable
				htTempData("RoomNumber") = CStr(oDataReader("room_number"))
				htTempData("Date") = CDate(oDataReader("booking_date"))
				htTempData("TotalPrice") = CInt(oDataReader("total_price"))
				lsData.Add(htTempData)
			Loop

		Catch ex As Exception
			Debug.Print("ERROR: " & ex.Message)
		Finally
			oConnection.Close()
		End Try

		Return lsData

	End Function

	'Third Report: customer_id, number of room booked
	Public Function thirdReport(sYear As String, sMonth As String) As List(Of Hashtable)

		Dim oConnection As OleDbConnection = New OleDbConnection(CONNECTION_STRING)
		Dim lsData As New List(Of Hashtable)

		Try
			Debug.Print("Connection string: " & oConnection.ConnectionString)

			oConnection.Open()
			Dim oCommand As OleDbCommand = New OleDbCommand
			oCommand.Connection = oConnection

			oCommand.CommandText =
				"SELECT customer_id, COUNT(room_id) AS number_room_booked FROM booking WHERE DatePart('yyyy', booking_date)=? AND DatePart('m', booking_date)=? GROUP BY customer_id;"
			oCommand.Parameters.Add("DatePart('yyyy', booking_date)", OleDbType.Integer, 8)
			oCommand.Parameters("DatePart('yyyy', booking_date)").Value = CStr(sYear)
			oCommand.Parameters.Add("DatePart('m', booking_date)", OleDbType.Integer, 8)
			oCommand.Parameters("DatePart('m', booking_date)").Value = CStr(sMonth)
			oCommand.Prepare()
			Dim oDataReader = oCommand.ExecuteReader()

			Dim htTempData As Hashtable
			Do While oDataReader.Read() = True
				htTempData = New Hashtable
				htTempData("CustomerID") = CStr(oDataReader("customer_id"))
				htTempData("NumberOfRoomBooked") = CInt(oDataReader("number_room_booked"))
				lsData.Add(htTempData)
			Loop

		Catch ex As Exception
			Debug.Print("ERROR: " & ex.Message)
		Finally
			oConnection.Close()
		End Try

		Return lsData

	End Function
	'Fourth Report: booking_id, room_id, customer_id, number of days, number of guests, total price
	Public Function fourthReport(sYear As String, sMonth As String) As List(Of Hashtable)

		Dim oConnection As OleDbConnection = New OleDbConnection(CONNECTION_STRING)
		Dim lsData As New List(Of Hashtable)

		Try
			Debug.Print("Connection string: " & oConnection.ConnectionString)

			oConnection.Open()
			Dim oCommand As OleDbCommand = New OleDbCommand
			oCommand.Connection = oConnection

			oCommand.CommandText =
			"SELECT booking_id, booking_date, room_id, customer_id, num_days, num_guests, total_price FROM booking WHERE DatePart('yyyy', booking_date)=? AND DatePart('m', booking_date)=? ORDER BY booking_id;"
			oCommand.Parameters.Add("DatePart('yyyy', booking_date)", OleDbType.Integer, 8)
			oCommand.Parameters("DatePart('yyyy', booking_date)").Value = CStr(sYear)
			oCommand.Parameters.Add("DatePart('m', booking_date)", OleDbType.Integer, 8)
			oCommand.Parameters("DatePart('m', booking_date)").Value = CStr(sMonth)
			oCommand.Prepare()
			Dim oDataReader = oCommand.ExecuteReader()

			Dim htTempData As Hashtable
			Do While oDataReader.Read() = True
				htTempData = New Hashtable
				htTempData("BookingID") = CStr(oDataReader("booking_id"))
				htTempData("Date") = CDate(oDataReader("booking_date"))
				htTempData("RoomID") = CStr(oDataReader("room_id"))
				htTempData("CustomerID") = CStr(oDataReader("customer_id"))
				htTempData("NumberOfDays") = CInt(oDataReader("num_days"))
				htTempData("NumberOfGuests") = CInt(oDataReader("num_guests"))
				htTempData("TotalPrice") = CInt(oDataReader("total_price"))
				lsData.Add(htTempData)
			Loop

		Catch ex As Exception
			Debug.Print("ERROR: " & ex.Message)
		Finally
			oConnection.Close()
		End Try

		Return lsData

	End Function
	'Fifth Report: customer_id, checkin date
	Public Function fifthReport(sYear As String, sMonth As String) As List(Of Hashtable)

		Dim oConnection As OleDbConnection = New OleDbConnection(CONNECTION_STRING)
		Dim lsData As New List(Of Hashtable)

		Try
			Debug.Print("Connection string: " & oConnection.ConnectionString)

			oConnection.Open()
			Dim oCommand As OleDbCommand = New OleDbCommand
			oCommand.Connection = oConnection

			oCommand.CommandText =
			"SELECT customer_id, checkin_date FROM booking WHERE DatePart('yyyy', checkin_date)=? AND DatePart('m', checkin_date)=? ORDER BY checkin_date;"
			oCommand.Parameters.Add("DatePart('yyyy', booking_date)", OleDbType.Integer, 8)
			oCommand.Parameters("DatePart('yyyy', booking_date)").Value = CInt(sYear)
			oCommand.Parameters.Add("DatePart('m', booking_date)", OleDbType.Integer, 8)
			oCommand.Parameters("DatePart('m', booking_date)").Value = CInt(sMonth)
			oCommand.Prepare()
			Dim oDataReader = oCommand.ExecuteReader()

			Dim htTempData As Hashtable
			Do While oDataReader.Read() = True
				htTempData = New Hashtable
				htTempData("CustomerID") = CStr(oDataReader("customer_id"))
				htTempData("CheckinDate") = CDate(oDataReader("checkin_date"))
				lsData.Add(htTempData)
			Loop

		Catch ex As Exception
			Debug.Print("ERROR: " & ex.Message)
		Finally
			oConnection.Close()
		End Try

		Return lsData

	End Function
	'Sixth Report: bookings_id, room_id, booking_date
	Public Function sixthReport(sYear As String, sMonth As String, sRoom As String) As List(Of Hashtable)

		Dim oConnection As OleDbConnection = New OleDbConnection(CONNECTION_STRING)
		Dim lsData As New List(Of Hashtable)

		Try
			Debug.Print("Connection string: " & oConnection.ConnectionString)

			oConnection.Open()
			Dim oCommand As OleDbCommand = New OleDbCommand
			oCommand.Connection = oConnection

			oCommand.CommandText =
			"SELECT booking_id, room_number, booking_date FROM booking, room WHERE DatePart('yyyy', booking_date)=? AND DatePart('m', booking_date)=? AND room_number=? AND room.room_id = booking.room_id ORDER BY room_number;"
			oCommand.Parameters.Add("DatePart('yyyy', booking_date)", OleDbType.Integer, 8)
			oCommand.Parameters("DatePart('yyyy', booking_date)").Value = CInt(sYear)
			oCommand.Parameters.Add("DatePart('m', booking_date)", OleDbType.Integer, 8)
			oCommand.Parameters("DatePart('m', booking_date)").Value = CInt(sMonth)
			oCommand.Parameters.Add("room_number", OleDbType.Integer, 8)
			oCommand.Parameters("room_number").Value = CInt(sRoom)
			oCommand.Prepare()
			Dim oDataReader = oCommand.ExecuteReader()

			Dim htTempData As Hashtable
			Do While oDataReader.Read() = True
				htTempData = New Hashtable
				htTempData("BookingID") = CStr(oDataReader("booking_id"))
				htTempData("RoomNumber") = CInt(oDataReader("room_number"))
				htTempData("Date") = CDate(oDataReader("booking_date"))
				lsData.Add(htTempData)
			Loop

		Catch ex As Exception
			Debug.Print("ERROR: " & ex.Message)
		Finally
			oConnection.Close()
		End Try

		Return lsData

	End Function
	'Create HTML page
	'First Report
	Private Function generateReport01(ByVal lsData As List(Of Hashtable), ByVal sReportTitle As String) _
As String

		Debug.Print("GenerateReport01")

		Dim sReportContent As String

		' 1. Generate the start of the HTML file
		Dim sDoctype As String = "<!DOCTYPE html>"
		Dim sHtmlStartTag As String = "<html lang=""en"">"
		Dim sHeadTitle As String = "<head><title>" & sReportTitle & "</title></head>"
		Dim sBodyStartTag As String = "<body>"
		Dim sReportHeading As String = "<h1>" & sReportTitle & "</h1>"
		sReportContent = sDoctype & Environment.NewLine & sHtmlStartTag & Environment.NewLine _
		  & sHeadTitle & Environment.NewLine & sBodyStartTag & Environment.NewLine & sReportHeading & Environment.NewLine

		' 2. Generate the customer table and its rows
		Dim sTable = generateTable1(lsData)
		sReportContent &= sTable & Environment.NewLine

		' 3. Generate the end of the HTML file
		Dim sBodyEndTag As String = "</body>"
		Dim sHTMLEndTag As String = "</html>"
		sReportContent &= sBodyEndTag & Environment.NewLine & sHTMLEndTag

		Return sReportContent

	End Function
	'Create table 1
	Private Function generateTable1(ByVal lsData As List(Of Hashtable)) As String
		' Generate the start of the table
		Dim sTable = "<table border=""1"">" & Environment.NewLine
		Dim htSample As Hashtable = lsData.Item(0)
		Dim lsKeys As List(Of String) = New List(Of String)
		lsKeys.Add("CustomerID")
		lsKeys.Add("Date")
		lsKeys.Add("NumberOfDays")

		' Generate the header row
		Dim sHeaderRow = "<tr>" & Environment.NewLine
		For Each key In lsKeys
			sHeaderRow &= "<th>" & CStr(key) & "</th>" & Environment.NewLine
		Next
		sHeaderRow &= "</tr>" & Environment.NewLine
		Debug.Print("sHeaderRow: " & sHeaderRow)
		sTable &= sHeaderRow

		' Generate the table rows
		For Each record In lsData
			Dim customer As Hashtable = record
			Dim sTableRow = "<tr>" & Environment.NewLine
			For Each key In lsKeys
				sTableRow &= "<td>" & CStr(customer(key)) & "</td>" & Environment.NewLine
			Next
			sTableRow &= "</tr>" & Environment.NewLine
			Debug.Print("sTableRow: " & sTableRow)
			sTable &= sTableRow
		Next

		' Generate the end of the table
		sTable &= "</table>" & Environment.NewLine

		Return sTable
	End Function
	'Create Report 1
	Public Sub createReport01(sCustomerID As String)
		Debug.Print("CreateReport01")

		Dim lsData = firstReport(sCustomerID)
		Dim sReportTitle = "Report 01"
		Dim sReportContent = generateReport01(lsData, sReportTitle)
		Dim sReportFilename = "Report-01.html"
		saveReport(sReportContent, sReportFilename)
		Dim sParam As String = """" & Application.StartupPath & "\" & sReportFilename & """"
		Debug.Print("sParam: " & sParam)
		System.Diagnostics.Process.Start(sParam)
	End Sub

	'Second Report
	Private Function generateReport02(ByVal lsData As List(Of Hashtable), ByVal sReportTitle As String) _
As String

		Debug.Print("GenerateReport02")

		Dim sReportContent As String

		' 1. Generate the start of the HTML file
		Dim sDoctype As String = "<!DOCTYPE html>"
		Dim sHtmlStartTag As String = "<html lang=""en"">"
		Dim sHeadTitle As String = "<head><title>" & sReportTitle & "</title></head>"
		Dim sBodyStartTag As String = "<body>"
		Dim sReportHeading As String = "<h1>" & sReportTitle & "</h1>"
		sReportContent = sDoctype & Environment.NewLine & sHtmlStartTag & Environment.NewLine _
		  & sHeadTitle & Environment.NewLine & sBodyStartTag & Environment.NewLine & sReportHeading & Environment.NewLine

		' 2. Generate the customer table and its rows
		Dim sTable2 = generateTable2(lsData)
		sReportContent &= sTable2 & Environment.NewLine

		' 3. Generate the end of the HTML file
		Dim sBodyEndTag As String = "</body>"
		Dim sHTMLEndTag As String = "</html>"
		sReportContent &= sBodyEndTag & Environment.NewLine & sHTMLEndTag

		Return sReportContent

	End Function
	'Create table 2
	Private Function generateTable2(ByVal lsData As List(Of Hashtable)) As String
		' Generate the start of the table
		Dim sTable = "<table border=""1"">" & Environment.NewLine
		Dim htSample As Hashtable = lsData.Item(0)
		Dim lsKeys As List(Of String) = New List(Of String)
		lsKeys.Add("RoomNumber")
		lsKeys.Add("Date")
		lsKeys.Add("TotalPrice")

		' Generate the header row
		Dim sHeaderRow = "<tr>" & Environment.NewLine
		For Each key In lsKeys
			sHeaderRow &= "<th>" & CStr(key) & "</th>" & Environment.NewLine
		Next
		sHeaderRow &= "</tr>" & Environment.NewLine
		Debug.Print("sHeaderRow: " & sHeaderRow)
		sTable &= sHeaderRow

		' Generate the table rows
		For Each record In lsData
			Dim customer As Hashtable = record
			Dim sTableRow = "<tr>" & Environment.NewLine
			For Each key In lsKeys
				sTableRow &= "<td>" & CStr(customer(key)) & "</td>" & Environment.NewLine
			Next
			sTableRow &= "</tr>" & Environment.NewLine
			Debug.Print("sTableRow: " & sTableRow)
			sTable &= sTableRow
		Next

		' Generate the end of the table
		sTable &= "</table>" & Environment.NewLine

		Return sTable
	End Function
	'Create report 2
	Public Sub createReport02(sRoomNumber As String)
		Debug.Print("CreateReport02")

		Dim lsData = secondReport(sRoomNumber)
		Dim sReportTitle = "Report 02"
		Dim sReportContent = generateReport02(lsData, sReportTitle)
		Dim sReportFilename = "Report-02.html"
		saveReport(sReportContent, sReportFilename)
		Dim sParam As String = """" & Application.StartupPath & "\" & sReportFilename & """"
		Debug.Print("sParam: " & sParam)
		System.Diagnostics.Process.Start(sParam)
	End Sub
	'Third report
	Private Function generateReport03(ByVal lsData As List(Of Hashtable), ByVal sReportTitle As String) _
As String

		Debug.Print("GenerateReport03")

		Dim sReportContent As String

		' 1. Generate the start of the HTML file
		Dim sDoctype As String = "<!DOCTYPE html>"
		Dim sHtmlStartTag As String = "<html lang=""en"">"
		Dim sHeadTitle As String = "<head><title>" & sReportTitle & "</title></head>"
		Dim sBodyStartTag As String = "<body>"
		Dim sReportHeading As String = "<h1>" & sReportTitle & "</h1>"
		sReportContent = sDoctype & Environment.NewLine & sHtmlStartTag & Environment.NewLine _
		  & sHeadTitle & Environment.NewLine & sBodyStartTag & Environment.NewLine & sReportHeading & Environment.NewLine

		' 2. Generate the customer table and its rows
		Dim sTable3 = generateTable3(lsData)
		sReportContent &= sTable3 & Environment.NewLine

		' 3. Generate the end of the HTML file
		Dim sBodyEndTag As String = "</body>"
		Dim sHTMLEndTag As String = "</html>"
		sReportContent &= sBodyEndTag & Environment.NewLine & sHTMLEndTag

		Return sReportContent

	End Function
	'Create table 3
	Private Function generateTable3(ByVal lsData As List(Of Hashtable)) As String
		' Generate the start of the table
		Dim sTable = "<table border=""1"">" & Environment.NewLine
		Dim htSample As Hashtable = lsData.Item(0)
		Dim lsKeys As List(Of String) = New List(Of String)
		lsKeys.Add("CustomerID")
		lsKeys.Add("NumberOfRoomBooked")

		' Generate the header row
		Dim sHeaderRow = "<tr>" & Environment.NewLine
		For Each key In lsKeys
			sHeaderRow &= "<th>" & CStr(key) & "</th>" & Environment.NewLine
		Next
		sHeaderRow &= "</tr>" & Environment.NewLine
		Debug.Print("sHeaderRow: " & sHeaderRow)
		sTable &= sHeaderRow

		' Generate the table rows
		For Each record In lsData
			Dim customer As Hashtable = record
			Dim sTableRow = "<tr>" & Environment.NewLine
			For Each key In lsKeys
				sTableRow &= "<td>" & CStr(customer(key)) & "</td>" & Environment.NewLine
			Next
			sTableRow &= "</tr>" & Environment.NewLine
			Debug.Print("sTableRow: " & sTableRow)
			sTable &= sTableRow
		Next

		' Generate the end of the table
		sTable &= "</table>" & Environment.NewLine

		Return sTable
	End Function
	'Create report 3
	Public Sub createReport03(sYear As String, sMonth As String)
		Debug.Print("CreateReport03")

		Dim lsData = thirdReport(sYear, sMonth)
		Dim sReportTitle = "Report 03"
		Dim sReportContent = generateReport03(lsData, sReportTitle)
		Dim sReportFilename = "Report-03.html"
		saveReport(sReportContent, sReportFilename)
		Dim sParam As String = """" & Application.StartupPath & "\" & sReportFilename & """"
		Debug.Print("sParam: " & sParam)
		System.Diagnostics.Process.Start(sParam)
	End Sub
	'Fourth report
	Private Function generateReport04(ByVal lsData As List(Of Hashtable), ByVal sReportTitle As String) _
As String

		Debug.Print("GenerateReport04")

		Dim sReportContent As String

		' 1. Generate the start of the HTML file
		Dim sDoctype As String = "<!DOCTYPE html>"
		Dim sHtmlStartTag As String = "<html lang=""en"">"
		Dim sHeadTitle As String = "<head><title>" & sReportTitle & "</title></head>"
		Dim sBodyStartTag As String = "<body>"
		Dim sReportHeading As String = "<h1>" & sReportTitle & "</h1>"
		sReportContent = sDoctype & Environment.NewLine & sHtmlStartTag & Environment.NewLine _
		  & sHeadTitle & Environment.NewLine & sBodyStartTag & Environment.NewLine & sReportHeading & Environment.NewLine

		' 2. Generate the customer table and its rows
		Dim sTable4 = generateTable4(lsData)
		sReportContent &= sTable4 & Environment.NewLine

		' 3. Generate the end of the HTML file
		Dim sBodyEndTag As String = "</body>"
		Dim sHTMLEndTag As String = "</html>"
		sReportContent &= sBodyEndTag & Environment.NewLine & sHTMLEndTag

		Return sReportContent

	End Function
	'Create table 4
	Private Function generateTable4(ByVal lsData As List(Of Hashtable)) As String
		' Generate the start of the table
		Dim sTable = "<table border=""1"">" & Environment.NewLine
		Dim htSample As Hashtable = lsData.Item(0)
		Dim lsKeys As List(Of String) = New List(Of String)
		lsKeys.Add("BookingID")
		lsKeys.Add("Date")
		lsKeys.Add("RoomID")
		lsKeys.Add("CustomerID")
		lsKeys.Add("NumberOfDays")
		lsKeys.Add("NumberOfGuests")
		lsKeys.Add("TotalPrice")

		' Generate the header row
		Dim sHeaderRow = "<tr>" & Environment.NewLine
		For Each key In lsKeys
			sHeaderRow &= "<th>" & CStr(key) & "</th>" & Environment.NewLine
		Next
		sHeaderRow &= "</tr>" & Environment.NewLine
		Debug.Print("sHeaderRow: " & sHeaderRow)
		sTable &= sHeaderRow

		' Generate the table rows
		For Each record In lsData
			Dim booking As Hashtable = record
			Dim sTableRow = "<tr>" & Environment.NewLine
			For Each key In lsKeys
				sTableRow &= "<td>" & CStr(booking(key)) & "</td>" & Environment.NewLine
			Next
			sTableRow &= "</tr>" & Environment.NewLine
			Debug.Print("sTableRow: " & sTableRow)
			sTable &= sTableRow
		Next

		' Generate the end of the table
		sTable &= "</table>" & Environment.NewLine

		Return sTable
	End Function
	'Create report 4
	Public Sub createReport04(sYear As String, sMonth As String)
		Debug.Print("CreateReport04")

		Dim lsData = fourthReport(sYear, sMonth)
		Dim sReportTitle = "Report 04"
		Dim sReportContent = generateReport04(lsData, sReportTitle)
		Dim sReportFilename = "Report-04.html"
		saveReport(sReportContent, sReportFilename)
		Dim sParam As String = """" & Application.StartupPath & "\" & sReportFilename & """"
		Debug.Print("sParam: " & sParam)
		System.Diagnostics.Process.Start(sParam)
	End Sub
	'Fifth report
	Private Function generateReport05(ByVal lsData As List(Of Hashtable), ByVal sReportTitle As String) _
As String

		Debug.Print("GenerateReport05")

		Dim sReportContent As String

		' 1. Generate the start of the HTML file
		Dim sDoctype As String = "<!DOCTYPE html>"
		Dim sHtmlStartTag As String = "<html lang=""en"">"
		Dim sHeadTitle As String = "<head><title>" & sReportTitle & "</title></head>"
		Dim sBodyStartTag As String = "<body>"
		Dim sReportHeading As String = "<h1>" & sReportTitle & "</h1>"
		sReportContent = sDoctype & Environment.NewLine & sHtmlStartTag & Environment.NewLine _
		  & sHeadTitle & Environment.NewLine & sBodyStartTag & Environment.NewLine & sReportHeading & Environment.NewLine

		' 2. Generate the customer table and its rows
		Dim sTable5 = generateTable5(lsData)
		sReportContent &= sTable5 & Environment.NewLine

		' 3. Generate the end of the HTML file
		Dim sBodyEndTag As String = "</body>"
		Dim sHTMLEndTag As String = "</html>"
		sReportContent &= sBodyEndTag & Environment.NewLine & sHTMLEndTag

		Return sReportContent

	End Function
	'Create table 5
	Private Function generateTable5(ByVal lsData As List(Of Hashtable)) As String
		' Generate the start of the table
		Dim sTable = "<table border=""1"">" & Environment.NewLine
		Dim htSample As Hashtable = lsData.Item(0)
		Dim lsKeys As List(Of String) = New List(Of String)
		lsKeys.Add("CustomerID")
		lsKeys.Add("CheckinDate")

		' Generate the header row
		Dim sHeaderRow = "<tr>" & Environment.NewLine
		For Each key In lsKeys
			sHeaderRow &= "<th>" & CStr(key) & "</th>" & Environment.NewLine
		Next
		sHeaderRow &= "</tr>" & Environment.NewLine
		sTable &= sHeaderRow

		' Generate the table rows
		For Each record In lsData
			Dim booking As Hashtable = record
			Dim sTableRow = "<tr>" & Environment.NewLine
			For Each key In lsKeys
				sTableRow &= "<td>" & CStr(booking(key)) & "</td>" & Environment.NewLine
			Next
			sTableRow &= "</tr>" & Environment.NewLine
			sTable &= sTableRow
		Next

		' Generate the end of the table
		sTable &= "</table>" & Environment.NewLine

		Return sTable
	End Function
	'Create report 5
	Public Sub createReport05(sYear As String, sMonth As String)
		Debug.Print("CreateReport05")

		Dim lsData = fifthReport(sYear, sMonth)
		Dim sReportTitle = "Report 05"
		Dim sReportContent = generateReport05(lsData, sReportTitle)
		Dim sReportFilename = "Report-05.html"
		saveReport(sReportContent, sReportFilename)
		Dim sParam As String = """" & Application.StartupPath & "\" & sReportFilename & """"
		Debug.Print("sParam: " & sParam)
		System.Diagnostics.Process.Start(sParam)
	End Sub
	'Sixth report
	Private Function generateReport06(ByVal lsData As List(Of Hashtable), ByVal sReportTitle As String) _
As String

		Debug.Print("GenerateReport06")

		Dim sReportContent As String

		' 1. Generate the start of the HTML file
		Dim sDoctype As String = "<!DOCTYPE html>"
		Dim sHtmlStartTag As String = "<html lang=""en"">"
		Dim sHeadTitle As String = "<head><title>" & sReportTitle & "</title></head>"
		Dim sBodyStartTag As String = "<body>"
		Dim sReportHeading As String = "<h1>" & sReportTitle & "</h1>"
		sReportContent = sDoctype & Environment.NewLine & sHtmlStartTag & Environment.NewLine _
		  & sHeadTitle & Environment.NewLine & sBodyStartTag & Environment.NewLine & sReportHeading & Environment.NewLine

		' 2. Generate the customer table and its rows
		Dim sTable6 = generateTable6(lsData)
		sReportContent &= sTable6 & Environment.NewLine

		' 3. Generate the end of the HTML file
		Dim sBodyEndTag As String = "</body>"
		Dim sHTMLEndTag As String = "</html>"
		sReportContent &= sBodyEndTag & Environment.NewLine & sHTMLEndTag

		Return sReportContent

	End Function
	'Create table 6
	Private Function generateTable6(ByVal lsData As List(Of Hashtable)) As String
		' Generate the start of the table
		Dim sTable = "<table border=""1"">" & Environment.NewLine
		Dim htSample As Hashtable = lsData.Item(0)
		Dim lsKeys As List(Of String) = New List(Of String)
		lsKeys.Add("RoomNumber")
		lsKeys.Add("BookingID")
		lsKeys.Add("Date")

		' Generate the header row
		Dim sHeaderRow = "<tr>" & Environment.NewLine
		For Each key In lsKeys
			sHeaderRow &= "<th>" & CStr(key) & "</th>" & Environment.NewLine
		Next
		sHeaderRow &= "</tr>" & Environment.NewLine
		sTable &= sHeaderRow

		' Generate the table rows
		For Each record In lsData
			Dim booking As Hashtable = record
			Dim sTableRow = "<tr>" & Environment.NewLine
			For Each key In lsKeys
				sTableRow &= "<td>" & CStr(booking(key)) & "</td>" & Environment.NewLine
			Next
			sTableRow &= "</tr>" & Environment.NewLine
			sTable &= sTableRow
		Next

		' Generate the end of the table
		sTable &= "</table>" & Environment.NewLine

		Return sTable
	End Function
	'Create report 6
	Public Sub createReport06(sYear As String, sMonth As String, sRoom As String)
		Debug.Print("CreateReport06")

		Dim lsData = sixthReport(sYear, sMonth, sRoom)
		Dim sReportTitle = "Report 06"
		Dim sReportContent = generateReport06(lsData, sReportTitle)
		Dim sReportFilename = "Report-06.html"
		saveReport(sReportContent, sReportFilename)
		Dim sParam As String = """" & Application.StartupPath & "\" & sReportFilename & """"
		Debug.Print("sParam: " & sParam)
		System.Diagnostics.Process.Start(sParam)
	End Sub

	'Save report
	Private Sub saveReport(ByVal sReportContent As String, ByVal sReportFilename As String)
		Debug.Print("saveReport: " & sReportFilename)
		Dim oReportFile As StreamWriter = New StreamWriter(Application.StartupPath & "\" & sReportFilename)

		' Check if the file is open before starting to write to it
		If Not (oReportFile Is Nothing) Then
			oReportFile.Write(sReportContent)
			oReportFile.Close()
		End If
	End Sub
End Class
