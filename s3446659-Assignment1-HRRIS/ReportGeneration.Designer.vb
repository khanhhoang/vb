﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ReportGeneration
	Inherits System.Windows.Forms.Form

	'Form overrides dispose to clean up the component list.
	<System.Diagnostics.DebuggerNonUserCode()> _
	Protected Overrides Sub Dispose(ByVal disposing As Boolean)
		Try
			If disposing AndAlso components IsNot Nothing Then
				components.Dispose()
			End If
		Finally
			MyBase.Dispose(disposing)
		End Try
	End Sub

	'Required by the Windows Form Designer
	Private components As System.ComponentModel.IContainer

	'NOTE: The following procedure is required by the Windows Form Designer
	'It can be modified using the Windows Form Designer.  
	'Do not modify it using the code editor.
	<System.Diagnostics.DebuggerStepThrough()> _
	Private Sub InitializeComponent()
		Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(ReportGeneration))
		Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
		Me.MenuToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
		Me.CustomerToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
		Me.RoomToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
		Me.BookingToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
		Me.ReportToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
		Me.GenerateNormalReportToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
		Me.GenerateBreakReportToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
		Me.HelpToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
		Me.AboutToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
		Me.Label1 = New System.Windows.Forms.Label()
		Me.GroupBox1 = New System.Windows.Forms.GroupBox()
		Me.cboCustomerID = New System.Windows.Forms.ComboBox()
		Me.Label2 = New System.Windows.Forms.Label()
		Me.picErrorCustomerId = New System.Windows.Forms.PictureBox()
		Me.btnCreateFirstReport = New System.Windows.Forms.Button()
		Me.GroupBox2 = New System.Windows.Forms.GroupBox()
		Me.cboRoomNumber = New System.Windows.Forms.ComboBox()
		Me.picErrorRoomNumber2 = New System.Windows.Forms.PictureBox()
		Me.Label3 = New System.Windows.Forms.Label()
		Me.btnCreateSecondReport = New System.Windows.Forms.Button()
		Me.GroupBox3 = New System.Windows.Forms.GroupBox()
		Me.picErrorMonth3 = New System.Windows.Forms.PictureBox()
		Me.picErrorYear3 = New System.Windows.Forms.PictureBox()
		Me.txtYear3 = New System.Windows.Forms.TextBox()
		Me.Label4 = New System.Windows.Forms.Label()
		Me.cboMonth3 = New System.Windows.Forms.ComboBox()
		Me.btnCreateThirdReport = New System.Windows.Forms.Button()
		Me.Label5 = New System.Windows.Forms.Label()
		Me.GroupBox4 = New System.Windows.Forms.GroupBox()
		Me.picErrorMonth4 = New System.Windows.Forms.PictureBox()
		Me.picErrorYear4 = New System.Windows.Forms.PictureBox()
		Me.btnCreateFourthReport = New System.Windows.Forms.Button()
		Me.cboMonth4 = New System.Windows.Forms.ComboBox()
		Me.txtYear4 = New System.Windows.Forms.TextBox()
		Me.Label8 = New System.Windows.Forms.Label()
		Me.Label6 = New System.Windows.Forms.Label()
		Me.GroupBox5 = New System.Windows.Forms.GroupBox()
		Me.picErrorMonth5 = New System.Windows.Forms.PictureBox()
		Me.picErrorYear5 = New System.Windows.Forms.PictureBox()
		Me.btnCreateFifthReport = New System.Windows.Forms.Button()
		Me.cboMonth5 = New System.Windows.Forms.ComboBox()
		Me.txtYear5 = New System.Windows.Forms.TextBox()
		Me.Label10 = New System.Windows.Forms.Label()
		Me.Label9 = New System.Windows.Forms.Label()
		Me.GroupBox6 = New System.Windows.Forms.GroupBox()
		Me.picErrorRoomNumber6 = New System.Windows.Forms.PictureBox()
		Me.picErrorMonth6 = New System.Windows.Forms.PictureBox()
		Me.PicErrorYear6 = New System.Windows.Forms.PictureBox()
		Me.Label12 = New System.Windows.Forms.Label()
		Me.cboRoomNumber6 = New System.Windows.Forms.ComboBox()
		Me.cboMonth6 = New System.Windows.Forms.ComboBox()
		Me.btnCreateSixthReport = New System.Windows.Forms.Button()
		Me.txtYear6 = New System.Windows.Forms.TextBox()
		Me.Label7 = New System.Windows.Forms.Label()
		Me.Label11 = New System.Windows.Forms.Label()
		Me.btnCloseRoom = New System.Windows.Forms.Button()
		Me.TextBox1 = New System.Windows.Forms.TextBox()
		Me.TextBox2 = New System.Windows.Forms.TextBox()
		Me.TextBox3 = New System.Windows.Forms.TextBox()
		Me.TextBox4 = New System.Windows.Forms.TextBox()
		Me.TextBox5 = New System.Windows.Forms.TextBox()
		Me.TextBox6 = New System.Windows.Forms.TextBox()
		Me.MenuStrip1.SuspendLayout()
		Me.GroupBox1.SuspendLayout()
		CType(Me.picErrorCustomerId, System.ComponentModel.ISupportInitialize).BeginInit()
		Me.GroupBox2.SuspendLayout()
		CType(Me.picErrorRoomNumber2, System.ComponentModel.ISupportInitialize).BeginInit()
		Me.GroupBox3.SuspendLayout()
		CType(Me.picErrorMonth3, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.picErrorYear3, System.ComponentModel.ISupportInitialize).BeginInit()
		Me.GroupBox4.SuspendLayout()
		CType(Me.picErrorMonth4, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.picErrorYear4, System.ComponentModel.ISupportInitialize).BeginInit()
		Me.GroupBox5.SuspendLayout()
		CType(Me.picErrorMonth5, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.picErrorYear5, System.ComponentModel.ISupportInitialize).BeginInit()
		Me.GroupBox6.SuspendLayout()
		CType(Me.picErrorRoomNumber6, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.picErrorMonth6, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.PicErrorYear6, System.ComponentModel.ISupportInitialize).BeginInit()
		Me.SuspendLayout()
		'
		'MenuStrip1
		'
		Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.MenuToolStripMenuItem, Me.ReportToolStripMenuItem, Me.HelpToolStripMenuItem, Me.AboutToolStripMenuItem})
		Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
		Me.MenuStrip1.Name = "MenuStrip1"
		Me.MenuStrip1.Size = New System.Drawing.Size(969, 24)
		Me.MenuStrip1.TabIndex = 15
		Me.MenuStrip1.Text = "MenuStrip1"
		'
		'MenuToolStripMenuItem
		'
		Me.MenuToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.CustomerToolStripMenuItem, Me.RoomToolStripMenuItem, Me.BookingToolStripMenuItem})
		Me.MenuToolStripMenuItem.Name = "MenuToolStripMenuItem"
		Me.MenuToolStripMenuItem.Size = New System.Drawing.Size(50, 20)
		Me.MenuToolStripMenuItem.Text = "Menu"
		'
		'CustomerToolStripMenuItem
		'
		Me.CustomerToolStripMenuItem.Name = "CustomerToolStripMenuItem"
		Me.CustomerToolStripMenuItem.Size = New System.Drawing.Size(152, 22)
		Me.CustomerToolStripMenuItem.Text = "Customer"
		'
		'RoomToolStripMenuItem
		'
		Me.RoomToolStripMenuItem.Name = "RoomToolStripMenuItem"
		Me.RoomToolStripMenuItem.Size = New System.Drawing.Size(152, 22)
		Me.RoomToolStripMenuItem.Text = "Room"
		'
		'BookingToolStripMenuItem
		'
		Me.BookingToolStripMenuItem.Name = "BookingToolStripMenuItem"
		Me.BookingToolStripMenuItem.Size = New System.Drawing.Size(152, 22)
		Me.BookingToolStripMenuItem.Text = "Booking"
		'
		'ReportToolStripMenuItem
		'
		Me.ReportToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.GenerateNormalReportToolStripMenuItem, Me.GenerateBreakReportToolStripMenuItem})
		Me.ReportToolStripMenuItem.Name = "ReportToolStripMenuItem"
		Me.ReportToolStripMenuItem.Size = New System.Drawing.Size(54, 20)
		Me.ReportToolStripMenuItem.Text = "Report"
		'
		'GenerateNormalReportToolStripMenuItem
		'
		Me.GenerateNormalReportToolStripMenuItem.Name = "GenerateNormalReportToolStripMenuItem"
		Me.GenerateNormalReportToolStripMenuItem.Size = New System.Drawing.Size(202, 22)
		Me.GenerateNormalReportToolStripMenuItem.Text = "Generate Normal Report"
		'
		'GenerateBreakReportToolStripMenuItem
		'
		Me.GenerateBreakReportToolStripMenuItem.Name = "GenerateBreakReportToolStripMenuItem"
		Me.GenerateBreakReportToolStripMenuItem.Size = New System.Drawing.Size(202, 22)
		Me.GenerateBreakReportToolStripMenuItem.Text = "Generate Break Report"
		'
		'HelpToolStripMenuItem
		'
		Me.HelpToolStripMenuItem.Name = "HelpToolStripMenuItem"
		Me.HelpToolStripMenuItem.Size = New System.Drawing.Size(44, 20)
		Me.HelpToolStripMenuItem.Text = "Help"
		'
		'AboutToolStripMenuItem
		'
		Me.AboutToolStripMenuItem.Name = "AboutToolStripMenuItem"
		Me.AboutToolStripMenuItem.Size = New System.Drawing.Size(52, 20)
		Me.AboutToolStripMenuItem.Text = "About"
		'
		'Label1
		'
		Me.Label1.AutoSize = True
		Me.Label1.Location = New System.Drawing.Point(267, 41)
		Me.Label1.Name = "Label1"
		Me.Label1.Size = New System.Drawing.Size(161, 13)
		Me.Label1.TabIndex = 16
		Me.Label1.Text = "Report-Generation functionalities"
		'
		'GroupBox1
		'
		Me.GroupBox1.Controls.Add(Me.cboCustomerID)
		Me.GroupBox1.Controls.Add(Me.Label2)
		Me.GroupBox1.Controls.Add(Me.picErrorCustomerId)
		Me.GroupBox1.Controls.Add(Me.btnCreateFirstReport)
		Me.GroupBox1.Location = New System.Drawing.Point(15, 69)
		Me.GroupBox1.Name = "GroupBox1"
		Me.GroupBox1.Size = New System.Drawing.Size(282, 70)
		Me.GroupBox1.TabIndex = 17
		Me.GroupBox1.TabStop = False
		Me.GroupBox1.Text = "Report 1"
		'
		'cboCustomerID
		'
		Me.cboCustomerID.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
		Me.cboCustomerID.FormattingEnabled = True
		Me.cboCustomerID.Location = New System.Drawing.Point(43, 32)
		Me.cboCustomerID.Name = "cboCustomerID"
		Me.cboCustomerID.Size = New System.Drawing.Size(81, 21)
		Me.cboCustomerID.TabIndex = 0
		'
		'Label2
		'
		Me.Label2.AutoSize = True
		Me.Label2.Location = New System.Drawing.Point(49, 16)
		Me.Label2.Name = "Label2"
		Me.Label2.Size = New System.Drawing.Size(62, 13)
		Me.Label2.TabIndex = 8
		Me.Label2.Text = "CustomerID"
		'
		'picErrorCustomerId
		'
		Me.picErrorCustomerId.Image = CType(resources.GetObject("picErrorCustomerId.Image"), System.Drawing.Image)
		Me.picErrorCustomerId.Location = New System.Drawing.Point(130, 34)
		Me.picErrorCustomerId.Name = "picErrorCustomerId"
		Me.picErrorCustomerId.Size = New System.Drawing.Size(20, 19)
		Me.picErrorCustomerId.TabIndex = 102
		Me.picErrorCustomerId.TabStop = False
		Me.picErrorCustomerId.Visible = False
		'
		'btnCreateFirstReport
		'
		Me.btnCreateFirstReport.Location = New System.Drawing.Point(167, 25)
		Me.btnCreateFirstReport.Name = "btnCreateFirstReport"
		Me.btnCreateFirstReport.Size = New System.Drawing.Size(96, 33)
		Me.btnCreateFirstReport.TabIndex = 1
		Me.btnCreateFirstReport.Text = "Generate Report"
		Me.btnCreateFirstReport.UseVisualStyleBackColor = True
		'
		'GroupBox2
		'
		Me.GroupBox2.Controls.Add(Me.cboRoomNumber)
		Me.GroupBox2.Controls.Add(Me.picErrorRoomNumber2)
		Me.GroupBox2.Controls.Add(Me.Label3)
		Me.GroupBox2.Controls.Add(Me.btnCreateSecondReport)
		Me.GroupBox2.Location = New System.Drawing.Point(423, 69)
		Me.GroupBox2.Name = "GroupBox2"
		Me.GroupBox2.Size = New System.Drawing.Size(282, 70)
		Me.GroupBox2.TabIndex = 18
		Me.GroupBox2.TabStop = False
		Me.GroupBox2.Text = "Report 2"
		'
		'cboRoomNumber
		'
		Me.cboRoomNumber.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
		Me.cboRoomNumber.FormattingEnabled = True
		Me.cboRoomNumber.Location = New System.Drawing.Point(43, 32)
		Me.cboRoomNumber.Name = "cboRoomNumber"
		Me.cboRoomNumber.Size = New System.Drawing.Size(81, 21)
		Me.cboRoomNumber.TabIndex = 4
		'
		'picErrorRoomNumber2
		'
		Me.picErrorRoomNumber2.Image = CType(resources.GetObject("picErrorRoomNumber2.Image"), System.Drawing.Image)
		Me.picErrorRoomNumber2.Location = New System.Drawing.Point(130, 32)
		Me.picErrorRoomNumber2.Name = "picErrorRoomNumber2"
		Me.picErrorRoomNumber2.Size = New System.Drawing.Size(20, 19)
		Me.picErrorRoomNumber2.TabIndex = 103
		Me.picErrorRoomNumber2.TabStop = False
		Me.picErrorRoomNumber2.Visible = False
		'
		'Label3
		'
		Me.Label3.AutoSize = True
		Me.Label3.Location = New System.Drawing.Point(49, 16)
		Me.Label3.Name = "Label3"
		Me.Label3.Size = New System.Drawing.Size(75, 13)
		Me.Label3.TabIndex = 9
		Me.Label3.Text = "Room Number"
		'
		'btnCreateSecondReport
		'
		Me.btnCreateSecondReport.Location = New System.Drawing.Point(167, 25)
		Me.btnCreateSecondReport.Name = "btnCreateSecondReport"
		Me.btnCreateSecondReport.Size = New System.Drawing.Size(96, 33)
		Me.btnCreateSecondReport.TabIndex = 3
		Me.btnCreateSecondReport.Text = "Generate Report"
		Me.btnCreateSecondReport.UseVisualStyleBackColor = True
		'
		'GroupBox3
		'
		Me.GroupBox3.Controls.Add(Me.picErrorMonth3)
		Me.GroupBox3.Controls.Add(Me.picErrorYear3)
		Me.GroupBox3.Controls.Add(Me.txtYear3)
		Me.GroupBox3.Controls.Add(Me.Label4)
		Me.GroupBox3.Controls.Add(Me.cboMonth3)
		Me.GroupBox3.Controls.Add(Me.btnCreateThirdReport)
		Me.GroupBox3.Controls.Add(Me.Label5)
		Me.GroupBox3.Location = New System.Drawing.Point(12, 221)
		Me.GroupBox3.Name = "GroupBox3"
		Me.GroupBox3.Size = New System.Drawing.Size(387, 70)
		Me.GroupBox3.TabIndex = 19
		Me.GroupBox3.TabStop = False
		Me.GroupBox3.Text = "Report 3"
		'
		'picErrorMonth3
		'
		Me.picErrorMonth3.Image = CType(resources.GetObject("picErrorMonth3.Image"), System.Drawing.Image)
		Me.picErrorMonth3.Location = New System.Drawing.Point(235, 34)
		Me.picErrorMonth3.Name = "picErrorMonth3"
		Me.picErrorMonth3.Size = New System.Drawing.Size(20, 19)
		Me.picErrorMonth3.TabIndex = 104
		Me.picErrorMonth3.TabStop = False
		Me.picErrorMonth3.Visible = False
		'
		'picErrorYear3
		'
		Me.picErrorYear3.Image = CType(resources.GetObject("picErrorYear3.Image"), System.Drawing.Image)
		Me.picErrorYear3.Location = New System.Drawing.Point(130, 31)
		Me.picErrorYear3.Name = "picErrorYear3"
		Me.picErrorYear3.Size = New System.Drawing.Size(20, 19)
		Me.picErrorYear3.TabIndex = 101
		Me.picErrorYear3.TabStop = False
		Me.picErrorYear3.Visible = False
		'
		'txtYear3
		'
		Me.txtYear3.Location = New System.Drawing.Point(43, 30)
		Me.txtYear3.Name = "txtYear3"
		Me.txtYear3.Size = New System.Drawing.Size(81, 20)
		Me.txtYear3.TabIndex = 5
		'
		'Label4
		'
		Me.Label4.AutoSize = True
		Me.Label4.Location = New System.Drawing.Point(49, 14)
		Me.Label4.Name = "Label4"
		Me.Label4.Size = New System.Drawing.Size(29, 13)
		Me.Label4.TabIndex = 10
		Me.Label4.Text = "Year"
		'
		'cboMonth3
		'
		Me.cboMonth3.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
		Me.cboMonth3.FormattingEnabled = True
		Me.cboMonth3.Items.AddRange(New Object() {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12"})
		Me.cboMonth3.Location = New System.Drawing.Point(170, 32)
		Me.cboMonth3.Name = "cboMonth3"
		Me.cboMonth3.Size = New System.Drawing.Size(59, 21)
		Me.cboMonth3.TabIndex = 6
		'
		'btnCreateThirdReport
		'
		Me.btnCreateThirdReport.Location = New System.Drawing.Point(273, 23)
		Me.btnCreateThirdReport.Name = "btnCreateThirdReport"
		Me.btnCreateThirdReport.Size = New System.Drawing.Size(96, 33)
		Me.btnCreateThirdReport.TabIndex = 7
		Me.btnCreateThirdReport.Text = "Generate Report"
		Me.btnCreateThirdReport.UseVisualStyleBackColor = True
		'
		'Label5
		'
		Me.Label5.AutoSize = True
		Me.Label5.Location = New System.Drawing.Point(167, 16)
		Me.Label5.Name = "Label5"
		Me.Label5.Size = New System.Drawing.Size(37, 13)
		Me.Label5.TabIndex = 11
		Me.Label5.Text = "Month"
		'
		'GroupBox4
		'
		Me.GroupBox4.Controls.Add(Me.picErrorMonth4)
		Me.GroupBox4.Controls.Add(Me.picErrorYear4)
		Me.GroupBox4.Controls.Add(Me.btnCreateFourthReport)
		Me.GroupBox4.Controls.Add(Me.cboMonth4)
		Me.GroupBox4.Controls.Add(Me.txtYear4)
		Me.GroupBox4.Controls.Add(Me.Label8)
		Me.GroupBox4.Controls.Add(Me.Label6)
		Me.GroupBox4.Location = New System.Drawing.Point(423, 221)
		Me.GroupBox4.Name = "GroupBox4"
		Me.GroupBox4.Size = New System.Drawing.Size(385, 70)
		Me.GroupBox4.TabIndex = 20
		Me.GroupBox4.TabStop = False
		Me.GroupBox4.Text = "Report 4"
		'
		'picErrorMonth4
		'
		Me.picErrorMonth4.Image = CType(resources.GetObject("picErrorMonth4.Image"), System.Drawing.Image)
		Me.picErrorMonth4.Location = New System.Drawing.Point(235, 34)
		Me.picErrorMonth4.Name = "picErrorMonth4"
		Me.picErrorMonth4.Size = New System.Drawing.Size(20, 19)
		Me.picErrorMonth4.TabIndex = 105
		Me.picErrorMonth4.TabStop = False
		Me.picErrorMonth4.Visible = False
		'
		'picErrorYear4
		'
		Me.picErrorYear4.Image = CType(resources.GetObject("picErrorYear4.Image"), System.Drawing.Image)
		Me.picErrorYear4.Location = New System.Drawing.Point(130, 32)
		Me.picErrorYear4.Name = "picErrorYear4"
		Me.picErrorYear4.Size = New System.Drawing.Size(20, 19)
		Me.picErrorYear4.TabIndex = 102
		Me.picErrorYear4.TabStop = False
		Me.picErrorYear4.Visible = False
		'
		'btnCreateFourthReport
		'
		Me.btnCreateFourthReport.Location = New System.Drawing.Point(272, 23)
		Me.btnCreateFourthReport.Name = "btnCreateFourthReport"
		Me.btnCreateFourthReport.Size = New System.Drawing.Size(96, 33)
		Me.btnCreateFourthReport.TabIndex = 12
		Me.btnCreateFourthReport.Text = "Generate Report"
		Me.btnCreateFourthReport.UseVisualStyleBackColor = True
		'
		'cboMonth4
		'
		Me.cboMonth4.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
		Me.cboMonth4.FormattingEnabled = True
		Me.cboMonth4.Items.AddRange(New Object() {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12"})
		Me.cboMonth4.Location = New System.Drawing.Point(170, 32)
		Me.cboMonth4.Name = "cboMonth4"
		Me.cboMonth4.Size = New System.Drawing.Size(59, 21)
		Me.cboMonth4.TabIndex = 12
		'
		'txtYear4
		'
		Me.txtYear4.Location = New System.Drawing.Point(43, 32)
		Me.txtYear4.Name = "txtYear4"
		Me.txtYear4.Size = New System.Drawing.Size(81, 20)
		Me.txtYear4.TabIndex = 12
		'
		'Label8
		'
		Me.Label8.AutoSize = True
		Me.Label8.Location = New System.Drawing.Point(167, 16)
		Me.Label8.Name = "Label8"
		Me.Label8.Size = New System.Drawing.Size(37, 13)
		Me.Label8.TabIndex = 12
		Me.Label8.Text = "Month"
		'
		'Label6
		'
		Me.Label6.AutoSize = True
		Me.Label6.Location = New System.Drawing.Point(49, 16)
		Me.Label6.Name = "Label6"
		Me.Label6.Size = New System.Drawing.Size(29, 13)
		Me.Label6.TabIndex = 12
		Me.Label6.Text = "Year"
		'
		'GroupBox5
		'
		Me.GroupBox5.Controls.Add(Me.picErrorMonth5)
		Me.GroupBox5.Controls.Add(Me.picErrorYear5)
		Me.GroupBox5.Controls.Add(Me.btnCreateFifthReport)
		Me.GroupBox5.Controls.Add(Me.cboMonth5)
		Me.GroupBox5.Controls.Add(Me.txtYear5)
		Me.GroupBox5.Controls.Add(Me.Label10)
		Me.GroupBox5.Controls.Add(Me.Label9)
		Me.GroupBox5.Location = New System.Drawing.Point(12, 377)
		Me.GroupBox5.Name = "GroupBox5"
		Me.GroupBox5.Size = New System.Drawing.Size(384, 70)
		Me.GroupBox5.TabIndex = 21
		Me.GroupBox5.TabStop = False
		Me.GroupBox5.Text = "Report 5"
		'
		'picErrorMonth5
		'
		Me.picErrorMonth5.Image = CType(resources.GetObject("picErrorMonth5.Image"), System.Drawing.Image)
		Me.picErrorMonth5.Location = New System.Drawing.Point(235, 35)
		Me.picErrorMonth5.Name = "picErrorMonth5"
		Me.picErrorMonth5.Size = New System.Drawing.Size(20, 19)
		Me.picErrorMonth5.TabIndex = 106
		Me.picErrorMonth5.TabStop = False
		Me.picErrorMonth5.Visible = False
		'
		'picErrorYear5
		'
		Me.picErrorYear5.Image = CType(resources.GetObject("picErrorYear5.Image"), System.Drawing.Image)
		Me.picErrorYear5.Location = New System.Drawing.Point(130, 32)
		Me.picErrorYear5.Name = "picErrorYear5"
		Me.picErrorYear5.Size = New System.Drawing.Size(20, 19)
		Me.picErrorYear5.TabIndex = 103
		Me.picErrorYear5.TabStop = False
		Me.picErrorYear5.Visible = False
		'
		'btnCreateFifthReport
		'
		Me.btnCreateFifthReport.Location = New System.Drawing.Point(270, 26)
		Me.btnCreateFifthReport.Name = "btnCreateFifthReport"
		Me.btnCreateFifthReport.Size = New System.Drawing.Size(96, 33)
		Me.btnCreateFifthReport.TabIndex = 13
		Me.btnCreateFifthReport.Text = "Generate Report"
		Me.btnCreateFifthReport.UseVisualStyleBackColor = True
		'
		'cboMonth5
		'
		Me.cboMonth5.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
		Me.cboMonth5.FormattingEnabled = True
		Me.cboMonth5.Items.AddRange(New Object() {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12"})
		Me.cboMonth5.Location = New System.Drawing.Point(170, 33)
		Me.cboMonth5.Name = "cboMonth5"
		Me.cboMonth5.Size = New System.Drawing.Size(59, 21)
		Me.cboMonth5.TabIndex = 13
		'
		'txtYear5
		'
		Me.txtYear5.Location = New System.Drawing.Point(43, 34)
		Me.txtYear5.Name = "txtYear5"
		Me.txtYear5.Size = New System.Drawing.Size(81, 20)
		Me.txtYear5.TabIndex = 14
		'
		'Label10
		'
		Me.Label10.AutoSize = True
		Me.Label10.Location = New System.Drawing.Point(49, 16)
		Me.Label10.Name = "Label10"
		Me.Label10.Size = New System.Drawing.Size(29, 13)
		Me.Label10.TabIndex = 16
		Me.Label10.Text = "Year"
		'
		'Label9
		'
		Me.Label9.AutoSize = True
		Me.Label9.Location = New System.Drawing.Point(167, 16)
		Me.Label9.Name = "Label9"
		Me.Label9.Size = New System.Drawing.Size(37, 13)
		Me.Label9.TabIndex = 15
		Me.Label9.Text = "Month"
		'
		'GroupBox6
		'
		Me.GroupBox6.Controls.Add(Me.picErrorRoomNumber6)
		Me.GroupBox6.Controls.Add(Me.picErrorMonth6)
		Me.GroupBox6.Controls.Add(Me.PicErrorYear6)
		Me.GroupBox6.Controls.Add(Me.Label12)
		Me.GroupBox6.Controls.Add(Me.cboRoomNumber6)
		Me.GroupBox6.Controls.Add(Me.cboMonth6)
		Me.GroupBox6.Controls.Add(Me.btnCreateSixthReport)
		Me.GroupBox6.Controls.Add(Me.txtYear6)
		Me.GroupBox6.Controls.Add(Me.Label7)
		Me.GroupBox6.Controls.Add(Me.Label11)
		Me.GroupBox6.Location = New System.Drawing.Point(423, 377)
		Me.GroupBox6.Name = "GroupBox6"
		Me.GroupBox6.Size = New System.Drawing.Size(534, 70)
		Me.GroupBox6.TabIndex = 22
		Me.GroupBox6.TabStop = False
		Me.GroupBox6.Text = "Report 6"
		'
		'picErrorRoomNumber6
		'
		Me.picErrorRoomNumber6.Image = CType(resources.GetObject("picErrorRoomNumber6.Image"), System.Drawing.Image)
		Me.picErrorRoomNumber6.Location = New System.Drawing.Point(363, 31)
		Me.picErrorRoomNumber6.Name = "picErrorRoomNumber6"
		Me.picErrorRoomNumber6.Size = New System.Drawing.Size(20, 19)
		Me.picErrorRoomNumber6.TabIndex = 108
		Me.picErrorRoomNumber6.TabStop = False
		Me.picErrorRoomNumber6.Visible = False
		'
		'picErrorMonth6
		'
		Me.picErrorMonth6.Image = CType(resources.GetObject("picErrorMonth6.Image"), System.Drawing.Image)
		Me.picErrorMonth6.Location = New System.Drawing.Point(235, 31)
		Me.picErrorMonth6.Name = "picErrorMonth6"
		Me.picErrorMonth6.Size = New System.Drawing.Size(20, 19)
		Me.picErrorMonth6.TabIndex = 107
		Me.picErrorMonth6.TabStop = False
		Me.picErrorMonth6.Visible = False
		'
		'PicErrorYear6
		'
		Me.PicErrorYear6.Image = CType(resources.GetObject("PicErrorYear6.Image"), System.Drawing.Image)
		Me.PicErrorYear6.Location = New System.Drawing.Point(130, 30)
		Me.PicErrorYear6.Name = "PicErrorYear6"
		Me.PicErrorYear6.Size = New System.Drawing.Size(20, 19)
		Me.PicErrorYear6.TabIndex = 104
		Me.PicErrorYear6.TabStop = False
		Me.PicErrorYear6.Visible = False
		'
		'Label12
		'
		Me.Label12.AutoSize = True
		Me.Label12.Location = New System.Drawing.Point(272, 13)
		Me.Label12.Name = "Label12"
		Me.Label12.Size = New System.Drawing.Size(75, 13)
		Me.Label12.TabIndex = 22
		Me.Label12.Text = "Room Number"
		'
		'cboRoomNumber6
		'
		Me.cboRoomNumber6.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
		Me.cboRoomNumber6.FormattingEnabled = True
		Me.cboRoomNumber6.Location = New System.Drawing.Point(275, 30)
		Me.cboRoomNumber6.Name = "cboRoomNumber6"
		Me.cboRoomNumber6.Size = New System.Drawing.Size(82, 21)
		Me.cboRoomNumber6.TabIndex = 21
		'
		'cboMonth6
		'
		Me.cboMonth6.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
		Me.cboMonth6.FormattingEnabled = True
		Me.cboMonth6.Items.AddRange(New Object() {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12"})
		Me.cboMonth6.Location = New System.Drawing.Point(170, 30)
		Me.cboMonth6.Name = "cboMonth6"
		Me.cboMonth6.Size = New System.Drawing.Size(59, 21)
		Me.cboMonth6.TabIndex = 17
		'
		'btnCreateSixthReport
		'
		Me.btnCreateSixthReport.Location = New System.Drawing.Point(418, 24)
		Me.btnCreateSixthReport.Name = "btnCreateSixthReport"
		Me.btnCreateSixthReport.Size = New System.Drawing.Size(96, 33)
		Me.btnCreateSixthReport.TabIndex = 17
		Me.btnCreateSixthReport.Text = "Generate Report"
		Me.btnCreateSixthReport.UseVisualStyleBackColor = True
		'
		'txtYear6
		'
		Me.txtYear6.Location = New System.Drawing.Point(43, 31)
		Me.txtYear6.Name = "txtYear6"
		Me.txtYear6.Size = New System.Drawing.Size(81, 20)
		Me.txtYear6.TabIndex = 18
		'
		'Label7
		'
		Me.Label7.AutoSize = True
		Me.Label7.Location = New System.Drawing.Point(49, 14)
		Me.Label7.Name = "Label7"
		Me.Label7.Size = New System.Drawing.Size(29, 13)
		Me.Label7.TabIndex = 20
		Me.Label7.Text = "Year"
		'
		'Label11
		'
		Me.Label11.AutoSize = True
		Me.Label11.Location = New System.Drawing.Point(167, 13)
		Me.Label11.Name = "Label11"
		Me.Label11.Size = New System.Drawing.Size(37, 13)
		Me.Label11.TabIndex = 19
		Me.Label11.Text = "Month"
		'
		'btnCloseRoom
		'
		Me.btnCloseRoom.Location = New System.Drawing.Point(786, 525)
		Me.btnCloseRoom.Name = "btnCloseRoom"
		Me.btnCloseRoom.Size = New System.Drawing.Size(75, 23)
		Me.btnCloseRoom.TabIndex = 23
		Me.btnCloseRoom.Text = "Close"
		Me.btnCloseRoom.UseVisualStyleBackColor = True
		'
		'TextBox1
		'
		Me.TextBox1.Enabled = False
		Me.TextBox1.Location = New System.Drawing.Point(15, 145)
		Me.TextBox1.Multiline = True
		Me.TextBox1.Name = "TextBox1"
		Me.TextBox1.Size = New System.Drawing.Size(281, 57)
		Me.TextBox1.TabIndex = 25
		Me.TextBox1.Text = "Given a customer ID (selected from a dropdown box created when the form is loaded" &
	"), when was the last time a booking made and for how many days?"
		'
		'TextBox2
		'
		Me.TextBox2.Enabled = False
		Me.TextBox2.Location = New System.Drawing.Point(423, 145)
		Me.TextBox2.Multiline = True
		Me.TextBox2.Name = "TextBox2"
		Me.TextBox2.Size = New System.Drawing.Size(281, 57)
		Me.TextBox2.TabIndex = 26
		Me.TextBox2.Text = "Given a room number, when was the last time the room booked and what was the tota" &
	"l price paid?"
		'
		'TextBox3
		'
		Me.TextBox3.Enabled = False
		Me.TextBox3.Location = New System.Drawing.Point(12, 297)
		Me.TextBox3.Multiline = True
		Me.TextBox3.Name = "TextBox3"
		Me.TextBox3.Size = New System.Drawing.Size(281, 57)
		Me.TextBox3.TabIndex = 27
		Me.TextBox3.Text = "How many rooms were booked by a given customer for a given period (year entered i" &
	"n a textbox and month selected from a dropdown box)?"
		'
		'TextBox4
		'
		Me.TextBox4.Enabled = False
		Me.TextBox4.Location = New System.Drawing.Point(423, 297)
		Me.TextBox4.Multiline = True
		Me.TextBox4.Name = "TextBox4"
		Me.TextBox4.Size = New System.Drawing.Size(281, 57)
		Me.TextBox4.TabIndex = 28
		Me.TextBox4.Text = "Show all the bookings made in a given month of a given year."
		'
		'TextBox5
		'
		Me.TextBox5.Enabled = False
		Me.TextBox5.Location = New System.Drawing.Point(12, 453)
		Me.TextBox5.Multiline = True
		Me.TextBox5.Name = "TextBox5"
		Me.TextBox5.Size = New System.Drawing.Size(281, 57)
		Me.TextBox5.TabIndex = 29
		Me.TextBox5.Text = "Show all the customers who are due for checkin for a given month of a given year." &
	" This could be used for sending them email reminders"
		'
		'TextBox6
		'
		Me.TextBox6.Enabled = False
		Me.TextBox6.Location = New System.Drawing.Point(423, 453)
		Me.TextBox6.Multiline = True
		Me.TextBox6.Name = "TextBox6"
		Me.TextBox6.Size = New System.Drawing.Size(281, 57)
		Me.TextBox6.TabIndex = 30
		Me.TextBox6.Text = "Show all the bookings for a particular room in a given month of a given year."
		'
		'ReportGeneration
		'
		Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
		Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
		Me.ClientSize = New System.Drawing.Size(969, 573)
		Me.Controls.Add(Me.TextBox6)
		Me.Controls.Add(Me.TextBox5)
		Me.Controls.Add(Me.TextBox4)
		Me.Controls.Add(Me.TextBox3)
		Me.Controls.Add(Me.TextBox2)
		Me.Controls.Add(Me.TextBox1)
		Me.Controls.Add(Me.btnCloseRoom)
		Me.Controls.Add(Me.GroupBox6)
		Me.Controls.Add(Me.GroupBox5)
		Me.Controls.Add(Me.GroupBox4)
		Me.Controls.Add(Me.GroupBox3)
		Me.Controls.Add(Me.GroupBox2)
		Me.Controls.Add(Me.GroupBox1)
		Me.Controls.Add(Me.Label1)
		Me.Controls.Add(Me.MenuStrip1)
		Me.Name = "ReportGeneration"
		Me.Text = "ReportGeneration"
		Me.MenuStrip1.ResumeLayout(False)
		Me.MenuStrip1.PerformLayout()
		Me.GroupBox1.ResumeLayout(False)
		Me.GroupBox1.PerformLayout()
		CType(Me.picErrorCustomerId, System.ComponentModel.ISupportInitialize).EndInit()
		Me.GroupBox2.ResumeLayout(False)
		Me.GroupBox2.PerformLayout()
		CType(Me.picErrorRoomNumber2, System.ComponentModel.ISupportInitialize).EndInit()
		Me.GroupBox3.ResumeLayout(False)
		Me.GroupBox3.PerformLayout()
		CType(Me.picErrorMonth3, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.picErrorYear3, System.ComponentModel.ISupportInitialize).EndInit()
		Me.GroupBox4.ResumeLayout(False)
		Me.GroupBox4.PerformLayout()
		CType(Me.picErrorMonth4, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.picErrorYear4, System.ComponentModel.ISupportInitialize).EndInit()
		Me.GroupBox5.ResumeLayout(False)
		Me.GroupBox5.PerformLayout()
		CType(Me.picErrorMonth5, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.picErrorYear5, System.ComponentModel.ISupportInitialize).EndInit()
		Me.GroupBox6.ResumeLayout(False)
		Me.GroupBox6.PerformLayout()
		CType(Me.picErrorRoomNumber6, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.picErrorMonth6, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.PicErrorYear6, System.ComponentModel.ISupportInitialize).EndInit()
		Me.ResumeLayout(False)
		Me.PerformLayout()

	End Sub

	Friend WithEvents MenuStrip1 As MenuStrip
	Friend WithEvents MenuToolStripMenuItem As ToolStripMenuItem
	Friend WithEvents CustomerToolStripMenuItem As ToolStripMenuItem
	Friend WithEvents RoomToolStripMenuItem As ToolStripMenuItem
	Friend WithEvents BookingToolStripMenuItem As ToolStripMenuItem
	Friend WithEvents ReportToolStripMenuItem As ToolStripMenuItem
	Friend WithEvents GenerateNormalReportToolStripMenuItem As ToolStripMenuItem
	Friend WithEvents GenerateBreakReportToolStripMenuItem As ToolStripMenuItem
	Friend WithEvents HelpToolStripMenuItem As ToolStripMenuItem
	Friend WithEvents AboutToolStripMenuItem As ToolStripMenuItem
	Friend WithEvents Label1 As Label
	Friend WithEvents GroupBox1 As GroupBox
	Friend WithEvents cboCustomerID As ComboBox
	Friend WithEvents Label2 As Label
	Friend WithEvents picErrorCustomerId As PictureBox
	Friend WithEvents btnCreateFirstReport As Button
	Friend WithEvents GroupBox2 As GroupBox
	Friend WithEvents cboRoomNumber As ComboBox
	Friend WithEvents picErrorRoomNumber2 As PictureBox
	Friend WithEvents Label3 As Label
	Friend WithEvents btnCreateSecondReport As Button
	Friend WithEvents GroupBox3 As GroupBox
	Friend WithEvents picErrorMonth3 As PictureBox
	Friend WithEvents picErrorYear3 As PictureBox
	Friend WithEvents txtYear3 As TextBox
	Friend WithEvents Label4 As Label
	Friend WithEvents cboMonth3 As ComboBox
	Friend WithEvents btnCreateThirdReport As Button
	Friend WithEvents Label5 As Label
	Friend WithEvents GroupBox4 As GroupBox
	Friend WithEvents picErrorMonth4 As PictureBox
	Friend WithEvents picErrorYear4 As PictureBox
	Friend WithEvents btnCreateFourthReport As Button
	Friend WithEvents cboMonth4 As ComboBox
	Friend WithEvents txtYear4 As TextBox
	Friend WithEvents Label8 As Label
	Friend WithEvents Label6 As Label
	Friend WithEvents GroupBox5 As GroupBox
	Friend WithEvents picErrorMonth5 As PictureBox
	Friend WithEvents picErrorYear5 As PictureBox
	Friend WithEvents btnCreateFifthReport As Button
	Friend WithEvents cboMonth5 As ComboBox
	Friend WithEvents txtYear5 As TextBox
	Friend WithEvents Label10 As Label
	Friend WithEvents Label9 As Label
	Friend WithEvents GroupBox6 As GroupBox
	Friend WithEvents picErrorRoomNumber6 As PictureBox
	Friend WithEvents picErrorMonth6 As PictureBox
	Friend WithEvents PicErrorYear6 As PictureBox
	Friend WithEvents Label12 As Label
	Friend WithEvents cboRoomNumber6 As ComboBox
	Friend WithEvents cboMonth6 As ComboBox
	Friend WithEvents btnCreateSixthReport As Button
	Friend WithEvents txtYear6 As TextBox
	Friend WithEvents Label7 As Label
	Friend WithEvents Label11 As Label
	Friend WithEvents btnCloseRoom As Button
	Friend WithEvents TextBox1 As TextBox
	Friend WithEvents TextBox2 As TextBox
	Friend WithEvents TextBox3 As TextBox
	Friend WithEvents TextBox4 As TextBox
	Friend WithEvents TextBox5 As TextBox
	Friend WithEvents TextBox6 As TextBox
End Class
