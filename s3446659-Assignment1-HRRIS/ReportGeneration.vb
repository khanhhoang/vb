﻿Option Explicit On
Option Strict On

Imports System.Data.OleDb
Imports System.IO
Imports System.Drawing

' Name:        ReportGeneration.vb
' Description: Form to make report
' Author:      Do Khanh Hoang
' Date:        28/04/2017



Public Class ReportGeneration
	'Load form
	Private Sub Report_Generation_Load(sender As Object, e As EventArgs) Handles MyBase.Load
		Dim oReportController As ReportGenerationController = New ReportGenerationController
		Dim lsCustomerData = oReportController.CallCustomerID()
		Dim lsRoomNumber = oReportController.CallRoomNumber()

		For Each customer_id In lsCustomerData
			cboCustomerID.Items.Add(CStr(customer_id("CustomerID")))
		Next

		For Each room_number In lsRoomNumber
			cboRoomNumber.Items.Add(CStr(room_number("RoomNumber")))
			cboRoomNumber6.Items.Add(CStr(room_number("RoomNumber")))
		Next

	End Sub
	'Create First Report button
	Private Sub btnGenerateReport_Click(sender As Object, e As EventArgs) Handles btnCreateFirstReport.Click
		Dim bValidation As New Validation
		Dim bIsValid As Boolean
		Dim tt As New ToolTip()
		bIsValid = bValidation.isNumericVal(cboCustomerID.Text)
		If bIsValid Then
			picErrorCustomerId.Visible = False
			Dim oController As ReportGenerationController = New ReportGenerationController
			Dim sCustomerID = cboCustomerID.Text
			oController.createReport01(sCustomerID)
		Else
			picErrorCustomerId.Visible = True
			tt.SetToolTip(picErrorCustomerId, "Please select the customerID to create report!")
			MsgBox("Error occurs! Cannot create report this time!")
		End If
	End Sub
	'Create Second Report button
	Private Sub btnCreateSecondReport_Click(sender As Object, e As EventArgs) Handles btnCreateSecondReport.Click
		Dim bValidation As New Validation
		Dim bIsValid As Boolean
		Dim tt As New ToolTip()
		bIsValid = bValidation.isNumericVal(cboRoomNumber.Text)
		If bIsValid Then
			Dim oController As ReportGenerationController = New ReportGenerationController
			Dim sRoomNumber = cboRoomNumber.Text
			oController.createReport02(sRoomNumber)
		Else
			picErrorRoomNumber2.Visible = True
			tt.SetToolTip(picErrorRoomNumber2, "Please select the room number!")
			MsgBox("Error occurs! Cannot create report this time!")
		End If
	End Sub
	'Create Third Report button
	Private Sub btnCreateThirdReport_Click(sender As Object, e As EventArgs) Handles btnCreateThirdReport.Click
		Dim bValidation As New Validation
		Dim bIsValid As Boolean
		Dim allValid As Boolean
		allValid = True
		Dim tt As New ToolTip()
		'Year
		bIsValid = bValidation.IsYearValid(txtYear3.Text)
		If bIsValid Then
			picErrorYear3.Visible = False
		Else
			picErrorYear3.Visible = True
			tt.SetToolTip(picErrorYear3, "Please re-enter the year correctly e.g: 2016")
			allValid = False
		End If

		'Month
		bIsValid = bValidation.isNumericVal(cboMonth3.Text)
		If bIsValid Then
			picErrorMonth3.Visible = False
		Else
			picErrorMonth3.Visible = True
			tt.SetToolTip(picErrorMonth3, "Please select month!")
			allValid = False
		End If
		'Both valid
		If allValid Then
			'Create report 3
			Dim oController As ReportGenerationController = New ReportGenerationController
			Dim sYear = txtYear3.Text
			Dim sMonth = cboMonth3.Text
			oController.createReport03(sYear, sMonth)
		Else
			MsgBox("Error occurs! Cannot create report this time!")
		End If
	End Sub
	'Create Fourth Report button
	Private Sub btnCreateFourthReport_Click(sender As Object, e As EventArgs) Handles btnCreateFourthReport.Click
		Dim bValidation As New Validation
		Dim bIsValid As Boolean
		Dim allValid As Boolean
		allValid = True
		Dim tt As New ToolTip()
		'Year
		bIsValid = bValidation.IsYearValid(txtYear4.Text)
		If bIsValid Then
			picErrorYear4.Visible = False
		Else
			picErrorYear4.Visible = True
			tt.SetToolTip(picErrorYear4, "Please re-enter the year correctly e.g: 2016")
			allValid = False
		End If

		'Month
		bIsValid = bValidation.isNumericVal(cboMonth4.Text)
		If bIsValid Then
			picErrorMonth4.Visible = False
		Else
			picErrorMonth4.Visible = True
			tt.SetToolTip(picErrorMonth4, "Please select month!")
			allValid = False
		End If
		If allValid Then
			Dim oController As ReportGenerationController = New ReportGenerationController
			Dim sYear = txtYear4.Text
			Dim sMonth = cboMonth4.Text
			oController.createReport04(sYear, sMonth)
		Else
			MsgBox("Error occurs! Cannot create report this time!")
		End If
	End Sub
	'Create Fifth Report button
	Private Sub btnCreateFifthReport_Click(sender As Object, e As EventArgs) Handles btnCreateFifthReport.Click
		Dim bValidation As New Validation
		Dim bIsValid As Boolean
		Dim allValid As Boolean
		allValid = True
		Dim tt As New ToolTip()
		'Year
		bIsValid = bValidation.IsYearValid(txtYear5.Text)
		If bIsValid Then
			picErrorYear5.Visible = False
		Else
			picErrorYear5.Visible = True
			tt.SetToolTip(picErrorYear5, "Please re-enter the year correctly e.g: 2016")
			allValid = False
		End If

		'Month
		bIsValid = bValidation.isNumericVal(cboMonth5.Text)
		If bIsValid Then
			picErrorMonth5.Visible = False
		Else
			picErrorMonth5.Visible = True
			tt.SetToolTip(picErrorMonth5, "Please select month!")
			allValid = False
		End If
		If allValid Then
			Dim oController As ReportGenerationController = New ReportGenerationController
			Dim sYear = txtYear5.Text
			Dim sMonth = cboMonth5.Text
			oController.createReport05(sYear, sMonth)
		Else
			MsgBox("Error occurs! Cannot create report this time!")
		End If
	End Sub
	'Create Sixth Report button
	Private Sub btnCreateSixthReport_Click(sender As Object, e As EventArgs) Handles btnCreateSixthReport.Click
		Dim bValidation As New Validation
		Dim bIsValid As Boolean
		Dim allValid As Boolean
		allValid = True
		Dim tt As New ToolTip()
		bIsValid = bValidation.IsYearValid(txtYear6.Text)
		If bIsValid Then
			PicErrorYear6.Visible = False
		Else
			PicErrorYear6.Visible = True
			tt.SetToolTip(PicErrorYear6, "Please re-enter the year correctly e.g: 2016")
			allValid = False
		End If

		'Month
		bIsValid = bValidation.isNumericVal(cboMonth6.Text)
		If bIsValid Then
			picErrorMonth6.Visible = False
		Else
			picErrorMonth6.Visible = True
			tt.SetToolTip(picErrorMonth6, "Please select month!")
			allValid = False
		End If

		'Room Number
		bIsValid = bValidation.isNumericVal(cboRoomNumber6.Text)
		If bIsValid Then
			picErrorRoomNumber6.Visible = False
		Else
			picErrorRoomNumber6.Visible = True
			tt.SetToolTip(picErrorRoomNumber6, "Please select room number!")
			allValid = False
		End If

		'All valid
		If allValid Then
			Dim oController As ReportGenerationController = New ReportGenerationController
			Dim sYear = txtYear6.Text
			Dim sMonth = cboMonth6.Text
			Dim sRoom = cboRoomNumber6.Text
			oController.createReport06(sYear, sMonth, sRoom)
		Else
			MsgBox("Error occurs! Cannot create report this time!")
		End If
	End Sub
	'Go to form break report
	'Private Sub btnGoToBreakReport_Click(sender As Object, e As EventArgs)
	'Me.Hide()
	'	frmBreakReportGeneration.Show()
	'End Sub
	'Closing Form
	Private Sub frm_Closing(sender As Object, e As System.ComponentModel.CancelEventArgs) Handles MyBase.Closing
		Me.Hide()
		frmBooking.Show()
	End Sub
	'Close button
	Private Sub btnCloseCustomer_Click(sender As Object, e As EventArgs) Handles btnCloseRoom.Click
		'Confirmation before exiting the system
		If MsgBox("Are you sure to close this form?", MsgBoxStyle.YesNo, "Exit Confirmation") = MsgBoxResult.Yes Then
			Me.Close()
			frmBooking.Show()
		ElseIf CBool(MsgBoxResult.No) Then
			Me.Show()
		End If
	End Sub
	'Menu navigation
	Private Sub BookingToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles BookingToolStripMenuItem.Click
		frmBooking.Show()
		Me.Hide()
	End Sub

	Private Sub RoomToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles RoomToolStripMenuItem.Click
		frmRoom.Show()
		Me.Hide()
	End Sub

	Private Sub CustomerToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles CustomerToolStripMenuItem.Click
		frmCustomer.Show()
		Me.Hide()
	End Sub
	Private Sub GenerateReportToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles GenerateNormalReportToolStripMenuItem.Click
		Me.Refresh()
	End Sub

	'Private Sub GenerateBreakReportToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles GenerateBreakReportToolStripMenuItem.Click
	'Me.Hide()
	'	frmBreakReportGeneration.Show()
	'End Sub

End Class