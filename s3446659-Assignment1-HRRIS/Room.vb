﻿Option Explicit On
Option Strict On

Imports System.Data.OleDb
Imports System.IO
Imports System.Drawing

' Name:        Room.vb
' Description: Form for Room details
' Author:      Do Khanh Hoang
' Date:        28/03/2017



Public Class frmRoom
    'Record values from input fields to the database
    Private Sub btnSubmitRoom_Click(sender As Object, e As EventArgs) Handles btnSubmitRoom.Click
        Dim bIsValid = rValidateFormData()

        If bIsValid Then
            Dim htData As Hashtable = New Hashtable
            htData("Type") = txtRoomType.Text
            htData("NumberOfBeds") = txtNumsOfBeds.Text
            htData("RoomNumber") = txtRoomNumber.Text
            htData("Floor") = txtFloor.Text
            htData("Price") = txtRoomPrice.Text
            htData("Availability") = txtRoomAvailability.Text
            htData("Description") = txtRoomDescription.Text

            Dim oRoomController As RoomController = New RoomController
            oRoomController.insert(htData)
        End If
    End Sub
    'Clear values from combobox type
    Private Sub cboType_SelectedIndexChanged(sender As Object, e As EventArgs)
        txtRoomType.Text = ""
        txtNumsOfBeds.Text = ""
        txtRoomNumber.Text = ""
        txtFloor.Text = ""
        txtRoomPrice.Text = ""
        txtRoomAvailability.Text = ""
        txtRoomDescription.Text = ""
    End Sub

	'Validate input values
	'Create function to validate data form
	Private Function rValidateFormData() As Boolean
		Dim rValidation As New Validation
		Dim rIsValid As Boolean
		Dim rAllFieldsValid As Boolean
		rAllFieldsValid = True
		Dim tt As New ToolTip()

		'Type
		rIsValid = rValidation.isAlphaNumericVal(txtRoomType.Text)
		If rIsValid Then
			picErrorRoomType.Visible = False
		Else
			picErrorRoomType.Visible = True
			tt.SetToolTip(picErrorRoomType, "Please check again your Room Type")
			rAllFieldsValid = False
		End If

		'Number of Beds
		rIsValid = rValidation.isNumericVal(txtNumsOfBeds.Text)
		If rIsValid Then
			picErrorRoomNumsOfBeds.Visible = False
		Else
			picErrorRoomNumsOfBeds.Visible = True
			tt.SetToolTip(picErrorRoomNumsOfBeds, "Please check again your Number of Beds")
			rAllFieldsValid = False
		End If

		'Room Number
		rIsValid = rValidation.isNumericVal(txtNumsOfBeds.Text)
		If rIsValid Then
			picErrorRoomNumber.Visible = False
		Else
			picErrorRoomNumber.Visible = True
			tt.SetToolTip(picErrorRoomNumber, "Please check again your Room Number")
			rAllFieldsValid = False
		End If

		'Floor
		rIsValid = rValidation.isNumericVal(txtFloor.Text)
		If rIsValid Then
			picErrorRoomFloor.Visible = False
		Else
			picErrorRoomFloor.Visible = True
			tt.SetToolTip(picErrorRoomFloor, "Please check again your Floor")
			rAllFieldsValid = False
		End If

		'Price
		rIsValid = rValidation.isNumericVal(txtRoomPrice.Text)
		If rIsValid Then
			picErrorRoomPrice.Visible = False
		Else
			picErrorRoomPrice.Visible = True
			tt.SetToolTip(picErrorRoomPrice, "Please check again your Price")
			rAllFieldsValid = False
		End If

		'Availability
		rIsValid = rValidation.isAlphaNumericVal(txtRoomAvailability.Text)
		If rIsValid Then
			picErrorRoomAvailability.Visible = False
		Else
			picErrorRoomAvailability.Visible = True
			tt.SetToolTip(picErrorRoomAvailability, "Please check again your Availability")
			rAllFieldsValid = False
		End If

		'Description
		rIsValid = rValidation.isAlphaNumericVal(txtRoomDescription.Text)
		If rIsValid Then
			picErrorRoomDescription.Visible = False
		Else
			picErrorRoomDescription.Visible = True
			tt.SetToolTip(picErrorRoomDescription, "Please check again your Description")
			rAllFieldsValid = False
		End If


		Return rAllFieldsValid

	End Function
	'Load all data to form Room
	Private Sub frmRoom_Load(sender As Object, e As EventArgs) Handles MyBase.Load
		Dim oController As RoomController = New RoomController
		lsData1 = oController.findAll()

		Dim htData As Hashtable
		Dim iIndex As Integer

		If iCurrentIndex = 0 Then
			btnPrevRoom.Enabled = False
		Else
			btnPrevRoom.Enabled = True
		End If
		If iCurrentIndex = lsData1.Count - 1 Then
			btnNextRoom.Enabled = False
		Else
			btnNextRoom.Enabled = True
		End If
		htData = lsData1.Item(iIndex)

		populateRoomFields(lsData1.Item(0))
	End Sub
	'Declare global variables
	Dim lsData1 As New List(Of Hashtable)
	Dim iCurrentIndex As Integer
	'Populate data to fields
	Private Sub populateRoomFields(htdata As Hashtable)
		txtRoomID.Text = CStr(htdata("RoomID"))
		txtRoomType.Text = CStr(htdata("Type"))
		txtRoomNumber.Text = CStr(htdata("RoomNumber"))
		txtNumsOfBeds.Text = CStr(htdata("NumberOfBeds"))
		txtFloor.Text = CStr(htdata("Floor"))
		txtRoomPrice.Text = CStr(htdata("Price"))
		txtRoomAvailability.Text = CStr(htdata("Availability"))
		txtRoomDescription.Text = CStr(htdata("Description"))


	End Sub
	'Find by roomID
	Private Sub btnFindRoom_Click(sender As Object, e As EventArgs) Handles btnFindRoom.Click
		Dim oController As RoomController = New RoomController

		Dim sID = txtFindRoom.Text
		Dim lsData1 = oController.findByID(sID)
		If lsData1.Count = 1 Then
			populateRoomFields(lsData1.Item(0))
		Else
			MsgBox("No records were found!")
			Debug.Print("No records were found")
		End If
	End Sub
	'Get first record
	Private Sub btnFirst_Click(sender As Object, e As EventArgs) Handles btnFirstRoom.Click
		Dim htData As Hashtable
		Dim iIndex As Integer
		iIndex = 0
		iCurrentIndex = iIndex

		If iCurrentIndex = 0 Then
			btnPrevRoom.Enabled = False
		Else
			btnPrevRoom.Enabled = True
		End If
		If iCurrentIndex = lsData1.Count - 1 Then
			btnNextRoom.Enabled = False
		Else
			btnNextRoom.Enabled = True
		End If
		htData = lsData1.Item(iIndex)

		populateRoomFields(lsData1.Item(iIndex))

	End Sub
	'Go to previous record
	Private Sub btnPrev_Click(sender As Object, e As EventArgs) Handles btnPrevRoom.Click
		Dim htData As Hashtable
		Dim iIndex As Integer
		iIndex = iCurrentIndex - 1
		iCurrentIndex = iIndex

		If iCurrentIndex = 0 Then
			btnPrevRoom.Enabled = False
		Else
			btnPrevRoom.Enabled = True
		End If
		If iCurrentIndex = lsData1.Count - 1 Then
			btnNextRoom.Enabled = False
		Else
			btnNextRoom.Enabled = True
		End If

		htData = lsData1.Item(iIndex)

		populateRoomFields(lsData1.Item(iIndex))
	End Sub
	'Go to next record
	Private Sub btnNext_Click(sender As Object, e As EventArgs) Handles btnNextRoom.Click
		Dim htData As Hashtable
		Dim iIndex As Integer
		iIndex = iCurrentIndex + 1
		iCurrentIndex = iIndex

		If iCurrentIndex = 0 Then
			btnPrevRoom.Enabled = False
		Else
			btnPrevRoom.Enabled = True
		End If
		If iCurrentIndex = lsData1.Count - 1 Then
			btnNextRoom.Enabled = False
		Else
			btnNextRoom.Enabled = True
		End If
		htData = lsData1.Item(iIndex)

		populateRoomFields(lsData1.Item(iIndex))
	End Sub
	'Go to last record
	Private Sub btnLast_Click(sender As Object, e As EventArgs) Handles btnLastRoom.Click
		Dim htData As Hashtable
		Dim iIndex As Integer
		iIndex = lsData1.Count - 1
		iCurrentIndex = iIndex

		If iCurrentIndex = 0 Then
			btnPrevRoom.Enabled = False
		Else
			btnPrevRoom.Enabled = True
		End If
		If iCurrentIndex = lsData1.Count - 1 Then
			btnNextRoom.Enabled = False
		Else
			btnNextRoom.Enabled = True
		End If

		htData = lsData1.Item(iIndex)

		populateRoomFields(lsData1.Item(iIndex))
	End Sub
	'Update
	Private Sub btnUpdate_Click(sender As Object, e As EventArgs) Handles btnUpdateRoom.Click
		Dim oController As RoomController = New RoomController
		Dim iNumRows = oController.update(getFormData)

		If iNumRows = 1 Then
			MsgBox("The record was updated!")
			Debug.Print("The record was updated!")
		Else
			MsgBox("The record was not updated!")
			Debug.Print("The record was not updated!")
		End If

	End Sub
	'Delete
	Private Sub btnDelete_Click(sender As Object, e As EventArgs) Handles btnDeleteRoom.Click
		If MsgBox("Are you sure to delete this record?", MsgBoxStyle.YesNo, "Delete Confirmation") = MsgBoxResult.Yes Then
			Dim oController As RoomController = New RoomController
			Dim sId = txtRoomID.Text
			Dim iNumRows = oController.delete(sId)

			If iNumRows = 1 Then
				clearForm()
				MsgBox("The record was deleted")
				Debug.Print("The record was deleted. Use the find button to check ...")
			Else
				MsgBox("The record was not deleted!")
				Debug.Print("The record was not deleted!")
			End If

		ElseIf CBool(MsgBoxResult.No) Then
			Me.Refresh()
		End If
	End Sub
	'Get data
	Private Function getFormData() As Hashtable
		Dim htData As Hashtable = New Hashtable

		htData("RoomID") = txtRoomID.Text
		htData("Type") = txtRoomType.Text
		htData("NumberOfBeds") = txtNumsOfBeds.Text
		htData("RoomNumber") = txtRoomNumber.Text
		htData("Floor") = txtFloor.Text
		htData("Price") = txtRoomPrice.Text
		htData("Availability") = txtRoomAvailability.Text
		htData("Description") = txtRoomDescription.Text

		Return htData
	End Function
	'Clear form
	Private Sub clearForm()
		txtRoomID.Clear()
		txtRoomType.Clear()
		txtRoomNumber.Clear()
		txtRoomPrice.Clear()
		txtNumsOfBeds.Clear()
		txtRoomAvailability.Clear()
		txtFloor.Clear()
		txtRoomDescription.Clear()
	End Sub
	'Refresh room form
	Private Sub clearRoom()
		txtRoomID.Text = ""
		txtRoomType.Text = ""
		txtNumsOfBeds.Text = ""
		txtRoomNumber.Text = ""
		txtFloor.Text = ""
		txtRoomPrice.Text = ""
		txtRoomAvailability.Text = ""
		txtRoomDescription.Text = ""

		picErrorRoomType.Visible = False
		picErrorRoomNumsOfBeds.Visible = False
		picErrorRoomNumber.Visible = False
		picErrorRoomFloor.Visible = False
		picErrorRoomPrice.Visible = False
		picErrorRoomAvailability.Visible = False
		picErrorRoomDescription.Visible = False


	End Sub
	'Refresh room form
	Private Sub btnClearRoom_Click(sender As Object, e As EventArgs) Handles btnNewRoom.Click
		clearRoom()
	End Sub
	'Close form
	Private Sub btnCloseRoom_Click(sender As Object, e As EventArgs) Handles btnCloseRoom.Click
        'Confirmation before exiting the system
        If MsgBox("Are you sure to close this form?", MsgBoxStyle.YesNo, "Exit Confirmation") = MsgBoxResult.Yes Then
			Me.Close()
			End
		ElseIf CBool(MsgBoxResult.No) Then
            Me.Show()
        End If
    End Sub
	'Navigation
	'Go to customer in Menu
	Private Sub CustomerToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles CustomerToolStripMenuItem.Click
		frmCustomer.Show()
		Me.Hide()
	End Sub
	'Go to room on Menu
	Private Sub RoomToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles RoomToolStripMenuItem.Click
		Me.Refresh()
	End Sub
	'Go to booking
	Private Sub btnGoToBooking_Click(ByVal sender As Object, ByVal e As EventArgs)
		frmBooking.Show()
		Me.Hide()
	End Sub
	'Go to booking on Menu
	Private Sub BookingToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles BookingToolStripMenuItem.Click
		frmBooking.Show()
		Me.Hide()
	End Sub
	'About page
	Private Sub AboutToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles AboutToolStripMenuItem.Click
		Dim oController As PageController = New PageController
		oController.createAboutPage()
	End Sub
	'Help page
	Private Sub HelpToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles HelpToolStripMenuItem.Click
		Dim oController As PageController = New PageController
		oController.createHelpPage()
	End Sub
	'Go to Generate normal report
	Private Sub ReportGenerationToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles GenerateNormalReportToolStripMenuItem.Click
		ReportGeneration.Show()
		Me.Hide()
	End Sub
	'Go to Generate Break Report
	Private Sub GenerateBreakReportToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles GenerateBreakReportToolStripMenuItem.Click
		BreakReport.Show()
		Me.Hide()
	End Sub
End Class