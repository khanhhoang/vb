﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmCustomer
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
		Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmCustomer))
		Me.GroupBox1 = New System.Windows.Forms.GroupBox()
		Me.dtpDOB = New System.Windows.Forms.DateTimePicker()
		Me.picErrorEmail = New System.Windows.Forms.PictureBox()
		Me.picErrorAddress = New System.Windows.Forms.PictureBox()
		Me.picErrorPhone = New System.Windows.Forms.PictureBox()
		Me.picErrorDOB = New System.Windows.Forms.PictureBox()
		Me.picErrorLastName = New System.Windows.Forms.PictureBox()
		Me.picErrorFirstName = New System.Windows.Forms.PictureBox()
		Me.cboGender = New System.Windows.Forms.ComboBox()
		Me.picErrorGender = New System.Windows.Forms.PictureBox()
		Me.txtemail = New System.Windows.Forms.TextBox()
		Me.picErrorTittle = New System.Windows.Forms.PictureBox()
		Me.txtaddress = New System.Windows.Forms.TextBox()
		Me.txtphone = New System.Windows.Forms.TextBox()
		Me.txtlastname = New System.Windows.Forms.TextBox()
		Me.txtfirstname = New System.Windows.Forms.TextBox()
		Me.cboTitle = New System.Windows.Forms.ComboBox()
		Me.txtcustomer_id = New System.Windows.Forms.TextBox()
		Me.lbldob = New System.Windows.Forms.Label()
		Me.lblemail = New System.Windows.Forms.Label()
		Me.lbladdress = New System.Windows.Forms.Label()
		Me.lblphone = New System.Windows.Forms.Label()
		Me.lbllastname = New System.Windows.Forms.Label()
		Me.lblfirstname = New System.Windows.Forms.Label()
		Me.lblgender = New System.Windows.Forms.Label()
		Me.lbltitle = New System.Windows.Forms.Label()
		Me.lblcustomer_id = New System.Windows.Forms.Label()
		Me.btnNewCustomer = New System.Windows.Forms.Button()
		Me.btnSubmitCustomer = New System.Windows.Forms.Button()
		Me.btnCloseCustomer = New System.Windows.Forms.Button()
		Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
		Me.MenuToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
		Me.CustomerToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
		Me.RoomToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
		Me.BookingToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
		Me.ReportToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
		Me.GenerateNormalReportToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
		Me.GenerateBreakReportToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
		Me.HelpToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
		Me.AboutToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
		Me.txtFindCustomer = New System.Windows.Forms.TextBox()
		Me.btnFindCustomer = New System.Windows.Forms.Button()
		Me.btnReadCustomer = New System.Windows.Forms.Button()
		Me.btnUpdateCustomer = New System.Windows.Forms.Button()
		Me.btnDeleteCustomer = New System.Windows.Forms.Button()
		Me.lstCustomer = New System.Windows.Forms.ListBox()
		Me.btnLastCustomer = New System.Windows.Forms.Button()
		Me.btnNextCustomer = New System.Windows.Forms.Button()
		Me.btnPrevCustomer = New System.Windows.Forms.Button()
		Me.btnFirstCustomer = New System.Windows.Forms.Button()
		Me.GroupBox1.SuspendLayout()
		CType(Me.picErrorEmail, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.picErrorAddress, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.picErrorPhone, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.picErrorDOB, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.picErrorLastName, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.picErrorFirstName, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.picErrorGender, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.picErrorTittle, System.ComponentModel.ISupportInitialize).BeginInit()
		Me.MenuStrip1.SuspendLayout()
		Me.SuspendLayout()
		'
		'GroupBox1
		'
		Me.GroupBox1.Controls.Add(Me.dtpDOB)
		Me.GroupBox1.Controls.Add(Me.picErrorEmail)
		Me.GroupBox1.Controls.Add(Me.picErrorAddress)
		Me.GroupBox1.Controls.Add(Me.picErrorPhone)
		Me.GroupBox1.Controls.Add(Me.picErrorDOB)
		Me.GroupBox1.Controls.Add(Me.picErrorLastName)
		Me.GroupBox1.Controls.Add(Me.picErrorFirstName)
		Me.GroupBox1.Controls.Add(Me.cboGender)
		Me.GroupBox1.Controls.Add(Me.picErrorGender)
		Me.GroupBox1.Controls.Add(Me.txtemail)
		Me.GroupBox1.Controls.Add(Me.picErrorTittle)
		Me.GroupBox1.Controls.Add(Me.txtaddress)
		Me.GroupBox1.Controls.Add(Me.txtphone)
		Me.GroupBox1.Controls.Add(Me.txtlastname)
		Me.GroupBox1.Controls.Add(Me.txtfirstname)
		Me.GroupBox1.Controls.Add(Me.cboTitle)
		Me.GroupBox1.Controls.Add(Me.txtcustomer_id)
		Me.GroupBox1.Controls.Add(Me.lbldob)
		Me.GroupBox1.Controls.Add(Me.lblemail)
		Me.GroupBox1.Controls.Add(Me.lbladdress)
		Me.GroupBox1.Controls.Add(Me.lblphone)
		Me.GroupBox1.Controls.Add(Me.lbllastname)
		Me.GroupBox1.Controls.Add(Me.lblfirstname)
		Me.GroupBox1.Controls.Add(Me.lblgender)
		Me.GroupBox1.Controls.Add(Me.lbltitle)
		Me.GroupBox1.Controls.Add(Me.lblcustomer_id)
		Me.GroupBox1.Location = New System.Drawing.Point(0, 22)
		Me.GroupBox1.Name = "GroupBox1"
		Me.GroupBox1.Size = New System.Drawing.Size(430, 288)
		Me.GroupBox1.TabIndex = 0
		Me.GroupBox1.TabStop = False
		Me.GroupBox1.Text = "Customer Details"
		'
		'dtpDOB
		'
		Me.dtpDOB.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
		Me.dtpDOB.Location = New System.Drawing.Point(113, 257)
		Me.dtpDOB.Name = "dtpDOB"
		Me.dtpDOB.Size = New System.Drawing.Size(215, 20)
		Me.dtpDOB.TabIndex = 115
		'
		'picErrorEmail
		'
		Me.picErrorEmail.Image = CType(resources.GetObject("picErrorEmail.Image"), System.Drawing.Image)
		Me.picErrorEmail.Location = New System.Drawing.Point(334, 231)
		Me.picErrorEmail.Name = "picErrorEmail"
		Me.picErrorEmail.Size = New System.Drawing.Size(20, 19)
		Me.picErrorEmail.TabIndex = 105
		Me.picErrorEmail.TabStop = False
		Me.picErrorEmail.Visible = False
		'
		'picErrorAddress
		'
		Me.picErrorAddress.Image = CType(resources.GetObject("picErrorAddress.Image"), System.Drawing.Image)
		Me.picErrorAddress.Location = New System.Drawing.Point(334, 200)
		Me.picErrorAddress.Name = "picErrorAddress"
		Me.picErrorAddress.Size = New System.Drawing.Size(20, 19)
		Me.picErrorAddress.TabIndex = 114
		Me.picErrorAddress.TabStop = False
		Me.picErrorAddress.Visible = False
		'
		'picErrorPhone
		'
		Me.picErrorPhone.Image = CType(resources.GetObject("picErrorPhone.Image"), System.Drawing.Image)
		Me.picErrorPhone.Location = New System.Drawing.Point(334, 171)
		Me.picErrorPhone.Name = "picErrorPhone"
		Me.picErrorPhone.Size = New System.Drawing.Size(20, 19)
		Me.picErrorPhone.TabIndex = 113
		Me.picErrorPhone.TabStop = False
		Me.picErrorPhone.Visible = False
		'
		'picErrorDOB
		'
		Me.picErrorDOB.Image = CType(resources.GetObject("picErrorDOB.Image"), System.Drawing.Image)
		Me.picErrorDOB.Location = New System.Drawing.Point(334, 257)
		Me.picErrorDOB.Name = "picErrorDOB"
		Me.picErrorDOB.Size = New System.Drawing.Size(20, 19)
		Me.picErrorDOB.TabIndex = 112
		Me.picErrorDOB.TabStop = False
		Me.picErrorDOB.Visible = False
		'
		'picErrorLastName
		'
		Me.picErrorLastName.Image = CType(resources.GetObject("picErrorLastName.Image"), System.Drawing.Image)
		Me.picErrorLastName.Location = New System.Drawing.Point(334, 143)
		Me.picErrorLastName.Name = "picErrorLastName"
		Me.picErrorLastName.Size = New System.Drawing.Size(20, 19)
		Me.picErrorLastName.TabIndex = 111
		Me.picErrorLastName.TabStop = False
		Me.picErrorLastName.Visible = False
		'
		'picErrorFirstName
		'
		Me.picErrorFirstName.Image = CType(resources.GetObject("picErrorFirstName.Image"), System.Drawing.Image)
		Me.picErrorFirstName.Location = New System.Drawing.Point(334, 114)
		Me.picErrorFirstName.Name = "picErrorFirstName"
		Me.picErrorFirstName.Size = New System.Drawing.Size(20, 19)
		Me.picErrorFirstName.TabIndex = 110
		Me.picErrorFirstName.TabStop = False
		Me.picErrorFirstName.Visible = False
		'
		'cboGender
		'
		Me.cboGender.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
		Me.cboGender.Font = New System.Drawing.Font("Arial", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.cboGender.FormattingEnabled = True
		Me.cboGender.Items.AddRange(New Object() {"Male", "Female"})
		Me.cboGender.Location = New System.Drawing.Point(113, 82)
		Me.cboGender.Name = "cboGender"
		Me.cboGender.Size = New System.Drawing.Size(122, 25)
		Me.cboGender.TabIndex = 109
		'
		'picErrorGender
		'
		Me.picErrorGender.Image = CType(resources.GetObject("picErrorGender.Image"), System.Drawing.Image)
		Me.picErrorGender.Location = New System.Drawing.Point(241, 84)
		Me.picErrorGender.Name = "picErrorGender"
		Me.picErrorGender.Size = New System.Drawing.Size(20, 19)
		Me.picErrorGender.TabIndex = 108
		Me.picErrorGender.TabStop = False
		Me.picErrorGender.Visible = False
		'
		'txtemail
		'
		Me.txtemail.Location = New System.Drawing.Point(113, 229)
		Me.txtemail.Name = "txtemail"
		Me.txtemail.Size = New System.Drawing.Size(215, 20)
		Me.txtemail.TabIndex = 104
		'
		'picErrorTittle
		'
		Me.picErrorTittle.Image = CType(resources.GetObject("picErrorTittle.Image"), System.Drawing.Image)
		Me.picErrorTittle.Location = New System.Drawing.Point(241, 54)
		Me.picErrorTittle.Name = "picErrorTittle"
		Me.picErrorTittle.Size = New System.Drawing.Size(20, 19)
		Me.picErrorTittle.TabIndex = 107
		Me.picErrorTittle.TabStop = False
		Me.picErrorTittle.Visible = False
		'
		'txtaddress
		'
		Me.txtaddress.Location = New System.Drawing.Point(113, 200)
		Me.txtaddress.Name = "txtaddress"
		Me.txtaddress.Size = New System.Drawing.Size(215, 20)
		Me.txtaddress.TabIndex = 103
		'
		'txtphone
		'
		Me.txtphone.Location = New System.Drawing.Point(113, 171)
		Me.txtphone.Name = "txtphone"
		Me.txtphone.Size = New System.Drawing.Size(215, 20)
		Me.txtphone.TabIndex = 102
		'
		'txtlastname
		'
		Me.txtlastname.Location = New System.Drawing.Point(113, 142)
		Me.txtlastname.Name = "txtlastname"
		Me.txtlastname.Size = New System.Drawing.Size(215, 20)
		Me.txtlastname.TabIndex = 101
		'
		'txtfirstname
		'
		Me.txtfirstname.Location = New System.Drawing.Point(113, 113)
		Me.txtfirstname.Name = "txtfirstname"
		Me.txtfirstname.Size = New System.Drawing.Size(215, 20)
		Me.txtfirstname.TabIndex = 100
		'
		'cboTitle
		'
		Me.cboTitle.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
		Me.cboTitle.Font = New System.Drawing.Font("Arial", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.cboTitle.FormattingEnabled = True
		Me.cboTitle.ItemHeight = 17
		Me.cboTitle.Items.AddRange(New Object() {"Mr", "Mrs", "Ms", "Miss"})
		Me.cboTitle.Location = New System.Drawing.Point(113, 52)
		Me.cboTitle.Name = "cboTitle"
		Me.cboTitle.Size = New System.Drawing.Size(122, 25)
		Me.cboTitle.TabIndex = 106
		'
		'txtcustomer_id
		'
		Me.txtcustomer_id.Location = New System.Drawing.Point(113, 26)
		Me.txtcustomer_id.Name = "txtcustomer_id"
		Me.txtcustomer_id.Size = New System.Drawing.Size(215, 20)
		Me.txtcustomer_id.TabIndex = 99
		'
		'lbldob
		'
		Me.lbldob.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.lbldob.Location = New System.Drawing.Point(32, 258)
		Me.lbldob.Name = "lbldob"
		Me.lbldob.Size = New System.Drawing.Size(75, 20)
		Me.lbldob.TabIndex = 17
		Me.lbldob.Text = "DOB"
		Me.lbldob.TextAlign = System.Drawing.ContentAlignment.MiddleRight
		'
		'lblemail
		'
		Me.lblemail.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.lblemail.Location = New System.Drawing.Point(32, 229)
		Me.lblemail.Name = "lblemail"
		Me.lblemail.Size = New System.Drawing.Size(75, 20)
		Me.lblemail.TabIndex = 16
		Me.lblemail.Text = "Email"
		Me.lblemail.TextAlign = System.Drawing.ContentAlignment.MiddleRight
		'
		'lbladdress
		'
		Me.lbladdress.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.lbladdress.Location = New System.Drawing.Point(32, 200)
		Me.lbladdress.Name = "lbladdress"
		Me.lbladdress.Size = New System.Drawing.Size(75, 20)
		Me.lbladdress.TabIndex = 15
		Me.lbladdress.Text = "Address"
		Me.lbladdress.TextAlign = System.Drawing.ContentAlignment.MiddleRight
		'
		'lblphone
		'
		Me.lblphone.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.lblphone.Location = New System.Drawing.Point(32, 171)
		Me.lblphone.Name = "lblphone"
		Me.lblphone.Size = New System.Drawing.Size(75, 20)
		Me.lblphone.TabIndex = 14
		Me.lblphone.Text = "Phone"
		Me.lblphone.TextAlign = System.Drawing.ContentAlignment.MiddleRight
		'
		'lbllastname
		'
		Me.lbllastname.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.lbllastname.Location = New System.Drawing.Point(32, 142)
		Me.lbllastname.Name = "lbllastname"
		Me.lbllastname.Size = New System.Drawing.Size(75, 20)
		Me.lbllastname.TabIndex = 13
		Me.lbllastname.Text = "Lastname"
		Me.lbllastname.TextAlign = System.Drawing.ContentAlignment.MiddleRight
		'
		'lblfirstname
		'
		Me.lblfirstname.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.lblfirstname.Location = New System.Drawing.Point(32, 113)
		Me.lblfirstname.Name = "lblfirstname"
		Me.lblfirstname.Size = New System.Drawing.Size(75, 20)
		Me.lblfirstname.TabIndex = 12
		Me.lblfirstname.Text = "Firstname"
		Me.lblfirstname.TextAlign = System.Drawing.ContentAlignment.MiddleRight
		'
		'lblgender
		'
		Me.lblgender.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.lblgender.Location = New System.Drawing.Point(32, 84)
		Me.lblgender.Name = "lblgender"
		Me.lblgender.Size = New System.Drawing.Size(75, 20)
		Me.lblgender.TabIndex = 11
		Me.lblgender.Text = "Gender"
		Me.lblgender.TextAlign = System.Drawing.ContentAlignment.MiddleRight
		'
		'lbltitle
		'
		Me.lbltitle.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.lbltitle.Location = New System.Drawing.Point(32, 55)
		Me.lbltitle.Name = "lbltitle"
		Me.lbltitle.Size = New System.Drawing.Size(75, 20)
		Me.lbltitle.TabIndex = 10
		Me.lbltitle.Text = "Title"
		Me.lbltitle.TextAlign = System.Drawing.ContentAlignment.MiddleRight
		'
		'lblcustomer_id
		'
		Me.lblcustomer_id.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.lblcustomer_id.Location = New System.Drawing.Point(32, 26)
		Me.lblcustomer_id.Name = "lblcustomer_id"
		Me.lblcustomer_id.Size = New System.Drawing.Size(75, 20)
		Me.lblcustomer_id.TabIndex = 9
		Me.lblcustomer_id.Text = "CustomerID"
		Me.lblcustomer_id.TextAlign = System.Drawing.ContentAlignment.MiddleRight
		'
		'btnNewCustomer
		'
		Me.btnNewCustomer.Location = New System.Drawing.Point(436, 53)
		Me.btnNewCustomer.Name = "btnNewCustomer"
		Me.btnNewCustomer.Size = New System.Drawing.Size(105, 23)
		Me.btnNewCustomer.TabIndex = 1
		Me.btnNewCustomer.Text = "New"
		Me.btnNewCustomer.UseVisualStyleBackColor = True
		'
		'btnSubmitCustomer
		'
		Me.btnSubmitCustomer.Location = New System.Drawing.Point(547, 53)
		Me.btnSubmitCustomer.Name = "btnSubmitCustomer"
		Me.btnSubmitCustomer.Size = New System.Drawing.Size(105, 23)
		Me.btnSubmitCustomer.TabIndex = 2
		Me.btnSubmitCustomer.Text = "Submit"
		Me.btnSubmitCustomer.UseVisualStyleBackColor = True
		'
		'btnCloseCustomer
		'
		Me.btnCloseCustomer.Location = New System.Drawing.Point(688, 277)
		Me.btnCloseCustomer.Name = "btnCloseCustomer"
		Me.btnCloseCustomer.Size = New System.Drawing.Size(75, 23)
		Me.btnCloseCustomer.TabIndex = 3
		Me.btnCloseCustomer.Text = "Close"
		Me.btnCloseCustomer.UseVisualStyleBackColor = True
		'
		'MenuStrip1
		'
		Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.MenuToolStripMenuItem, Me.ReportToolStripMenuItem, Me.HelpToolStripMenuItem, Me.AboutToolStripMenuItem})
		Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
		Me.MenuStrip1.Name = "MenuStrip1"
		Me.MenuStrip1.Size = New System.Drawing.Size(1084, 24)
		Me.MenuStrip1.TabIndex = 14
		Me.MenuStrip1.Text = "MenuStrip1"
		'
		'MenuToolStripMenuItem
		'
		Me.MenuToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.CustomerToolStripMenuItem, Me.RoomToolStripMenuItem, Me.BookingToolStripMenuItem})
		Me.MenuToolStripMenuItem.Name = "MenuToolStripMenuItem"
		Me.MenuToolStripMenuItem.Size = New System.Drawing.Size(50, 20)
		Me.MenuToolStripMenuItem.Text = "Menu"
		'
		'CustomerToolStripMenuItem
		'
		Me.CustomerToolStripMenuItem.Name = "CustomerToolStripMenuItem"
		Me.CustomerToolStripMenuItem.Size = New System.Drawing.Size(126, 22)
		Me.CustomerToolStripMenuItem.Text = "Customer"
		'
		'RoomToolStripMenuItem
		'
		Me.RoomToolStripMenuItem.Name = "RoomToolStripMenuItem"
		Me.RoomToolStripMenuItem.Size = New System.Drawing.Size(126, 22)
		Me.RoomToolStripMenuItem.Text = "Room"
		'
		'BookingToolStripMenuItem
		'
		Me.BookingToolStripMenuItem.Name = "BookingToolStripMenuItem"
		Me.BookingToolStripMenuItem.Size = New System.Drawing.Size(126, 22)
		Me.BookingToolStripMenuItem.Text = "Booking"
		'
		'ReportToolStripMenuItem
		'
		Me.ReportToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.GenerateNormalReportToolStripMenuItem, Me.GenerateBreakReportToolStripMenuItem})
		Me.ReportToolStripMenuItem.Name = "ReportToolStripMenuItem"
		Me.ReportToolStripMenuItem.Size = New System.Drawing.Size(54, 20)
		Me.ReportToolStripMenuItem.Text = "Report"
		'
		'GenerateNormalReportToolStripMenuItem
		'
		Me.GenerateNormalReportToolStripMenuItem.Name = "GenerateNormalReportToolStripMenuItem"
		Me.GenerateNormalReportToolStripMenuItem.Size = New System.Drawing.Size(202, 22)
		Me.GenerateNormalReportToolStripMenuItem.Text = "Generate Normal Report"
		'
		'GenerateBreakReportToolStripMenuItem
		'
		Me.GenerateBreakReportToolStripMenuItem.Name = "GenerateBreakReportToolStripMenuItem"
		Me.GenerateBreakReportToolStripMenuItem.Size = New System.Drawing.Size(202, 22)
		Me.GenerateBreakReportToolStripMenuItem.Text = "Generate Break Report"
		'
		'HelpToolStripMenuItem
		'
		Me.HelpToolStripMenuItem.Name = "HelpToolStripMenuItem"
		Me.HelpToolStripMenuItem.Size = New System.Drawing.Size(44, 20)
		Me.HelpToolStripMenuItem.Text = "Help"
		'
		'AboutToolStripMenuItem
		'
		Me.AboutToolStripMenuItem.Name = "AboutToolStripMenuItem"
		Me.AboutToolStripMenuItem.Size = New System.Drawing.Size(52, 20)
		Me.AboutToolStripMenuItem.Text = "About"
		'
		'txtFindCustomer
		'
		Me.txtFindCustomer.Font = New System.Drawing.Font("Arial", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.txtFindCustomer.Location = New System.Drawing.Point(436, 27)
		Me.txtFindCustomer.Multiline = True
		Me.txtFindCustomer.Name = "txtFindCustomer"
		Me.txtFindCustomer.Size = New System.Drawing.Size(473, 20)
		Me.txtFindCustomer.TabIndex = 90
		'
		'btnFindCustomer
		'
		Me.btnFindCustomer.BackColor = System.Drawing.Color.Transparent
		Me.btnFindCustomer.Font = New System.Drawing.Font("Arial", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.btnFindCustomer.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
		Me.btnFindCustomer.Location = New System.Drawing.Point(929, 24)
		Me.btnFindCustomer.Name = "btnFindCustomer"
		Me.btnFindCustomer.Size = New System.Drawing.Size(62, 25)
		Me.btnFindCustomer.TabIndex = 105
		Me.btnFindCustomer.Text = "Find"
		Me.btnFindCustomer.UseVisualStyleBackColor = False
		'
		'btnReadCustomer
		'
		Me.btnReadCustomer.Location = New System.Drawing.Point(658, 53)
		Me.btnReadCustomer.Name = "btnReadCustomer"
		Me.btnReadCustomer.Size = New System.Drawing.Size(105, 23)
		Me.btnReadCustomer.TabIndex = 106
		Me.btnReadCustomer.Text = "Read"
		Me.btnReadCustomer.UseVisualStyleBackColor = True
		'
		'btnUpdateCustomer
		'
		Me.btnUpdateCustomer.Location = New System.Drawing.Point(769, 53)
		Me.btnUpdateCustomer.Name = "btnUpdateCustomer"
		Me.btnUpdateCustomer.Size = New System.Drawing.Size(105, 23)
		Me.btnUpdateCustomer.TabIndex = 107
		Me.btnUpdateCustomer.Text = "Update"
		Me.btnUpdateCustomer.UseVisualStyleBackColor = True
		'
		'btnDeleteCustomer
		'
		Me.btnDeleteCustomer.Location = New System.Drawing.Point(880, 55)
		Me.btnDeleteCustomer.Name = "btnDeleteCustomer"
		Me.btnDeleteCustomer.Size = New System.Drawing.Size(105, 23)
		Me.btnDeleteCustomer.TabIndex = 108
		Me.btnDeleteCustomer.Text = "Delete"
		Me.btnDeleteCustomer.UseVisualStyleBackColor = True
		'
		'lstCustomer
		'
		Me.lstCustomer.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.lstCustomer.FormattingEnabled = True
		Me.lstCustomer.ItemHeight = 15
		Me.lstCustomer.Location = New System.Drawing.Point(436, 84)
		Me.lstCustomer.Name = "lstCustomer"
		Me.lstCustomer.Size = New System.Drawing.Size(596, 184)
		Me.lstCustomer.TabIndex = 109
		'
		'btnLastCustomer
		'
		Me.btnLastCustomer.Location = New System.Drawing.Point(303, 305)
		Me.btnLastCustomer.Name = "btnLastCustomer"
		Me.btnLastCustomer.Size = New System.Drawing.Size(70, 30)
		Me.btnLastCustomer.TabIndex = 113
		Me.btnLastCustomer.Text = ">|"
		Me.btnLastCustomer.UseVisualStyleBackColor = True
		'
		'btnNextCustomer
		'
		Me.btnNextCustomer.Location = New System.Drawing.Point(207, 305)
		Me.btnNextCustomer.Name = "btnNextCustomer"
		Me.btnNextCustomer.Size = New System.Drawing.Size(70, 30)
		Me.btnNextCustomer.TabIndex = 112
		Me.btnNextCustomer.Text = ">"
		Me.btnNextCustomer.UseVisualStyleBackColor = True
		'
		'btnPrevCustomer
		'
		Me.btnPrevCustomer.Location = New System.Drawing.Point(113, 305)
		Me.btnPrevCustomer.Name = "btnPrevCustomer"
		Me.btnPrevCustomer.Size = New System.Drawing.Size(70, 30)
		Me.btnPrevCustomer.TabIndex = 111
		Me.btnPrevCustomer.Text = "<"
		Me.btnPrevCustomer.UseVisualStyleBackColor = True
		'
		'btnFirstCustomer
		'
		Me.btnFirstCustomer.Location = New System.Drawing.Point(22, 305)
		Me.btnFirstCustomer.Name = "btnFirstCustomer"
		Me.btnFirstCustomer.Size = New System.Drawing.Size(70, 30)
		Me.btnFirstCustomer.TabIndex = 110
		Me.btnFirstCustomer.Text = "|<"
		Me.btnFirstCustomer.UseVisualStyleBackColor = True
		'
		'frmCustomer
		'
		Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
		Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
		Me.ClientSize = New System.Drawing.Size(1084, 361)
		Me.Controls.Add(Me.btnLastCustomer)
		Me.Controls.Add(Me.btnNextCustomer)
		Me.Controls.Add(Me.btnPrevCustomer)
		Me.Controls.Add(Me.btnFirstCustomer)
		Me.Controls.Add(Me.lstCustomer)
		Me.Controls.Add(Me.btnDeleteCustomer)
		Me.Controls.Add(Me.btnUpdateCustomer)
		Me.Controls.Add(Me.btnReadCustomer)
		Me.Controls.Add(Me.btnFindCustomer)
		Me.Controls.Add(Me.txtFindCustomer)
		Me.Controls.Add(Me.btnCloseCustomer)
		Me.Controls.Add(Me.btnSubmitCustomer)
		Me.Controls.Add(Me.btnNewCustomer)
		Me.Controls.Add(Me.GroupBox1)
		Me.Controls.Add(Me.MenuStrip1)
		Me.MainMenuStrip = Me.MenuStrip1
		Me.Name = "frmCustomer"
		Me.Text = "Customer"
		Me.GroupBox1.ResumeLayout(False)
		Me.GroupBox1.PerformLayout()
		CType(Me.picErrorEmail, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.picErrorAddress, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.picErrorPhone, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.picErrorDOB, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.picErrorLastName, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.picErrorFirstName, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.picErrorGender, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.picErrorTittle, System.ComponentModel.ISupportInitialize).EndInit()
		Me.MenuStrip1.ResumeLayout(False)
		Me.MenuStrip1.PerformLayout()
		Me.ResumeLayout(False)
		Me.PerformLayout()

	End Sub
	Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents dtpDOB As System.Windows.Forms.DateTimePicker
    Friend WithEvents picErrorEmail As System.Windows.Forms.PictureBox
    Friend WithEvents picErrorAddress As System.Windows.Forms.PictureBox
    Friend WithEvents picErrorPhone As System.Windows.Forms.PictureBox
    Friend WithEvents picErrorDOB As System.Windows.Forms.PictureBox
    Friend WithEvents picErrorLastName As System.Windows.Forms.PictureBox
    Friend WithEvents picErrorFirstName As System.Windows.Forms.PictureBox
    Friend WithEvents cboGender As System.Windows.Forms.ComboBox
    Friend WithEvents picErrorGender As System.Windows.Forms.PictureBox
    Friend WithEvents txtemail As System.Windows.Forms.TextBox
    Friend WithEvents picErrorTittle As System.Windows.Forms.PictureBox
    Friend WithEvents txtaddress As System.Windows.Forms.TextBox
    Friend WithEvents txtphone As System.Windows.Forms.TextBox
    Friend WithEvents txtlastname As System.Windows.Forms.TextBox
    Friend WithEvents txtfirstname As System.Windows.Forms.TextBox
    Friend WithEvents cboTitle As System.Windows.Forms.ComboBox
    Friend WithEvents txtcustomer_id As System.Windows.Forms.TextBox
    Friend WithEvents lbldob As System.Windows.Forms.Label
    Friend WithEvents lblemail As System.Windows.Forms.Label
    Friend WithEvents lbladdress As System.Windows.Forms.Label
    Friend WithEvents lblphone As System.Windows.Forms.Label
    Friend WithEvents lbllastname As System.Windows.Forms.Label
    Friend WithEvents lblfirstname As System.Windows.Forms.Label
    Friend WithEvents lblgender As System.Windows.Forms.Label
    Friend WithEvents lbltitle As System.Windows.Forms.Label
    Friend WithEvents lblcustomer_id As System.Windows.Forms.Label
    Friend WithEvents btnNewCustomer As System.Windows.Forms.Button
    Friend WithEvents btnSubmitCustomer As System.Windows.Forms.Button
    Friend WithEvents btnCloseCustomer As System.Windows.Forms.Button
	Friend WithEvents MenuStrip1 As MenuStrip
	Friend WithEvents MenuToolStripMenuItem As ToolStripMenuItem
	Friend WithEvents CustomerToolStripMenuItem As ToolStripMenuItem
	Friend WithEvents RoomToolStripMenuItem As ToolStripMenuItem
	Friend WithEvents BookingToolStripMenuItem As ToolStripMenuItem
	Friend WithEvents ReportToolStripMenuItem As ToolStripMenuItem
	Friend WithEvents GenerateNormalReportToolStripMenuItem As ToolStripMenuItem
	Friend WithEvents GenerateBreakReportToolStripMenuItem As ToolStripMenuItem
	Friend WithEvents HelpToolStripMenuItem As ToolStripMenuItem
	Friend WithEvents AboutToolStripMenuItem As ToolStripMenuItem
	Friend WithEvents txtFindCustomer As TextBox
	Friend WithEvents btnFindCustomer As Button
	Friend WithEvents btnReadCustomer As Button
	Friend WithEvents btnUpdateCustomer As Button
	Friend WithEvents btnDeleteCustomer As Button
	Friend WithEvents lstCustomer As ListBox
	Friend WithEvents btnLastCustomer As Button
	Friend WithEvents btnNextCustomer As Button
	Friend WithEvents btnPrevCustomer As Button
	Friend WithEvents btnFirstCustomer As Button
End Class
