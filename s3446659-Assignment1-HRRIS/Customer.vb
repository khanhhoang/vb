﻿Option Explicit On
Option Strict On

Imports System.Data.OleDb
Imports System.IO
Imports System.Drawing


' Name:        Customer.vb
' Description: Form for Customer Details
' Author:      Do Khanh Hoang
' Date:        28/03/2017



Public Class frmCustomer
    'Record values from input fields to the database
    Private Sub btnSubmitCustomer_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSubmitCustomer.Click
        Dim cIsvalid = cValidateFormData()

        If cIsvalid Then
            Dim htData As Hashtable = New Hashtable
            htData("Title") = cboTitle.Text
            htData("Gender") = cboGender.Text
            htData("FirstName") = txtFirstName.Text
            htData("LastName") = txtLastName.Text
            htData("Phone") = txtPhone.Text
            htData("Address") = txtAddress.Text
            htData("Email") = txtEmail.Text
            htData("DOB") = dtpDOB.Text

            Dim oCustomerController As CustomerController = New CustomerController
            oCustomerController.insert(htData)
        End If
    End Sub

	'Validate input value whether it's correct or not
	'Create function to validate data form
	Private Function cValidateFormData() As Boolean
		Dim cValidation As New Validation
		Dim cIsValid As Boolean
		Dim cAllFieldsValid As Boolean
		cAllFieldsValid = True
		Dim tt As New ToolTip()

		'Title
		cIsValid = cValidation.isAlphaNumericVal(cboTitle.Text)
		If cIsValid Then
			picErrorTittle.Visible = False
		Else
			picErrorTittle.Visible = True
			tt.SetToolTip(picErrorTittle, "Please select the title")
			cAllFieldsValid = False
		End If

		'Gender
		cIsValid = cValidation.isAlphaNumericVal(cboGender.Text)
		If cIsValid Then
			picErrorGender.Visible = False
		Else
			picErrorGender.Visible = True
			tt.SetToolTip(picErrorGender, "Please select the gender")
			cAllFieldsValid = False
		End If

		'First Name
		cIsValid = cValidation.isAlphaNumericVal(txtFirstName.Text)
		If cIsValid Then
			picErrorFirstName.Visible = False
		Else
			picErrorFirstName.Visible = True
			tt.SetToolTip(picErrorFirstName, "Please check again your Firstname")
			cAllFieldsValid = False
		End If

		'Last Name
		cIsValid = cValidation.isAlphaNumericVal(txtLastName.Text)
		If cIsValid Then
			picErrorLastName.Visible = False
		Else
			picErrorLastName.Visible = True
			tt.SetToolTip(picErrorLastName, "Please check again your Lastname")
			cAllFieldsValid = False
		End If

		'Phone
		cIsValid = cValidation.isNumericVal(txtPhone.Text)
		If cIsValid Then
			picErrorPhone.Visible = False
		Else
			picErrorPhone.Visible = True
			tt.SetToolTip(picErrorPhone, "Your phone must be in number")
			cAllFieldsValid = False
		End If

		'Address
		cIsValid = cValidation.isAlphaNumericVal(txtAddress.Text)
		If cIsValid Then
			picErrorAddress.Visible = False
		Else
			picErrorAddress.Visible = True
			tt.SetToolTip(picErrorAddress, "Value input is not valid")
			cAllFieldsValid = False
		End If

		'Email
		cIsValid = cValidation.isValidEmail(txtEmail.Text)
		If cIsValid Then
			picErrorEmail.Visible = False
		Else
			picErrorEmail.Visible = True
			tt.SetToolTip(picErrorEmail, "Please type your email in the format name@example.com")
			cAllFieldsValid = False
		End If

		'DOB
		If dtpDOB.Value <= DateValue("1/31/2004") Then
			cIsValid = True
			picErrorDOB.Visible = False
		Else
			picErrorDOB.Visible = True
			tt.SetToolTip(picErrorDOB, "Please check again your dob!!!")
			cAllFieldsValid = False
		End If

		'All fields are valid
		If cAllFieldsValid Then
			Debug.Print("All fields are valid")
		Else
			MsgBox("One of the fields was invalid")
		End If

		Return cAllFieldsValid

	End Function
	'Populate data to fields
	Private Sub populateCustomerFields(htdata As Hashtable)
		txtcustomer_id.Text = CStr(htdata("CustomerID"))
		cboTitle.Text = CStr(htdata("Title"))
		cboGender.Text = CStr(htdata("Gender"))
		txtfirstname.Text = CStr(htdata("FirstName"))
		txtlastname.Text = CStr(htdata("LastName"))
		txtphone.Text = CStr(htdata("Phone"))
		txtaddress.Text = CStr(htdata("Address"))
		txtemail.Text = CStr(htdata("Email"))
		dtpDOB.Text = CStr(htdata("DOB"))
	End Sub
	'Load all data to form Customer
	Private Sub frmCustomer_Load(sender As Object, e As EventArgs) Handles MyBase.Load
		Dim oController As CustomerController = New CustomerController
		lsData = oController.findAll()

		Dim htData As Hashtable
		Dim iIndex As Integer

		If iCurrentIndex = 0 Then
			btnPrevCustomer.Enabled = False
		Else
			btnPrevCustomer.Enabled = True
		End If
		If iCurrentIndex = lsData.Count - 1 Then
			btnNextCustomer.Enabled = False
		Else
			btnNextCustomer.Enabled = True
		End If
		htData = lsData.Item(iIndex)

		populateCustomerFields(lsData.Item(0))
	End Sub
	'Find by firstname
	Private Sub btnFindCustomer_Click(sender As Object, e As EventArgs) Handles btnFindCustomer.Click
		Dim oController As CustomerController = New CustomerController

		Dim sFirstName = txtFindCustomer.Text
		Dim lsData = oController.findByFirstName(sFirstName)

		If lsData.Count = 1 Then
			populateCustomerFields(lsData.Item(0))
		ElseIf lsData.Count > 1 Then
			Dim sCustomerDetails As String
			For Each customer In lsData
				sCustomerDetails = CStr(customer("CustomerID"))
				sCustomerDetails = sCustomerDetails & " | " & CStr(customer("Title"))
				sCustomerDetails = sCustomerDetails & " | " & CStr(customer("Gender"))
				sCustomerDetails = sCustomerDetails & " | " & CStr(customer("FirstName"))
				sCustomerDetails = sCustomerDetails & " | " & CStr(customer("LastName"))
				sCustomerDetails = sCustomerDetails & " | " & CInt(customer("Phone"))
				sCustomerDetails = sCustomerDetails & " | " & CStr(customer("Address"))
				sCustomerDetails = sCustomerDetails & " | " & CStr(customer("Email"))
				sCustomerDetails = sCustomerDetails & " | " & CDate(customer("DOB"))

				lstCustomer.Items.Add(sCustomerDetails)
			Next
		Else
			MsgBox("No records were found")
			Debug.Print("No records were found")
			clearCustomer()
		End If

	End Sub
	'Read data
	Private Sub btnRead_Click(sender As Object, e As EventArgs) Handles btnReadCustomer.Click
		Dim oController As CustomerController = New CustomerController

		Dim sID = txtcustomer_id.Text
		Dim lsData = oController.findByID(sID)
		If lsData.Count = 1 Then
			populateCustomerFields(lsData.Item(0))
		Else
			Debug.Print("No records were found")
		End If

	End Sub
	'Update
	Private Sub btnUpdate_Click(sender As Object, e As EventArgs) Handles btnUpdateCustomer.Click
		Dim oController As CustomerController = New CustomerController
		Dim iNumRows = oController.update(getFormData)

		If iNumRows = 1 Then
			MsgBox("The record was updated!")
			Debug.Print("The record was updated!")
		Else
			MsgBox("The record was not updated!")
			Debug.Print("The record was not updated!")
		End If

	End Sub
	'Delete
	Private Sub btnDelete_Click(sender As Object, e As EventArgs) Handles btnDeleteCustomer.Click
		If MsgBox("Are you sure to delete this record?", MsgBoxStyle.YesNo, "Delete Confirmation") = MsgBoxResult.Yes Then
			Dim oController As CustomerController = New CustomerController
			Dim sId = txtcustomer_id.Text
			Dim iNumRows = oController.delete(sId)

			If iNumRows = 1 Then
				clearForm()
				MsgBox("The record was deleted")
				Debug.Print("The record was deleted. Use the find button to check ...")
			Else
				MsgBox("The record was not deleted!")
				Debug.Print("The record was not deleted!")
			End If

		ElseIf CBool(MsgBoxResult.No) Then
			Me.Refresh()
		End If
	End Sub
	'Get data
	Private Function getFormData() As Hashtable
		Dim htData As Hashtable = New Hashtable

		htData("CustomerID") = txtcustomer_id.Text

		htData("Title") = cboTitle.Text
		htData("Gender") = cboGender.Text
		htData("FirstName") = txtfirstname.Text
		htData("LastName") = txtlastname.Text
		htData("Phone") = txtphone.Text
		htData("Address") = txtaddress.Text
		htData("Email") = txtemail.Text
		htData("DOB") = dtpDOB.Text

		Return htData
	End Function
	'Declare global variables
	Dim lsData As New List(Of Hashtable)
	Dim iCurrentIndex As Integer
	'Get first record
	Private Sub btnFirst_Click(sender As Object, e As EventArgs) Handles btnFirstCustomer.Click
		Dim htData As Hashtable
		Dim iIndex As Integer
		iIndex = 0
		iCurrentIndex = iIndex

		If iCurrentIndex = 0 Then
			btnPrevCustomer.Enabled = False
		Else
			btnPrevCustomer.Enabled = True
		End If
		If iCurrentIndex = lsData.Count - 1 Then
			btnNextCustomer.Enabled = False
		Else
			btnNextCustomer.Enabled = True
		End If
		htData = lsData.Item(iIndex)

		populateCustomerFields(lsData.Item(iIndex))

	End Sub
	'Go to previous record
	Private Sub btnPrev_Click(sender As Object, e As EventArgs) Handles btnPrevCustomer.Click
		Dim htData As Hashtable
		Dim iIndex As Integer
		iIndex = iCurrentIndex - 1
		iCurrentIndex = iIndex

		If iCurrentIndex = 0 Then
			btnPrevCustomer.Enabled = False
		Else
			btnPrevCustomer.Enabled = True
		End If
		If iCurrentIndex = lsData.Count - 1 Then
			btnNextCustomer.Enabled = False
		Else
			btnNextCustomer.Enabled = True
		End If

		htData = lsData.Item(iIndex)

		populateCustomerFields(lsData.Item(iIndex))
	End Sub
	'Go to next record
	Private Sub btnNext_Click(sender As Object, e As EventArgs) Handles btnNextCustomer.Click
		Dim htData As Hashtable
		Dim iIndex As Integer
		iIndex = iCurrentIndex + 1
		iCurrentIndex = iIndex

		If iCurrentIndex = 0 Then
			btnPrevCustomer.Enabled = False
		Else
			btnPrevCustomer.Enabled = True
		End If
		If iCurrentIndex = lsData.Count - 1 Then
			btnNextCustomer.Enabled = False
		Else
			btnNextCustomer.Enabled = True
		End If
		htData = lsData.Item(iIndex)

		populateCustomerFields(lsData.Item(iIndex))
	End Sub
	'Go to last record
	Private Sub btnLast_Click(sender As Object, e As EventArgs) Handles btnLastCustomer.Click
		Dim htData As Hashtable
		Dim iIndex As Integer
		iIndex = lsData.Count - 1
		iCurrentIndex = iIndex

		If iCurrentIndex = 0 Then
			btnPrevCustomer.Enabled = False
		Else
			btnPrevCustomer.Enabled = True
		End If
		If iCurrentIndex = lsData.Count - 1 Then
			btnNextCustomer.Enabled = False
		Else
			btnNextCustomer.Enabled = True
		End If

		htData = lsData.Item(iIndex)

		populateCustomerFields(lsData.Item(iIndex))
	End Sub
	'Listview
	Private Sub lstCustomer_SelectedIndexChanged(sender As Object, e As EventArgs) Handles lstCustomer.SelectedIndexChanged
		Dim selectedIndex As Integer = lstCustomer.SelectedIndex
		Dim selectedCustomer As Object = lstCustomer.SelectedItem

		Dim htData = lsData.Item(selectedIndex)
		Dim sCustomerDetails As String
		sCustomerDetails = CStr(htData("CustomerID"))
		sCustomerDetails = sCustomerDetails & " | " & CStr(htData("Title"))
		sCustomerDetails = sCustomerDetails & " | " & CStr(htData("Gender"))
		sCustomerDetails = sCustomerDetails & " | " & CStr(htData("FirstName"))
		sCustomerDetails = sCustomerDetails & " | " & CStr(htData("LastName"))
		sCustomerDetails = sCustomerDetails & " | " & CInt(htData("Phone"))
		sCustomerDetails = sCustomerDetails & " | " & CStr(htData("Address"))
		sCustomerDetails = sCustomerDetails & " | " & CStr(htData("Email"))

		dtpDOB.Text = CStr(Date.Today)
		picErrorTittle.Visible = False
		picErrorGender.Visible = False
		picErrorFirstName.Visible = False
		picErrorLastName.Visible = False
		picErrorPhone.Visible = False
		picErrorAddress.Visible = False
		picErrorEmail.Visible = False
		picErrorDOB.Visible = False
	End Sub
	'Clear customer form
	Private Sub clearCustomer()

		txtcustomer_id.Text = ""
		cboTitle.SelectedIndex = -1
		cboGender.SelectedIndex = -1
		txtfirstname.Text = ""
		txtlastname.Text = ""
		txtphone.Text = ""
		txtaddress.Text = ""
		txtemail.Text = ""
		txtFindCustomer.Text = ""
		dtpDOB.Text = CStr(Date.Today)
		picErrorTittle.Visible = False
		picErrorGender.Visible = False
		picErrorFirstName.Visible = False
		picErrorLastName.Visible = False
		picErrorPhone.Visible = False
		picErrorAddress.Visible = False
		picErrorEmail.Visible = False
		picErrorDOB.Visible = False
	End Sub
	'Clear form
	Private Sub clearForm()
		txtcustomer_id.Clear()
		cboTitle.SelectedIndex = -1
		cboGender.SelectedIndex = -1
		txtfirstname.Clear()
		txtlastname.Clear()
		txtphone.Clear()
		txtaddress.Clear()
		txtemail.Clear()
		txtFindCustomer.Clear()
		dtpDOB.Text = CStr(Date.Today)
	End Sub
	'Refresh customer form
	Private Sub btnClearCustomer_Click(sender As Object, e As EventArgs) Handles btnNewCustomer.Click
		clearCustomer()
	End Sub
	'Close the form
	Private Sub btnCloseCustomer_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnCloseCustomer.Click
        'Confirmation before exiting the system
        If MsgBox("Are you sure to close this form?", MsgBoxStyle.YesNo, "Exit Confirmation") = MsgBoxResult.Yes Then
			Me.Close()
			End
		ElseIf CBool(MsgBoxResult.No) Then
            Me.Show()
        End If
    End Sub
	'Navigation
	'Stay in customer form
	Private Sub btnGoToCustomer_Click(ByVal sender As Object, ByVal e As EventArgs)
		Me.Refresh()
	End Sub
	'Go to customer in Menu
	Private Sub CustomerToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles CustomerToolStripMenuItem.Click
		Me.Refresh()
	End Sub
	'Go to room
	Private Sub btnGoToRoom_Click(ByVal sender As Object, ByVal e As EventArgs)
		frmRoom.Show()
		Me.Hide()
	End Sub
	'Go to room on Menu
	Private Sub RoomToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles RoomToolStripMenuItem.Click
		frmRoom.Show()
		Me.Hide()
	End Sub
	'Go to booking
	Private Sub btnGoToBooking_Click(ByVal sender As Object, ByVal e As EventArgs)
		frmBooking.Show()
		Me.Hide()
	End Sub
	'Go to booking on Menu
	Private Sub BookingToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles BookingToolStripMenuItem.Click
		frmBooking.Show()
		Me.Hide()
	End Sub
	'About page
	Private Sub AboutToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles AboutToolStripMenuItem.Click
		Dim oController As PageController = New PageController
		oController.createAboutPage()
	End Sub
	'Help page
	Private Sub HelpToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles HelpToolStripMenuItem.Click
		Dim oController As PageController = New PageController
		oController.createHelpPage()
	End Sub

	Private Sub dtpDOB_ValueChanged(sender As Object, e As EventArgs) Handles dtpDOB.ValueChanged

	End Sub
	'Go to Generate normal report
	Private Sub ReportGenerationToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles GenerateNormalReportToolStripMenuItem.Click
		ReportGeneration.Show()
		Me.Hide()
	End Sub
	'Go to Generate Break Report
	Private Sub GenerateBreakReportToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles GenerateBreakReportToolStripMenuItem.Click
		BreakReport.Show()
		Me.Hide()
	End Sub
End Class