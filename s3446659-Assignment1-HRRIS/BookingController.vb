﻿Option Explicit On
Option Strict On

Imports System.Data.OleDb


' Name:        BookingController.vb
' Description: Intermediate between VB and access (Booking)
' Author:      Do Khanh Hoang
' Date:        28/03/2017
'comment

Public Class BookingController
	'Connect to database
	Public Const CONNECTION_STRING As String =
	 "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=HRRISDB.accdb"

	Public Function insert(ByVal htData As Hashtable) As Integer
		'Declares variables
		Dim oConnection As OleDbConnection = New OleDbConnection(CONNECTION_STRING)
		Dim iNumBookingRows As Integer

		Try
			Debug.Print("Connection string: " & oConnection.ConnectionString)
			oConnection.Open()
			Dim oBookingCommand As OleDbCommand = New OleDbCommand
			Dim oInvoiceCommand As OleDbCommand = New OleDbCommand
			oBookingCommand.Connection = oConnection
			oInvoiceCommand.Connection = oConnection

			'Insert to database
			oBookingCommand.CommandText =
			   "INSERT INTO booking (booking_date, room_id, customer_id, num_days, num_guests, checkin_date, total_price, comments) VALUES (?, ?, ?, ?, ?, ?, ?, ?);"

			oBookingCommand.Parameters.Add("Date", OleDbType.Date)
			oBookingCommand.Parameters.Add("RoomID", OleDbType.Integer, 10)
			oBookingCommand.Parameters.Add("CustomerID", OleDbType.Integer, 10)
			oBookingCommand.Parameters.Add("NumberOfDays", OleDbType.Integer, 10)
			oBookingCommand.Parameters.Add("NumberOfGuests", OleDbType.Integer, 10)
			oBookingCommand.Parameters.Add("CheckinDate", OleDbType.Date)
			oBookingCommand.Parameters.Add("TotalPrice", OleDbType.Double, 10)
			oBookingCommand.Parameters.Add("Comments", OleDbType.VarChar, 255)

			'Define value type
			oBookingCommand.Parameters("Date").Value = CDate(htData("Date"))
			oBookingCommand.Parameters("RoomID").Value = CInt(htData("RoomID"))
			oBookingCommand.Parameters("CustomerID").Value = CInt(htData("CustomerID"))
			oBookingCommand.Parameters("NumberOfDays").Value = CInt(htData("NumberOfDays"))
			oBookingCommand.Parameters("NumberOfGuests").Value = CInt(htData("NumberOfGuests"))
			oBookingCommand.Parameters("CheckinDate").Value = CDate(htData("CheckinDate"))
			oBookingCommand.Parameters("TotalPrice").Value = CInt(htData("TotalPrice"))
			oBookingCommand.Parameters("Comments").Value = CStr(htData("Comments"))

			oBookingCommand.Prepare()

			iNumBookingRows = oBookingCommand.ExecuteNonQuery()


			Debug.Print(CStr(iNumBookingRows))

			MsgBox("The record was inserted")

		Catch ex As Exception
			Debug.Print("ERROR: " & ex.Message)
			MsgBox("An error occured. The record wasn't inserted.")
		Finally

		End Try

		Return iNumBookingRows
	End Function
	'Find all
	Public Function findAll() As List(Of Hashtable)

		Dim oConnection As OleDbConnection = New OleDbConnection(CONNECTION_STRING)
		Dim lsData As New List(Of Hashtable)

		Try
			Debug.Print("Connection string: " & oConnection.ConnectionString)

			oConnection.Open()
			Dim oCommand As OleDbCommand = New OleDbCommand
			oCommand.Connection = oConnection

			oCommand.CommandText =
				"SELECT * FROM booking ORDER BY booking_id;"
			oCommand.Prepare()
			Dim oDataReader = oCommand.ExecuteReader()

			Dim htTempData As Hashtable
			Do While oDataReader.Read() = True
				htTempData = New Hashtable
				htTempData("BookingID") = CStr(oDataReader("booking_id"))
				htTempData("Date") = CDate(oDataReader("booking_date"))
				htTempData("RoomID") = CInt(oDataReader("room_id"))
				htTempData("CustomerID") = CInt(oDataReader("customer_id"))
				htTempData("NumberOfDays") = CInt(oDataReader("num_days"))
				htTempData("NumberOfGuests") = CInt(oDataReader("num_guests"))
				htTempData("CheckinDate") = CDate(oDataReader("checkin_date"))
				htTempData("TotalPrice") = CInt(oDataReader("total_price"))
				htTempData("Comments") = CStr(oDataReader("comments"))
				lsData.Add(htTempData)
			Loop

			Debug.Print("The records was found.")

		Catch ex As Exception
			Debug.Print("ERROR: " & ex.Message)
		Finally
			oConnection.Close()
		End Try

		Return lsData

	End Function
	'get price
	Public Function getPrice() As List(Of String)

		Dim oConnection As OleDbConnection = New OleDbConnection(CONNECTION_STRING)
		Dim lsData As New List(Of String)

		Try
			Debug.Print("Connection string: " & oConnection.ConnectionString)

			oConnection.Open()
			Dim oCommand As OleDbCommand = New OleDbCommand
			oCommand.Connection = oConnection

			oCommand.CommandText =
				"SELECT price FROM room;"
			oCommand.Prepare()
			Dim oDataReader = oCommand.ExecuteReader()

			Do While oDataReader.Read() = True
				lsData.Add(oDataReader("price").ToString())
			Loop

		Catch ex As Exception
			Debug.Print("ERROR: " & ex.Message)
		Finally
			oConnection.Close()
		End Try

		Return lsData

	End Function
	'Call room_id
	Public Function callRoomID() As List(Of String)

		Dim oConnection As OleDbConnection = New OleDbConnection(CONNECTION_STRING)
		Dim lsData As New List(Of String)

		Try
			Debug.Print("Connection string: " & oConnection.ConnectionString)

			oConnection.Open()
			Dim oCommand As OleDbCommand = New OleDbCommand
			oCommand.Connection = oConnection

			oCommand.CommandText =
				"SELECT room_id FROM room ORDER BY room_id;"
			oCommand.Prepare()
			Dim oDataReader = oCommand.ExecuteReader()

			Do While oDataReader.Read() = True
				lsData.Add(oDataReader("room_id").ToString())
			Loop

		Catch ex As Exception
			Debug.Print("ERROR: " & ex.Message)
		Finally
			oConnection.Close()
		End Try

		Return lsData

	End Function

	'Call customer_id
	Public Function callCustomerID() As List(Of String)

		Dim oConnection As OleDbConnection = New OleDbConnection(CONNECTION_STRING)
		Dim lsData As New List(Of String)

		Try
			Debug.Print("Connection string: " & oConnection.ConnectionString)

			oConnection.Open()
			Dim oCommand As OleDbCommand = New OleDbCommand
			oCommand.Connection = oConnection

			oCommand.CommandText =
				"SELECT customer_id FROM customer ORDER BY customer_id;"
			oCommand.Prepare()
			Dim oDataReader = oCommand.ExecuteReader()

			Do While oDataReader.Read() = True
				lsData.Add(oDataReader("customer_id").ToString())
			Loop

		Catch ex As Exception
			Debug.Print("ERROR: " & ex.Message)
		Finally
			oConnection.Close()
		End Try

		Return lsData

	End Function
	'Read by ID
	Public Function findByID(sID As String) As List(Of Hashtable)

		Dim oConnection As OleDbConnection = New OleDbConnection(CONNECTION_STRING)
		Dim lsData As New List(Of Hashtable)

		Try
			Debug.Print("Connection string: " & oConnection.ConnectionString)

			oConnection.Open()
			Dim oCommand As OleDbCommand = New OleDbCommand
			oCommand.Connection = oConnection

			oCommand.CommandText =
				"SELECT * FROM booking WHERE booking_id = ?;"
			oCommand.Parameters.Add("booking_id", OleDbType.Integer, 8)
			oCommand.Parameters("booking_id").Value = CInt(sID)
			oCommand.Prepare()
			Dim oDataReader = oCommand.ExecuteReader()

			Dim htTempData As Hashtable
			Do While oDataReader.Read() = True
				htTempData = New Hashtable
				htTempData("BookingID") = CStr(oDataReader("booking_id"))
				htTempData("Date") = CDate(oDataReader("booking_date"))
				htTempData("RoomID") = CInt(oDataReader("room_id"))
				htTempData("CustomerID") = CInt(oDataReader("customer_id"))
				htTempData("NumberOfDays") = CInt(oDataReader("num_days"))
				htTempData("NumberOfGuests") = CInt(oDataReader("num_guests"))
				htTempData("CheckinDate") = CDate(oDataReader("checkin_date"))
				htTempData("TotalPrice") = CInt(oDataReader("total_price"))
				htTempData("Comments") = CStr(oDataReader("comments"))
				lsData.Add(htTempData)
			Loop

			Debug.Print("Read the record successfully")

		Catch ex As Exception
			Debug.Print("ERROR: " & ex.Message)
			MsgBox("No records were found!")
		Finally
			oConnection.Close()
		End Try

		Return lsData

	End Function
	'Update Function
	Public Function update(ByVal htData As Hashtable) As Integer

		Dim oConnection As OleDbConnection = New OleDbConnection(CONNECTION_STRING)
		Dim iNumRows As Integer

		Try
			Debug.Print("Connection string: " & oConnection.ConnectionString)

			oConnection.Open()
			Dim oCommand As OleDbCommand = New OleDbCommand
			oCommand.Connection = oConnection

			oCommand.CommandText =
				"UPDATE booking SET booking_date = ?, room_id = ?, customer_id = ?, num_days = ?, num_guests = ?, checkin_date = ?, total_price = ?, comment = ? WHERE booking_id = ?;"

			oCommand.Parameters.Add("Date", OleDbType.Date)
			oCommand.Parameters.Add("RoomID", OleDbType.Integer, 10)
			oCommand.Parameters.Add("CustomerID", OleDbType.Integer, 10)
			oCommand.Parameters.Add("NumberOfDays", OleDbType.Integer, 10)
			oCommand.Parameters.Add("NumberOfGuests", OleDbType.Integer, 10)
			oCommand.Parameters.Add("CheckinDate", OleDbType.Date)
			oCommand.Parameters.Add("TotalPrice", OleDbType.Double, 10)
			oCommand.Parameters.Add("Comments", OleDbType.VarChar, 255)
			oCommand.Parameters.Add("BookingID", OleDbType.Integer, 8)

			'Define value type
			oCommand.Parameters("Date").Value = CDate(htData("Date"))
			oCommand.Parameters("RoomID").Value = CInt(htData("RoomID"))
			oCommand.Parameters("CustomerID").Value = CInt(htData("CustomerID"))
			oCommand.Parameters("NumberOfDays").Value = CInt(htData("NumberOfDays"))
			oCommand.Parameters("NumberOfGuests").Value = CInt(htData("NumberOfGuests"))
			oCommand.Parameters("CheckinDate").Value = CDate(htData("CheckinDate"))
			oCommand.Parameters("TotalPrice").Value = CInt(htData("TotalPrice"))
			oCommand.Parameters("Comments").Value = CStr(htData("Comments"))
			oCommand.Parameters("BookingID").Value = CInt(htData("Booking"))

			oCommand.Prepare()
			iNumRows = oCommand.ExecuteNonQuery()

			Debug.Print(CStr(iNumRows))
			Debug.Print("The record was updated.")

		Catch ex As Exception
			Debug.Print("ERROR: " & ex.Message)
		Finally
			oConnection.Close()
		End Try

		Return iNumRows
	End Function
	'Delete function
	Public Function delete(sBId As String, sRId As String, sCId As String) As Integer
		Dim oConnection As OleDbConnection = New OleDbConnection(CONNECTION_STRING)
		Dim iNumRows As Integer

		Try
			Debug.Print("Connection string: " & oConnection.ConnectionString)

			oConnection.Open()
			Dim oCommand As OleDbCommand = New OleDbCommand

			oCommand.Connection = oConnection
			oCommand.CommandText =
			   "DELETE FROM booking WHERE booking_id = ? AND room_id = ? AND customer_id = ?;"
			oCommand.Parameters.Add("BookingID", OleDbType.Integer, 8)
			oCommand.Parameters.Add("RoomID", OleDbType.Integer, 8)
			oCommand.Parameters.Add("CustomerID", OleDbType.Integer, 8)
			oCommand.Parameters("BookingID").Value = CInt(sBId)
			oCommand.Parameters("RoomID").Value = CInt(sRId)
			oCommand.Parameters("CustomerID").Value = CInt(sCId)

			oCommand.Prepare()
			iNumRows = oCommand.ExecuteNonQuery()

			Debug.Print(CStr(iNumRows))
			Debug.Print("The record was deleted.")

		Catch ex As Exception
			Debug.Print("ERROR: " & ex.Message)
		Finally
			oConnection.Close()
		End Try

		Return iNumRows

	End Function
End Class

