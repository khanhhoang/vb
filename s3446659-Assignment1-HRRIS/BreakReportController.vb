﻿Option Explicit On
Option Strict On

Imports System.IO
Imports System.Data.OleDb




Public Class BreakReportController
	'Connect to database
	Public Const CONNECTION_STRING As String =
	 "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=HRRISDB.accdb"

	'BreakReport1: booking_id, room_id, customer_id, number of days, number of guests, total price
	Public Function BreakReport1(sYear As String, sMonth As String) As List(Of Hashtable)

		Dim oConnection As OleDbConnection = New OleDbConnection(CONNECTION_STRING)
		Dim lsData As New List(Of Hashtable)

		Try
			Debug.Print("Connection string: " & oConnection.ConnectionString)

			oConnection.Open()
			Dim oCommand As OleDbCommand = New OleDbCommand
			oCommand.Connection = oConnection

			oCommand.CommandText =
			"SELECT booking_id, booking_date, room_number, num_days, num_guests, total_price, type, price, num_beds, floor FROM booking, room WHERE room.room_id = booking.room_id AND DatePart('yyyy', booking_date)=? AND DatePart('m', booking_date)=? ORDER BY booking_id;"
			oCommand.Parameters.Add("DatePart('yyyy', booking_date)", OleDbType.Integer, 8)
			oCommand.Parameters("DatePart('yyyy', booking_date)").Value = CStr(sYear)
			oCommand.Parameters.Add("DatePart('m', booking_date)", OleDbType.Integer, 8)
			oCommand.Parameters("DatePart('m', booking_date)").Value = CStr(sMonth)
			oCommand.Prepare()
			Dim oDataReader = oCommand.ExecuteReader()

			Dim htTempData As Hashtable
			Do While oDataReader.Read() = True
				htTempData = New Hashtable
				htTempData("BookingID") = CStr(oDataReader("booking_id"))
				htTempData("Date") = CDate(oDataReader("booking_date"))
				htTempData("RoomNumber") = CStr(oDataReader("room_number"))
				htTempData("NumberOfDays") = CInt(oDataReader("num_days"))
				htTempData("NumberOfGuests") = CInt(oDataReader("num_guests"))
				htTempData("TotalPrice") = CInt(oDataReader("total_price"))
				htTempData("Type") = CStr(oDataReader("type"))
				htTempData("Price") = CInt(oDataReader("price"))
				htTempData("NumberOfBeds") = CInt(oDataReader("num_beds"))
				htTempData("Floor") = CInt(oDataReader("floor"))
				lsData.Add(htTempData)
			Loop

		Catch ex As Exception
			Debug.Print("ERROR: " & ex.Message)
		Finally
			oConnection.Close()
		End Try

		Return lsData

	End Function

	Private Function generateBreakReport1(ByVal lsData As List(Of Hashtable), ByVal sReportTitle As String) As String
		Debug.Print("GenerateBreakReport")

		Dim sReportContent As String

		' 1. Generate the start of the HTML file
		Dim sDoctype As String = "<!DOCTYPE html>"
		Dim sHtmlStartTag As String = "<html lang=""en"">"
		Dim sHeadTitle As String = "<head><title>" & sReportTitle & "</title></head>"
		Dim sBodyStartTag As String = "<body>"
		Dim sReportHeading As String = "</h1>" & sReportTitle & "</h1>"
		sReportContent = sDoctype & Environment.NewLine & sHtmlStartTag & Environment.NewLine & sHeadTitle _
		 & Environment.NewLine & sBodyStartTag & Environment.NewLine & sReportHeading & Environment.NewLine

		' 2. Generate the product table and its rows
		Dim sTable = generateControlBreakTable1(lsData)
		sReportContent &= sTable & Environment.NewLine

		' 3. Generate the end of the HTML file
		Dim sBodyEndTag As String = "</body>"
		Dim sHTMLEndTag As String = "</html>"
		sReportContent &= sBodyEndTag & Environment.NewLine & sHTMLEndTag

		Return sReportContent

	End Function
	Private Function generateControlBreakTable1(ByVal lsData As List(Of Hashtable)) As String

		' Generate the start of the table
		Dim sTable = "<table border=""1"">" & Environment.NewLine
		Dim htSample As Hashtable = lsData.Item(0)
		'Dim lsKeys = htSample.Keys
		Dim lsKeys As List(Of String) = New List(Of String)
		lsKeys.Add("BookingID")
		lsKeys.Add("RoomNumber")
		lsKeys.Add("Type")
		lsKeys.Add("Price")
		lsKeys.Add("NumberOfBeds")
		lsKeys.Add("Floor")
		lsKeys.Add("Date")
		lsKeys.Add("NumberOfDays")
		lsKeys.Add("NumberOfGuests")
		lsKeys.Add("TotalPrice")

		' Generate the header row
		Dim sHeaderRow = "<tr>" & Environment.NewLine
		For Each key In lsKeys
			sHeaderRow &= "<th>" & CStr(key) & "</th>" & Environment.NewLine
		Next
		sHeaderRow &= "</tr>" & Environment.NewLine
		sTable &= sHeaderRow

		' Generate the table rows
		sTable &= generateTable1Rows(lsData, lsKeys)

		' Generate the end of the table
		sTable &= "</table>" & Environment.NewLine

		Return sTable

	End Function
	Private Function generateTable1Rows(ByVal lsData As List(Of Hashtable),
									   ByVal lsKeys As List(Of String)) As String

		'1. Initialisation
		Dim sRows As String = ""
		Dim sTableRow As String
		Dim iCountRecordsPerCategory As Integer = 0
		Dim bFirstTime As Boolean = True
		Dim sCurrentControlField As String = ""
		Dim sPreviousControlField As String = ""
		'2. Loop through the list of hashtables
		For Each record In lsData

			'2a. Get a product and set the current key
			Dim bookings As Hashtable = record
			sCurrentControlField = CStr(bookings("RoomNumber"))

			'2b. Do not check for control break on the first iteration of the loop
			If bFirstTime Then
				bFirstTime = False
			Else
				'Output total row if change in control field
				'And reset the total

				If sCurrentControlField <> sPreviousControlField Then
					sTableRow = "<tr><td colspan = """ & lsKeys.Count & """>" _
						& " Total number of bookings made for room " & sPreviousControlField _
						& " : " & iCountRecordsPerCategory _
						& "</td></tr>" _
						& Environment.NewLine
					sRows &= sTableRow
					iCountRecordsPerCategory = 0
				End If
			End If

			' 2c. Output a normal row for every pass thru' the list
			sTableRow = "<tr>" & Environment.NewLine
			For Each key In lsKeys
				sTableRow &= "<td>" & CStr(bookings(key)) & "</td>" & Environment.NewLine
			Next
			sTableRow &= "</tr>" & Environment.NewLine
			sRows &= sTableRow

			'2d. Update control field and increment total
			sPreviousControlField = sCurrentControlField
			iCountRecordsPerCategory += 1
		Next

		'3. After the loop, need to output the last total row
		sTableRow = "<tr><td colspan = """ & lsKeys.Count & """>" _
						& " Total number of bookings made for room " & sCurrentControlField _
						& " : " & iCountRecordsPerCategory _
						& "</td></tr>" _
						& Environment.NewLine
		sRows &= sTableRow

		Return sRows

	End Function

	'Create break report 1
	Public Sub createBreakReport1(sYear As String, sMonth As String)
		Debug.Print("CreateBreakReport1")

		Dim lsData = BreakReport1(sYear, sMonth)
		Dim sReportTitle = "Control Break Report 1"
		Dim sReportContent = generateBreakReport1(lsData, sReportTitle)
		Dim sReportFilename = "BreakReport-1.html"
		saveReport(sReportContent, sReportFilename)
		Dim sParam As String = """" & Application.StartupPath & "\" & sReportFilename & """"
		Debug.Print("sParam: " & sParam)
		System.Diagnostics.Process.Start(sParam)
	End Sub

	'Save report
	Private Sub saveReport(ByVal sReportContent As String, ByVal sReportFilename As String)
		Debug.Print("saveReport: " & sReportFilename)
		Dim oReportFile As StreamWriter = New StreamWriter(Application.StartupPath & "\" & sReportFilename)

		' Check if the file is open before starting to write to it
		If Not (oReportFile Is Nothing) Then
			oReportFile.Write(sReportContent)
			oReportFile.Close()
		End If
	End Sub
End Class
