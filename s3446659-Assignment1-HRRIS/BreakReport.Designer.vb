﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class BreakReport
	Inherits System.Windows.Forms.Form

	'Form overrides dispose to clean up the component list.
	<System.Diagnostics.DebuggerNonUserCode()> _
	Protected Overrides Sub Dispose(ByVal disposing As Boolean)
		Try
			If disposing AndAlso components IsNot Nothing Then
				components.Dispose()
			End If
		Finally
			MyBase.Dispose(disposing)
		End Try
	End Sub

	'Required by the Windows Form Designer
	Private components As System.ComponentModel.IContainer

	'NOTE: The following procedure is required by the Windows Form Designer
	'It can be modified using the Windows Form Designer.  
	'Do not modify it using the code editor.
	<System.Diagnostics.DebuggerStepThrough()> _
	Private Sub InitializeComponent()
		Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
		Me.MenuToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
		Me.CustomerToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
		Me.RoomToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
		Me.BookingToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
		Me.ReportToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
		Me.GenerateNormalReportToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
		Me.GenerateBreakReportToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
		Me.HelpToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
		Me.AboutToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
		Me.GroupBox1 = New System.Windows.Forms.GroupBox()
		Me.btnCreateFirstBreakReport = New System.Windows.Forms.Button()
		Me.label = New System.Windows.Forms.Label()
		Me.txtYear1 = New System.Windows.Forms.TextBox()
		Me.Label1 = New System.Windows.Forms.Label()
		Me.cboMonth1 = New System.Windows.Forms.ComboBox()
		Me.TextBox1 = New System.Windows.Forms.TextBox()
		Me.btnCloseRoom = New System.Windows.Forms.Button()
		Me.MenuStrip1.SuspendLayout()
		Me.GroupBox1.SuspendLayout()
		Me.SuspendLayout()
		'
		'MenuStrip1
		'
		Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.MenuToolStripMenuItem, Me.ReportToolStripMenuItem, Me.HelpToolStripMenuItem, Me.AboutToolStripMenuItem})
		Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
		Me.MenuStrip1.Name = "MenuStrip1"
		Me.MenuStrip1.Size = New System.Drawing.Size(686, 24)
		Me.MenuStrip1.TabIndex = 15
		Me.MenuStrip1.Text = "MenuStrip1"
		'
		'MenuToolStripMenuItem
		'
		Me.MenuToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.CustomerToolStripMenuItem, Me.RoomToolStripMenuItem, Me.BookingToolStripMenuItem})
		Me.MenuToolStripMenuItem.Name = "MenuToolStripMenuItem"
		Me.MenuToolStripMenuItem.Size = New System.Drawing.Size(50, 20)
		Me.MenuToolStripMenuItem.Text = "Menu"
		'
		'CustomerToolStripMenuItem
		'
		Me.CustomerToolStripMenuItem.Name = "CustomerToolStripMenuItem"
		Me.CustomerToolStripMenuItem.Size = New System.Drawing.Size(126, 22)
		Me.CustomerToolStripMenuItem.Text = "Customer"
		'
		'RoomToolStripMenuItem
		'
		Me.RoomToolStripMenuItem.Name = "RoomToolStripMenuItem"
		Me.RoomToolStripMenuItem.Size = New System.Drawing.Size(126, 22)
		Me.RoomToolStripMenuItem.Text = "Room"
		'
		'BookingToolStripMenuItem
		'
		Me.BookingToolStripMenuItem.Name = "BookingToolStripMenuItem"
		Me.BookingToolStripMenuItem.Size = New System.Drawing.Size(126, 22)
		Me.BookingToolStripMenuItem.Text = "Booking"
		'
		'ReportToolStripMenuItem
		'
		Me.ReportToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.GenerateNormalReportToolStripMenuItem, Me.GenerateBreakReportToolStripMenuItem})
		Me.ReportToolStripMenuItem.Name = "ReportToolStripMenuItem"
		Me.ReportToolStripMenuItem.Size = New System.Drawing.Size(54, 20)
		Me.ReportToolStripMenuItem.Text = "Report"
		'
		'GenerateNormalReportToolStripMenuItem
		'
		Me.GenerateNormalReportToolStripMenuItem.Name = "GenerateNormalReportToolStripMenuItem"
		Me.GenerateNormalReportToolStripMenuItem.Size = New System.Drawing.Size(202, 22)
		Me.GenerateNormalReportToolStripMenuItem.Text = "Generate Normal Report"
		'
		'GenerateBreakReportToolStripMenuItem
		'
		Me.GenerateBreakReportToolStripMenuItem.Name = "GenerateBreakReportToolStripMenuItem"
		Me.GenerateBreakReportToolStripMenuItem.Size = New System.Drawing.Size(202, 22)
		Me.GenerateBreakReportToolStripMenuItem.Text = "Generate Break Report"
		'
		'HelpToolStripMenuItem
		'
		Me.HelpToolStripMenuItem.Name = "HelpToolStripMenuItem"
		Me.HelpToolStripMenuItem.Size = New System.Drawing.Size(44, 20)
		Me.HelpToolStripMenuItem.Text = "Help"
		'
		'AboutToolStripMenuItem
		'
		Me.AboutToolStripMenuItem.Name = "AboutToolStripMenuItem"
		Me.AboutToolStripMenuItem.Size = New System.Drawing.Size(52, 20)
		Me.AboutToolStripMenuItem.Text = "About"
		'
		'GroupBox1
		'
		Me.GroupBox1.Controls.Add(Me.btnCreateFirstBreakReport)
		Me.GroupBox1.Controls.Add(Me.label)
		Me.GroupBox1.Controls.Add(Me.txtYear1)
		Me.GroupBox1.Controls.Add(Me.Label1)
		Me.GroupBox1.Controls.Add(Me.cboMonth1)
		Me.GroupBox1.Location = New System.Drawing.Point(12, 27)
		Me.GroupBox1.Name = "GroupBox1"
		Me.GroupBox1.Size = New System.Drawing.Size(527, 87)
		Me.GroupBox1.TabIndex = 16
		Me.GroupBox1.TabStop = False
		Me.GroupBox1.Text = "Break Report 1"
		'
		'btnCreateFirstBreakReport
		'
		Me.btnCreateFirstBreakReport.Location = New System.Drawing.Point(395, 36)
		Me.btnCreateFirstBreakReport.Name = "btnCreateFirstBreakReport"
		Me.btnCreateFirstBreakReport.Size = New System.Drawing.Size(96, 37)
		Me.btnCreateFirstBreakReport.TabIndex = 5
		Me.btnCreateFirstBreakReport.Text = "Generate Break Report"
		Me.btnCreateFirstBreakReport.UseVisualStyleBackColor = True
		'
		'label
		'
		Me.label.AutoSize = True
		Me.label.Location = New System.Drawing.Point(23, 29)
		Me.label.Name = "label"
		Me.label.Size = New System.Drawing.Size(29, 13)
		Me.label.TabIndex = 4
		Me.label.Text = "Year"
		'
		'txtYear1
		'
		Me.txtYear1.Location = New System.Drawing.Point(26, 43)
		Me.txtYear1.Name = "txtYear1"
		Me.txtYear1.Size = New System.Drawing.Size(100, 20)
		Me.txtYear1.TabIndex = 3
		'
		'Label1
		'
		Me.Label1.AutoSize = True
		Me.Label1.Location = New System.Drawing.Point(185, 29)
		Me.Label1.Name = "Label1"
		Me.Label1.Size = New System.Drawing.Size(37, 13)
		Me.Label1.TabIndex = 2
		Me.Label1.Text = "Month"
		'
		'cboMonth1
		'
		Me.cboMonth1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
		Me.cboMonth1.FormattingEnabled = True
		Me.cboMonth1.Items.AddRange(New Object() {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12"})
		Me.cboMonth1.Location = New System.Drawing.Point(185, 45)
		Me.cboMonth1.Name = "cboMonth1"
		Me.cboMonth1.Size = New System.Drawing.Size(121, 21)
		Me.cboMonth1.TabIndex = 0
		'
		'TextBox1
		'
		Me.TextBox1.Enabled = False
		Me.TextBox1.Location = New System.Drawing.Point(12, 120)
		Me.TextBox1.Multiline = True
		Me.TextBox1.Name = "TextBox1"
		Me.TextBox1.Size = New System.Drawing.Size(281, 57)
		Me.TextBox1.TabIndex = 26
		Me.TextBox1.Text = "All bookings made for a given year and month broken down according to room number" &
	". The break should show the room's details and the total number of bookings made" &
	"."
		'
		'btnCloseRoom
		'
		Me.btnCloseRoom.Location = New System.Drawing.Point(557, 221)
		Me.btnCloseRoom.Name = "btnCloseRoom"
		Me.btnCloseRoom.Size = New System.Drawing.Size(75, 23)
		Me.btnCloseRoom.TabIndex = 27
		Me.btnCloseRoom.Text = "Close"
		Me.btnCloseRoom.UseVisualStyleBackColor = True
		'
		'BreakReport
		'
		Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
		Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
		Me.ClientSize = New System.Drawing.Size(686, 276)
		Me.Controls.Add(Me.btnCloseRoom)
		Me.Controls.Add(Me.TextBox1)
		Me.Controls.Add(Me.GroupBox1)
		Me.Controls.Add(Me.MenuStrip1)
		Me.Name = "BreakReport"
		Me.Text = "BreakReport"
		Me.MenuStrip1.ResumeLayout(False)
		Me.MenuStrip1.PerformLayout()
		Me.GroupBox1.ResumeLayout(False)
		Me.GroupBox1.PerformLayout()
		Me.ResumeLayout(False)
		Me.PerformLayout()

	End Sub

	Friend WithEvents MenuStrip1 As MenuStrip
	Friend WithEvents MenuToolStripMenuItem As ToolStripMenuItem
	Friend WithEvents CustomerToolStripMenuItem As ToolStripMenuItem
	Friend WithEvents RoomToolStripMenuItem As ToolStripMenuItem
	Friend WithEvents BookingToolStripMenuItem As ToolStripMenuItem
	Friend WithEvents ReportToolStripMenuItem As ToolStripMenuItem
	Friend WithEvents GenerateNormalReportToolStripMenuItem As ToolStripMenuItem
	Friend WithEvents GenerateBreakReportToolStripMenuItem As ToolStripMenuItem
	Friend WithEvents HelpToolStripMenuItem As ToolStripMenuItem
	Friend WithEvents AboutToolStripMenuItem As ToolStripMenuItem
	Friend WithEvents GroupBox1 As GroupBox
	Friend WithEvents btnCreateFirstBreakReport As Button
	Friend WithEvents label As Label
	Friend WithEvents txtYear1 As TextBox
	Friend WithEvents Label1 As Label
	Friend WithEvents cboMonth1 As ComboBox
	Friend WithEvents TextBox1 As TextBox
	Friend WithEvents btnCloseRoom As Button
End Class
