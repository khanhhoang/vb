﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmRoom
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
		Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmRoom))
		Me.GroupBox1 = New System.Windows.Forms.GroupBox()
		Me.picErrorRoomDescription = New System.Windows.Forms.PictureBox()
		Me.picErrorRoomAvailability = New System.Windows.Forms.PictureBox()
		Me.picErrorRoomPrice = New System.Windows.Forms.PictureBox()
		Me.picErrorRoomFloor = New System.Windows.Forms.PictureBox()
		Me.txtRoomDescription = New System.Windows.Forms.TextBox()
		Me.txtRoomAvailability = New System.Windows.Forms.TextBox()
		Me.txtRoomPrice = New System.Windows.Forms.TextBox()
		Me.txtFloor = New System.Windows.Forms.TextBox()
		Me.txtRoomNumber = New System.Windows.Forms.TextBox()
		Me.txtNumsOfBeds = New System.Windows.Forms.TextBox()
		Me.txtRoomType = New System.Windows.Forms.TextBox()
		Me.picErrorRoomNumber = New System.Windows.Forms.PictureBox()
		Me.picErrorRoomNumsOfBeds = New System.Windows.Forms.PictureBox()
		Me.picErrorRoomType = New System.Windows.Forms.PictureBox()
		Me.txtRoomID = New System.Windows.Forms.TextBox()
		Me.lblDescription = New System.Windows.Forms.Label()
		Me.lblAvailability = New System.Windows.Forms.Label()
		Me.lblPrice = New System.Windows.Forms.Label()
		Me.lblFloor = New System.Windows.Forms.Label()
		Me.lblRoomNumber = New System.Windows.Forms.Label()
		Me.lblNumberOfBeds = New System.Windows.Forms.Label()
		Me.lblType = New System.Windows.Forms.Label()
		Me.lblRoomID = New System.Windows.Forms.Label()
		Me.btnCloseRoom = New System.Windows.Forms.Button()
		Me.btnNewRoom = New System.Windows.Forms.Button()
		Me.btnSubmitRoom = New System.Windows.Forms.Button()
		Me.txtFindRoom = New System.Windows.Forms.TextBox()
		Me.btnFindRoom = New System.Windows.Forms.Button()
		Me.btnDeleteRoom = New System.Windows.Forms.Button()
		Me.btnUpdateRoom = New System.Windows.Forms.Button()
		Me.btnLastRoom = New System.Windows.Forms.Button()
		Me.btnNextRoom = New System.Windows.Forms.Button()
		Me.btnPrevRoom = New System.Windows.Forms.Button()
		Me.btnFirstRoom = New System.Windows.Forms.Button()
		Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
		Me.MenuToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
		Me.CustomerToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
		Me.RoomToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
		Me.BookingToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
		Me.ReportToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
		Me.GenerateNormalReportToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
		Me.GenerateBreakReportToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
		Me.HelpToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
		Me.AboutToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
		Me.GroupBox1.SuspendLayout()
		CType(Me.picErrorRoomDescription, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.picErrorRoomAvailability, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.picErrorRoomPrice, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.picErrorRoomFloor, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.picErrorRoomNumber, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.picErrorRoomNumsOfBeds, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.picErrorRoomType, System.ComponentModel.ISupportInitialize).BeginInit()
		Me.MenuStrip1.SuspendLayout()
		Me.SuspendLayout()
		'
		'GroupBox1
		'
		Me.GroupBox1.Controls.Add(Me.picErrorRoomDescription)
		Me.GroupBox1.Controls.Add(Me.picErrorRoomAvailability)
		Me.GroupBox1.Controls.Add(Me.picErrorRoomPrice)
		Me.GroupBox1.Controls.Add(Me.picErrorRoomFloor)
		Me.GroupBox1.Controls.Add(Me.txtRoomDescription)
		Me.GroupBox1.Controls.Add(Me.txtRoomAvailability)
		Me.GroupBox1.Controls.Add(Me.txtRoomPrice)
		Me.GroupBox1.Controls.Add(Me.txtFloor)
		Me.GroupBox1.Controls.Add(Me.txtRoomNumber)
		Me.GroupBox1.Controls.Add(Me.txtNumsOfBeds)
		Me.GroupBox1.Controls.Add(Me.txtRoomType)
		Me.GroupBox1.Controls.Add(Me.picErrorRoomNumber)
		Me.GroupBox1.Controls.Add(Me.picErrorRoomNumsOfBeds)
		Me.GroupBox1.Controls.Add(Me.picErrorRoomType)
		Me.GroupBox1.Controls.Add(Me.txtRoomID)
		Me.GroupBox1.Controls.Add(Me.lblDescription)
		Me.GroupBox1.Controls.Add(Me.lblAvailability)
		Me.GroupBox1.Controls.Add(Me.lblPrice)
		Me.GroupBox1.Controls.Add(Me.lblFloor)
		Me.GroupBox1.Controls.Add(Me.lblRoomNumber)
		Me.GroupBox1.Controls.Add(Me.lblNumberOfBeds)
		Me.GroupBox1.Controls.Add(Me.lblType)
		Me.GroupBox1.Controls.Add(Me.lblRoomID)
		Me.GroupBox1.Location = New System.Drawing.Point(-3, 22)
		Me.GroupBox1.Name = "GroupBox1"
		Me.GroupBox1.Size = New System.Drawing.Size(433, 300)
		Me.GroupBox1.TabIndex = 0
		Me.GroupBox1.TabStop = False
		Me.GroupBox1.Text = "Room Details"
		'
		'picErrorRoomDescription
		'
		Me.picErrorRoomDescription.Image = CType(resources.GetObject("picErrorRoomDescription.Image"), System.Drawing.Image)
		Me.picErrorRoomDescription.Location = New System.Drawing.Point(416, 222)
		Me.picErrorRoomDescription.Name = "picErrorRoomDescription"
		Me.picErrorRoomDescription.Size = New System.Drawing.Size(20, 19)
		Me.picErrorRoomDescription.TabIndex = 86
		Me.picErrorRoomDescription.TabStop = False
		Me.picErrorRoomDescription.Visible = False
		'
		'picErrorRoomAvailability
		'
		Me.picErrorRoomAvailability.Image = CType(resources.GetObject("picErrorRoomAvailability.Image"), System.Drawing.Image)
		Me.picErrorRoomAvailability.Location = New System.Drawing.Point(316, 193)
		Me.picErrorRoomAvailability.Name = "picErrorRoomAvailability"
		Me.picErrorRoomAvailability.Size = New System.Drawing.Size(20, 19)
		Me.picErrorRoomAvailability.TabIndex = 99
		Me.picErrorRoomAvailability.TabStop = False
		Me.picErrorRoomAvailability.Visible = False
		'
		'picErrorRoomPrice
		'
		Me.picErrorRoomPrice.Image = CType(resources.GetObject("picErrorRoomPrice.Image"), System.Drawing.Image)
		Me.picErrorRoomPrice.Location = New System.Drawing.Point(316, 165)
		Me.picErrorRoomPrice.Name = "picErrorRoomPrice"
		Me.picErrorRoomPrice.Size = New System.Drawing.Size(20, 19)
		Me.picErrorRoomPrice.TabIndex = 98
		Me.picErrorRoomPrice.TabStop = False
		Me.picErrorRoomPrice.Visible = False
		'
		'picErrorRoomFloor
		'
		Me.picErrorRoomFloor.Image = CType(resources.GetObject("picErrorRoomFloor.Image"), System.Drawing.Image)
		Me.picErrorRoomFloor.Location = New System.Drawing.Point(316, 135)
		Me.picErrorRoomFloor.Name = "picErrorRoomFloor"
		Me.picErrorRoomFloor.Size = New System.Drawing.Size(20, 19)
		Me.picErrorRoomFloor.TabIndex = 97
		Me.picErrorRoomFloor.TabStop = False
		Me.picErrorRoomFloor.Visible = False
		'
		'txtRoomDescription
		'
		Me.txtRoomDescription.Font = New System.Drawing.Font("Arial", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.txtRoomDescription.Location = New System.Drawing.Point(126, 222)
		Me.txtRoomDescription.Multiline = True
		Me.txtRoomDescription.Name = "txtRoomDescription"
		Me.txtRoomDescription.Size = New System.Drawing.Size(285, 74)
		Me.txtRoomDescription.TabIndex = 96
		'
		'txtRoomAvailability
		'
		Me.txtRoomAvailability.Location = New System.Drawing.Point(126, 194)
		Me.txtRoomAvailability.Name = "txtRoomAvailability"
		Me.txtRoomAvailability.Size = New System.Drawing.Size(184, 20)
		Me.txtRoomAvailability.TabIndex = 95
		'
		'txtRoomPrice
		'
		Me.txtRoomPrice.Location = New System.Drawing.Point(126, 165)
		Me.txtRoomPrice.Name = "txtRoomPrice"
		Me.txtRoomPrice.Size = New System.Drawing.Size(184, 20)
		Me.txtRoomPrice.TabIndex = 94
		'
		'txtFloor
		'
		Me.txtFloor.Location = New System.Drawing.Point(126, 135)
		Me.txtFloor.Name = "txtFloor"
		Me.txtFloor.Size = New System.Drawing.Size(184, 20)
		Me.txtFloor.TabIndex = 93
		'
		'txtRoomNumber
		'
		Me.txtRoomNumber.Location = New System.Drawing.Point(126, 106)
		Me.txtRoomNumber.Name = "txtRoomNumber"
		Me.txtRoomNumber.Size = New System.Drawing.Size(184, 20)
		Me.txtRoomNumber.TabIndex = 92
		'
		'txtNumsOfBeds
		'
		Me.txtNumsOfBeds.Location = New System.Drawing.Point(126, 77)
		Me.txtNumsOfBeds.Name = "txtNumsOfBeds"
		Me.txtNumsOfBeds.Size = New System.Drawing.Size(184, 20)
		Me.txtNumsOfBeds.TabIndex = 91
		'
		'txtRoomType
		'
		Me.txtRoomType.Location = New System.Drawing.Point(126, 48)
		Me.txtRoomType.Name = "txtRoomType"
		Me.txtRoomType.Size = New System.Drawing.Size(184, 20)
		Me.txtRoomType.TabIndex = 90
		'
		'picErrorRoomNumber
		'
		Me.picErrorRoomNumber.Image = CType(resources.GetObject("picErrorRoomNumber.Image"), System.Drawing.Image)
		Me.picErrorRoomNumber.Location = New System.Drawing.Point(316, 106)
		Me.picErrorRoomNumber.Name = "picErrorRoomNumber"
		Me.picErrorRoomNumber.Size = New System.Drawing.Size(20, 19)
		Me.picErrorRoomNumber.TabIndex = 89
		Me.picErrorRoomNumber.TabStop = False
		Me.picErrorRoomNumber.Visible = False
		'
		'picErrorRoomNumsOfBeds
		'
		Me.picErrorRoomNumsOfBeds.Image = CType(resources.GetObject("picErrorRoomNumsOfBeds.Image"), System.Drawing.Image)
		Me.picErrorRoomNumsOfBeds.Location = New System.Drawing.Point(316, 78)
		Me.picErrorRoomNumsOfBeds.Name = "picErrorRoomNumsOfBeds"
		Me.picErrorRoomNumsOfBeds.Size = New System.Drawing.Size(20, 19)
		Me.picErrorRoomNumsOfBeds.TabIndex = 86
		Me.picErrorRoomNumsOfBeds.TabStop = False
		Me.picErrorRoomNumsOfBeds.Visible = False
		'
		'picErrorRoomType
		'
		Me.picErrorRoomType.Image = CType(resources.GetObject("picErrorRoomType.Image"), System.Drawing.Image)
		Me.picErrorRoomType.Location = New System.Drawing.Point(316, 49)
		Me.picErrorRoomType.Name = "picErrorRoomType"
		Me.picErrorRoomType.Size = New System.Drawing.Size(20, 19)
		Me.picErrorRoomType.TabIndex = 85
		Me.picErrorRoomType.TabStop = False
		Me.picErrorRoomType.Visible = False
		'
		'txtRoomID
		'
		Me.txtRoomID.Enabled = False
		Me.txtRoomID.Location = New System.Drawing.Point(126, 20)
		Me.txtRoomID.Name = "txtRoomID"
		Me.txtRoomID.Size = New System.Drawing.Size(184, 20)
		Me.txtRoomID.TabIndex = 25
		'
		'lblDescription
		'
		Me.lblDescription.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.lblDescription.Location = New System.Drawing.Point(31, 222)
		Me.lblDescription.Name = "lblDescription"
		Me.lblDescription.Size = New System.Drawing.Size(89, 20)
		Me.lblDescription.TabIndex = 24
		Me.lblDescription.Text = "Description"
		Me.lblDescription.TextAlign = System.Drawing.ContentAlignment.MiddleRight
		'
		'lblAvailability
		'
		Me.lblAvailability.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.lblAvailability.Location = New System.Drawing.Point(31, 193)
		Me.lblAvailability.Name = "lblAvailability"
		Me.lblAvailability.Size = New System.Drawing.Size(89, 20)
		Me.lblAvailability.TabIndex = 23
		Me.lblAvailability.Text = "Availability"
		Me.lblAvailability.TextAlign = System.Drawing.ContentAlignment.MiddleRight
		'
		'lblPrice
		'
		Me.lblPrice.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.lblPrice.Location = New System.Drawing.Point(31, 164)
		Me.lblPrice.Name = "lblPrice"
		Me.lblPrice.Size = New System.Drawing.Size(89, 20)
		Me.lblPrice.TabIndex = 22
		Me.lblPrice.Text = "Price"
		Me.lblPrice.TextAlign = System.Drawing.ContentAlignment.MiddleRight
		'
		'lblFloor
		'
		Me.lblFloor.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.lblFloor.Location = New System.Drawing.Point(31, 135)
		Me.lblFloor.Name = "lblFloor"
		Me.lblFloor.Size = New System.Drawing.Size(89, 20)
		Me.lblFloor.TabIndex = 21
		Me.lblFloor.Text = "Floor"
		Me.lblFloor.TextAlign = System.Drawing.ContentAlignment.MiddleRight
		'
		'lblRoomNumber
		'
		Me.lblRoomNumber.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.lblRoomNumber.Location = New System.Drawing.Point(31, 106)
		Me.lblRoomNumber.Name = "lblRoomNumber"
		Me.lblRoomNumber.Size = New System.Drawing.Size(89, 20)
		Me.lblRoomNumber.TabIndex = 20
		Me.lblRoomNumber.Text = "Room Number"
		Me.lblRoomNumber.TextAlign = System.Drawing.ContentAlignment.MiddleRight
		'
		'lblNumberOfBeds
		'
		Me.lblNumberOfBeds.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.lblNumberOfBeds.Location = New System.Drawing.Point(31, 77)
		Me.lblNumberOfBeds.Name = "lblNumberOfBeds"
		Me.lblNumberOfBeds.Size = New System.Drawing.Size(89, 20)
		Me.lblNumberOfBeds.TabIndex = 19
		Me.lblNumberOfBeds.Text = "Number of Beds"
		Me.lblNumberOfBeds.TextAlign = System.Drawing.ContentAlignment.MiddleRight
		'
		'lblType
		'
		Me.lblType.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.lblType.Location = New System.Drawing.Point(31, 48)
		Me.lblType.Name = "lblType"
		Me.lblType.Size = New System.Drawing.Size(89, 20)
		Me.lblType.TabIndex = 18
		Me.lblType.Text = "Type"
		Me.lblType.TextAlign = System.Drawing.ContentAlignment.MiddleRight
		'
		'lblRoomID
		'
		Me.lblRoomID.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.lblRoomID.Location = New System.Drawing.Point(31, 19)
		Me.lblRoomID.Name = "lblRoomID"
		Me.lblRoomID.Size = New System.Drawing.Size(89, 20)
		Me.lblRoomID.TabIndex = 17
		Me.lblRoomID.Text = "RoomID"
		Me.lblRoomID.TextAlign = System.Drawing.ContentAlignment.MiddleRight
		'
		'btnCloseRoom
		'
		Me.btnCloseRoom.Location = New System.Drawing.Point(692, 294)
		Me.btnCloseRoom.Name = "btnCloseRoom"
		Me.btnCloseRoom.Size = New System.Drawing.Size(75, 23)
		Me.btnCloseRoom.TabIndex = 9
		Me.btnCloseRoom.Text = "Close"
		Me.btnCloseRoom.UseVisualStyleBackColor = True
		'
		'btnNewRoom
		'
		Me.btnNewRoom.Location = New System.Drawing.Point(677, 83)
		Me.btnNewRoom.Name = "btnNewRoom"
		Me.btnNewRoom.Size = New System.Drawing.Size(105, 23)
		Me.btnNewRoom.TabIndex = 7
		Me.btnNewRoom.Text = "New"
		Me.btnNewRoom.UseVisualStyleBackColor = True
		'
		'btnSubmitRoom
		'
		Me.btnSubmitRoom.Location = New System.Drawing.Point(677, 130)
		Me.btnSubmitRoom.Name = "btnSubmitRoom"
		Me.btnSubmitRoom.Size = New System.Drawing.Size(105, 23)
		Me.btnSubmitRoom.TabIndex = 13
		Me.btnSubmitRoom.Text = "Submit"
		Me.btnSubmitRoom.UseVisualStyleBackColor = True
		'
		'txtFindRoom
		'
		Me.txtFindRoom.Font = New System.Drawing.Font("Arial", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.txtFindRoom.Location = New System.Drawing.Point(464, 39)
		Me.txtFindRoom.Multiline = True
		Me.txtFindRoom.Name = "txtFindRoom"
		Me.txtFindRoom.Size = New System.Drawing.Size(473, 20)
		Me.txtFindRoom.TabIndex = 91
		'
		'btnFindRoom
		'
		Me.btnFindRoom.BackColor = System.Drawing.Color.Transparent
		Me.btnFindRoom.Font = New System.Drawing.Font("Arial", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.btnFindRoom.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
		Me.btnFindRoom.Location = New System.Drawing.Point(943, 36)
		Me.btnFindRoom.Name = "btnFindRoom"
		Me.btnFindRoom.Size = New System.Drawing.Size(62, 25)
		Me.btnFindRoom.TabIndex = 106
		Me.btnFindRoom.Text = "Find"
		Me.btnFindRoom.UseVisualStyleBackColor = False
		'
		'btnDeleteRoom
		'
		Me.btnDeleteRoom.Location = New System.Drawing.Point(677, 217)
		Me.btnDeleteRoom.Name = "btnDeleteRoom"
		Me.btnDeleteRoom.Size = New System.Drawing.Size(105, 23)
		Me.btnDeleteRoom.TabIndex = 111
		Me.btnDeleteRoom.Text = "Delete"
		Me.btnDeleteRoom.UseVisualStyleBackColor = True
		'
		'btnUpdateRoom
		'
		Me.btnUpdateRoom.Location = New System.Drawing.Point(677, 174)
		Me.btnUpdateRoom.Name = "btnUpdateRoom"
		Me.btnUpdateRoom.Size = New System.Drawing.Size(105, 23)
		Me.btnUpdateRoom.TabIndex = 110
		Me.btnUpdateRoom.Text = "Update"
		Me.btnUpdateRoom.UseVisualStyleBackColor = True
		'
		'btnLastRoom
		'
		Me.btnLastRoom.Location = New System.Drawing.Point(312, 324)
		Me.btnLastRoom.Name = "btnLastRoom"
		Me.btnLastRoom.Size = New System.Drawing.Size(70, 30)
		Me.btnLastRoom.TabIndex = 117
		Me.btnLastRoom.Text = ">|"
		Me.btnLastRoom.UseVisualStyleBackColor = True
		'
		'btnNextRoom
		'
		Me.btnNextRoom.Location = New System.Drawing.Point(216, 324)
		Me.btnNextRoom.Name = "btnNextRoom"
		Me.btnNextRoom.Size = New System.Drawing.Size(70, 30)
		Me.btnNextRoom.TabIndex = 116
		Me.btnNextRoom.Text = ">"
		Me.btnNextRoom.UseVisualStyleBackColor = True
		'
		'btnPrevRoom
		'
		Me.btnPrevRoom.Location = New System.Drawing.Point(122, 324)
		Me.btnPrevRoom.Name = "btnPrevRoom"
		Me.btnPrevRoom.Size = New System.Drawing.Size(70, 30)
		Me.btnPrevRoom.TabIndex = 115
		Me.btnPrevRoom.Text = "<"
		Me.btnPrevRoom.UseVisualStyleBackColor = True
		'
		'btnFirstRoom
		'
		Me.btnFirstRoom.Location = New System.Drawing.Point(31, 324)
		Me.btnFirstRoom.Name = "btnFirstRoom"
		Me.btnFirstRoom.Size = New System.Drawing.Size(70, 30)
		Me.btnFirstRoom.TabIndex = 114
		Me.btnFirstRoom.Text = "|<"
		Me.btnFirstRoom.UseVisualStyleBackColor = True
		'
		'MenuStrip1
		'
		Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.MenuToolStripMenuItem, Me.ReportToolStripMenuItem, Me.HelpToolStripMenuItem, Me.AboutToolStripMenuItem})
		Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
		Me.MenuStrip1.Name = "MenuStrip1"
		Me.MenuStrip1.Size = New System.Drawing.Size(1084, 24)
		Me.MenuStrip1.TabIndex = 118
		Me.MenuStrip1.Text = "MenuStrip1"
		'
		'MenuToolStripMenuItem
		'
		Me.MenuToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.CustomerToolStripMenuItem, Me.RoomToolStripMenuItem, Me.BookingToolStripMenuItem})
		Me.MenuToolStripMenuItem.Name = "MenuToolStripMenuItem"
		Me.MenuToolStripMenuItem.Size = New System.Drawing.Size(50, 20)
		Me.MenuToolStripMenuItem.Text = "Menu"
		'
		'CustomerToolStripMenuItem
		'
		Me.CustomerToolStripMenuItem.Name = "CustomerToolStripMenuItem"
		Me.CustomerToolStripMenuItem.Size = New System.Drawing.Size(126, 22)
		Me.CustomerToolStripMenuItem.Text = "Customer"
		'
		'RoomToolStripMenuItem
		'
		Me.RoomToolStripMenuItem.Name = "RoomToolStripMenuItem"
		Me.RoomToolStripMenuItem.Size = New System.Drawing.Size(126, 22)
		Me.RoomToolStripMenuItem.Text = "Room"
		'
		'BookingToolStripMenuItem
		'
		Me.BookingToolStripMenuItem.Name = "BookingToolStripMenuItem"
		Me.BookingToolStripMenuItem.Size = New System.Drawing.Size(126, 22)
		Me.BookingToolStripMenuItem.Text = "Booking"
		'
		'ReportToolStripMenuItem
		'
		Me.ReportToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.GenerateNormalReportToolStripMenuItem, Me.GenerateBreakReportToolStripMenuItem})
		Me.ReportToolStripMenuItem.Name = "ReportToolStripMenuItem"
		Me.ReportToolStripMenuItem.Size = New System.Drawing.Size(54, 20)
		Me.ReportToolStripMenuItem.Text = "Report"
		'
		'GenerateNormalReportToolStripMenuItem
		'
		Me.GenerateNormalReportToolStripMenuItem.Name = "GenerateNormalReportToolStripMenuItem"
		Me.GenerateNormalReportToolStripMenuItem.Size = New System.Drawing.Size(202, 22)
		Me.GenerateNormalReportToolStripMenuItem.Text = "Generate Normal Report"
		'
		'GenerateBreakReportToolStripMenuItem
		'
		Me.GenerateBreakReportToolStripMenuItem.Name = "GenerateBreakReportToolStripMenuItem"
		Me.GenerateBreakReportToolStripMenuItem.Size = New System.Drawing.Size(202, 22)
		Me.GenerateBreakReportToolStripMenuItem.Text = "Generate Break Report"
		'
		'HelpToolStripMenuItem
		'
		Me.HelpToolStripMenuItem.Name = "HelpToolStripMenuItem"
		Me.HelpToolStripMenuItem.Size = New System.Drawing.Size(44, 20)
		Me.HelpToolStripMenuItem.Text = "Help"
		'
		'AboutToolStripMenuItem
		'
		Me.AboutToolStripMenuItem.Name = "AboutToolStripMenuItem"
		Me.AboutToolStripMenuItem.Size = New System.Drawing.Size(52, 20)
		Me.AboutToolStripMenuItem.Text = "About"
		'
		'frmRoom
		'
		Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
		Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
		Me.ClientSize = New System.Drawing.Size(1084, 361)
		Me.Controls.Add(Me.MenuStrip1)
		Me.Controls.Add(Me.btnLastRoom)
		Me.Controls.Add(Me.btnNextRoom)
		Me.Controls.Add(Me.btnPrevRoom)
		Me.Controls.Add(Me.btnFirstRoom)
		Me.Controls.Add(Me.btnDeleteRoom)
		Me.Controls.Add(Me.btnUpdateRoom)
		Me.Controls.Add(Me.btnFindRoom)
		Me.Controls.Add(Me.txtFindRoom)
		Me.Controls.Add(Me.btnSubmitRoom)
		Me.Controls.Add(Me.btnCloseRoom)
		Me.Controls.Add(Me.btnNewRoom)
		Me.Controls.Add(Me.GroupBox1)
		Me.Name = "frmRoom"
		Me.Text = "Room"
		Me.GroupBox1.ResumeLayout(False)
		Me.GroupBox1.PerformLayout()
		CType(Me.picErrorRoomDescription, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.picErrorRoomAvailability, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.picErrorRoomPrice, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.picErrorRoomFloor, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.picErrorRoomNumber, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.picErrorRoomNumsOfBeds, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.picErrorRoomType, System.ComponentModel.ISupportInitialize).EndInit()
		Me.MenuStrip1.ResumeLayout(False)
		Me.MenuStrip1.PerformLayout()
		Me.ResumeLayout(False)
		Me.PerformLayout()

	End Sub
	Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents lblDescription As System.Windows.Forms.Label
    Friend WithEvents lblAvailability As System.Windows.Forms.Label
    Friend WithEvents lblPrice As System.Windows.Forms.Label
    Friend WithEvents lblFloor As System.Windows.Forms.Label
    Friend WithEvents lblRoomNumber As System.Windows.Forms.Label
    Friend WithEvents lblNumberOfBeds As System.Windows.Forms.Label
    Friend WithEvents lblType As System.Windows.Forms.Label
    Friend WithEvents lblRoomID As System.Windows.Forms.Label
    Friend WithEvents txtRoomID As System.Windows.Forms.TextBox
    Friend WithEvents picErrorRoomType As System.Windows.Forms.PictureBox
    Friend WithEvents picErrorRoomNumber As System.Windows.Forms.PictureBox
    Friend WithEvents picErrorRoomNumsOfBeds As System.Windows.Forms.PictureBox
	Friend WithEvents btnCloseRoom As System.Windows.Forms.Button
	Friend WithEvents btnNewRoom As System.Windows.Forms.Button
	Friend WithEvents btnSubmitRoom As System.Windows.Forms.Button
	Friend WithEvents txtRoomAvailability As System.Windows.Forms.TextBox
	Friend WithEvents txtRoomPrice As System.Windows.Forms.TextBox
	Friend WithEvents txtFloor As System.Windows.Forms.TextBox
	Friend WithEvents txtRoomNumber As System.Windows.Forms.TextBox
	Friend WithEvents txtNumsOfBeds As System.Windows.Forms.TextBox
	Friend WithEvents txtRoomType As System.Windows.Forms.TextBox
	Friend WithEvents txtRoomDescription As System.Windows.Forms.TextBox
	Friend WithEvents picErrorRoomDescription As System.Windows.Forms.PictureBox
	Friend WithEvents picErrorRoomAvailability As System.Windows.Forms.PictureBox
	Friend WithEvents picErrorRoomPrice As System.Windows.Forms.PictureBox
	Friend WithEvents picErrorRoomFloor As System.Windows.Forms.PictureBox
	Friend WithEvents txtFindRoom As TextBox
	Friend WithEvents btnFindRoom As Button
	Friend WithEvents btnDeleteRoom As Button
	Friend WithEvents btnUpdateRoom As Button
	Friend WithEvents btnLastRoom As Button
	Friend WithEvents btnNextRoom As Button
	Friend WithEvents btnPrevRoom As Button
	Friend WithEvents btnFirstRoom As Button
	Friend WithEvents MenuStrip1 As MenuStrip
	Friend WithEvents MenuToolStripMenuItem As ToolStripMenuItem
	Friend WithEvents CustomerToolStripMenuItem As ToolStripMenuItem
	Friend WithEvents RoomToolStripMenuItem As ToolStripMenuItem
	Friend WithEvents BookingToolStripMenuItem As ToolStripMenuItem
	Friend WithEvents ReportToolStripMenuItem As ToolStripMenuItem
	Friend WithEvents GenerateNormalReportToolStripMenuItem As ToolStripMenuItem
	Friend WithEvents GenerateBreakReportToolStripMenuItem As ToolStripMenuItem
	Friend WithEvents HelpToolStripMenuItem As ToolStripMenuItem
	Friend WithEvents AboutToolStripMenuItem As ToolStripMenuItem
End Class
