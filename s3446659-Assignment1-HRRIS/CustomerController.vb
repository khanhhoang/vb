﻿Option Explicit On
Option Strict On

Imports System.Data.OleDb

' Name:        CustomerController.vb
' Description: Intermediate between VB and access (Customer)
' Author:      Do Khanh Hoang
' Date:        28/03/2017

Public Class CustomerController
    'Connect to database
    Public Const CONNECTION_STRING As String = _
     "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=HRRISDB.accdb"
	'Insert value to database
	Public Function insert(ByVal htData As Hashtable) As Integer

		Dim oConnection As OleDbConnection = New OleDbConnection(CONNECTION_STRING)
		Dim iNumRows As Integer

		Try
			Debug.Print("Connection string: " & oConnection.ConnectionString)

			oConnection.Open()
			Dim oCommand As OleDbCommand = New OleDbCommand
			oCommand.Connection = oConnection

			'Insert to database
			oCommand.CommandText =
			   "INSERT INTO customer (title, gender, firstname, lastname, phone, address, email, dob) VALUES (?, ?, ?, ?, ?, ?, ?, ?);"

			oCommand.Parameters.Add("Title", OleDbType.VarChar, 255)
			oCommand.Parameters.Add("Gender", OleDbType.VarChar, 255)
			oCommand.Parameters.Add("FirstName", OleDbType.VarChar, 255)
			oCommand.Parameters.Add("LastName", OleDbType.VarChar, 255)
			oCommand.Parameters.Add("Phone", OleDbType.Integer, 10)
			oCommand.Parameters.Add("Address", OleDbType.VarChar, 255)
			oCommand.Parameters.Add("Email", OleDbType.VarChar, 255)
			oCommand.Parameters.Add("DOB", OleDbType.Date)

			'Define value type
			oCommand.Parameters("Title").Value = CStr(htData("Title"))
			oCommand.Parameters("Gender").Value = CStr(htData("Gender"))
			oCommand.Parameters("FirstName").Value = CStr(htData("FirstName"))
			oCommand.Parameters("LastName").Value = CStr(htData("LastName"))
			oCommand.Parameters("Phone").Value = CInt(htData("Phone"))
			oCommand.Parameters("Address").Value = CStr(htData("Address"))
			oCommand.Parameters("Email").Value = CStr(htData("Email"))
			oCommand.Parameters("DOB").Value = CDate(htData("DOB"))

			oCommand.Prepare()

			iNumRows = oCommand.ExecuteNonQuery()
			Debug.Print(CStr(iNumRows))

			Debug.Print("SQL: " & oCommand.CommandText)

			Debug.Print("The record was inserted.")
			MsgBox("The record was inserted")

		Catch ex As Exception
			Debug.Print("ERROR: " & ex.Message)
			MsgBox("An error occured. The record wasn't inserted.")
		Finally
			oConnection.Close()
		End Try

		Return iNumRows

	End Function
	'Find all
	Public Function findAll() As List(Of Hashtable)

		Dim oConnection As OleDbConnection = New OleDbConnection(CONNECTION_STRING)
		Dim lsData As New List(Of Hashtable)

		Try
			Debug.Print("Connection string: " & oConnection.ConnectionString)

			oConnection.Open()
			Dim oCommand As OleDbCommand = New OleDbCommand
			oCommand.Connection = oConnection

			oCommand.CommandText =
				"SELECT * FROM customer ORDER BY customer_id;"
			oCommand.Prepare()
			Dim oDataReader = oCommand.ExecuteReader()

			Dim htTempData As Hashtable
			Do While oDataReader.Read() = True
				htTempData = New Hashtable
				htTempData("CustomerID") = CStr(oDataReader("customer_id"))
				htTempData("Title") = CStr(oDataReader("title"))
				htTempData("Gender") = CStr(oDataReader("gender"))
				htTempData("FirstName") = CStr(oDataReader("firstname"))
				htTempData("LastName") = CStr(oDataReader("lastname"))
				htTempData("Phone") = CStr(oDataReader("phone"))
				htTempData("Address") = CStr(oDataReader("address"))
				htTempData("Email") = CStr(oDataReader("email"))
				htTempData("DOB") = CStr(oDataReader("dob"))
				lsData.Add(htTempData)
			Loop

			Debug.Print("The records was found.")

		Catch ex As Exception
			Debug.Print("ERROR: " & ex.Message)
			MsgBox("An error occurred. The records could not be found!")
		Finally
			oConnection.Close()
		End Try

		Return lsData

	End Function
	'Find by FirstName
	Public Function findByFirstName(sFirstName As String) As List(Of Hashtable)

		Dim oConnection As OleDbConnection = New OleDbConnection(CONNECTION_STRING)
		Dim lsData As New List(Of Hashtable)

		Try
			Debug.Print("Connection string: " & oConnection.ConnectionString)

			oConnection.Open()
			Dim oCommand As OleDbCommand = New OleDbCommand
			oCommand.Connection = oConnection

			oCommand.CommandText =
				"SELECT * FROM customer WHERE firstname = ?;"
			oCommand.Parameters.Add("firstname", OleDbType.VarChar, 8)
			oCommand.Parameters("firstname").Value = CStr(sFirstName)
			oCommand.Prepare()
			Dim oDataReader = oCommand.ExecuteReader()

			Dim htTempData As Hashtable
			Do While oDataReader.Read() = True
				htTempData = New Hashtable
				htTempData("CustomerID") = CStr(oDataReader("customer_id"))
				htTempData("Title") = CStr(oDataReader("title"))
				htTempData("Gender") = CStr(oDataReader("gender"))
				htTempData("FirstName") = CStr(oDataReader("firstname"))
				htTempData("LastName") = CStr(oDataReader("lastname"))
				htTempData("Phone") = CStr(oDataReader("phone"))
				htTempData("Address") = CStr(oDataReader("address"))
				htTempData("Email") = CStr(oDataReader("email"))
				htTempData("DOB") = CStr(oDataReader("dob"))
				lsData.Add(htTempData)
			Loop

			Debug.Print("The record was found.")

		Catch ex As Exception
			Debug.Print("ERROR: " & ex.Message)
			MsgBox("An error occurred. The record(s) could not be found!")
		Finally
			oConnection.Close()
		End Try

		Return lsData

	End Function
	'Find by ID
	Public Function findByID(sID As String) As List(Of Hashtable)

		Dim oConnection As OleDbConnection = New OleDbConnection(CONNECTION_STRING)
		Dim lsData As New List(Of Hashtable)

		Try
			Debug.Print("Connection string: " & oConnection.ConnectionString)

			oConnection.Open()
			Dim oCommand As OleDbCommand = New OleDbCommand
			oCommand.Connection = oConnection

			oCommand.CommandText =
				"SELECT * FROM customer WHERE customer_id = ?;"
			oCommand.Parameters.Add("customer_id", OleDbType.Integer, 8)
			oCommand.Parameters("customer_id").Value = CInt(sID)
			oCommand.Prepare()
			Dim oDataReader = oCommand.ExecuteReader()

			Dim htTempData As Hashtable
			Do While oDataReader.Read() = True
				htTempData = New Hashtable
				htTempData("CustomerID") = CStr(oDataReader("customer_id"))
				htTempData("Title") = CStr(oDataReader("title"))
				htTempData("Gender") = CStr(oDataReader("gender"))
				htTempData("FirstName") = CStr(oDataReader("firstname"))
				htTempData("LastName") = CStr(oDataReader("lastname"))
				htTempData("Phone") = CStr(oDataReader("phone"))
				htTempData("Address") = CStr(oDataReader("address"))
				htTempData("Email") = CStr(oDataReader("email"))
				htTempData("DOB") = CStr(oDataReader("dob"))
				lsData.Add(htTempData)
			Loop

			Debug.Print("Read the record successfully")

		Catch ex As Exception
			Debug.Print("ERROR: " & ex.Message)
			MsgBox("No records were found!")
		Finally
			oConnection.Close()
		End Try

		Return lsData

	End Function
	'Update Function
	Public Function update(ByVal htData As Hashtable) As Integer

		Dim oConnection As OleDbConnection = New OleDbConnection(CONNECTION_STRING)
		Dim iNumRows As Integer

		Try
			Debug.Print("Connection string: " & oConnection.ConnectionString)

			oConnection.Open()
			Dim oCommand As OleDbCommand = New OleDbCommand
			oCommand.Connection = oConnection

			oCommand.CommandText =
				"UPDATE customer SET title = ?, gender = ?, firstname = ?, lastname = ?, phone = ?, address = ?, email = ?, dob = ? WHERE customer_id = ?;"

			oCommand.Parameters.Add("Title", OleDbType.VarChar, 255)
			oCommand.Parameters.Add("Gender", OleDbType.VarChar, 255)
			oCommand.Parameters.Add("FirstName", OleDbType.VarChar, 255)
			oCommand.Parameters.Add("LastName", OleDbType.VarChar, 255)
			oCommand.Parameters.Add("Phone", OleDbType.Integer, 10)
			oCommand.Parameters.Add("Address", OleDbType.VarChar, 255)
			oCommand.Parameters.Add("Email", OleDbType.VarChar, 255)
			oCommand.Parameters.Add("DOB", OleDbType.Date)
			oCommand.Parameters.Add("CustomerID", OleDbType.Integer, 1)

			'Define value type
			oCommand.Parameters("Title").Value = CStr(htData("Title"))
			oCommand.Parameters("Gender").Value = CStr(htData("Gender"))
			oCommand.Parameters("FirstName").Value = CStr(htData("FirstName"))
			oCommand.Parameters("LastName").Value = CStr(htData("LastName"))
			oCommand.Parameters("Phone").Value = CInt(htData("Phone"))
			oCommand.Parameters("Address").Value = CStr(htData("Address"))
			oCommand.Parameters("Email").Value = CStr(htData("Email"))
			oCommand.Parameters("DOB").Value = CDate(htData("DOB"))
			oCommand.Parameters("CustomerID").Value = CInt(htData("CustomerID"))

			oCommand.Prepare()
			iNumRows = oCommand.ExecuteNonQuery()

			Debug.Print(CStr(iNumRows))
			Debug.Print("The record was updated.")

		Catch ex As Exception
			Debug.Print("ERROR: " & ex.Message)
			MsgBox("An error occurred. The record was not updated!")
		Finally
			oConnection.Close()
		End Try

		Return iNumRows
	End Function
	'Delete function
	Public Function delete(sId As String) As Integer
		Dim oConnection As OleDbConnection = New OleDbConnection(CONNECTION_STRING)
		Dim iNumRows As Integer

		Try
			Debug.Print("Connection string: " & oConnection.ConnectionString)

			oConnection.Open()
			Dim oCommand As OleDbCommand = New OleDbCommand

			oCommand.Connection = oConnection
			oCommand.CommandText =
			   "DELETE FROM booking WHERE customer_id = ?;"
			oCommand.Parameters.Add("CustomerID", OleDbType.Integer, 8)
			oCommand.Parameters("CustomerID").Value = CInt(sId)
			oCommand.Prepare()
			iNumRows = oCommand.ExecuteNonQuery()

			oCommand.Connection = oConnection
			oCommand.CommandText =
			   "DELETE FROM customer WHERE customer_id = ?;"
			oCommand.Parameters.Add("CustomerID", OleDbType.Integer, 8)
			oCommand.Parameters("CustomerID").Value = CInt(sId)
			oCommand.Prepare()
			iNumRows = oCommand.ExecuteNonQuery()

			Debug.Print(CStr(iNumRows))
			Debug.Print("The record was deleted.")

		Catch ex As Exception
			Debug.Print("ERROR: " & ex.Message)
		Finally
			oConnection.Close()
		End Try

		Return iNumRows

	End Function

End Class
