﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmBooking
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
		Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmBooking))
		Me.btnGoToRoom = New System.Windows.Forms.Button()
		Me.btnGoToCustomer = New System.Windows.Forms.Button()
		Me.btnCloseBooking = New System.Windows.Forms.Button()
		Me.btnNewBooking = New System.Windows.Forms.Button()
		Me.GroupBox1 = New System.Windows.Forms.GroupBox()
		Me.nudNumberOfDays = New System.Windows.Forms.NumericUpDown()
		Me.picErrorCheckinDate = New System.Windows.Forms.PictureBox()
		Me.picErrorNumOfGuests = New System.Windows.Forms.PictureBox()
		Me.picErrorRoomID = New System.Windows.Forms.PictureBox()
		Me.picErrorCustomerID = New System.Windows.Forms.PictureBox()
		Me.picErrorNumOfDays = New System.Windows.Forms.PictureBox()
		Me.dtpCheckinDate = New System.Windows.Forms.DateTimePicker()
		Me.dtpBookingDate = New System.Windows.Forms.DateTimePicker()
		Me.txtTotalPrice = New System.Windows.Forms.TextBox()
		Me.lblTotalPrice = New System.Windows.Forms.Label()
		Me.cboCustomerID = New System.Windows.Forms.ComboBox()
		Me.cboRoomID = New System.Windows.Forms.ComboBox()
		Me.txtComments = New System.Windows.Forms.TextBox()
		Me.txtNumberOfGuests = New System.Windows.Forms.TextBox()
		Me.txtBookingID = New System.Windows.Forms.TextBox()
		Me.lblComments = New System.Windows.Forms.Label()
		Me.lblCheckinDate = New System.Windows.Forms.Label()
		Me.lblNumberofGuests = New System.Windows.Forms.Label()
		Me.lblNumberOfDays = New System.Windows.Forms.Label()
		Me.lblCustomerID = New System.Windows.Forms.Label()
		Me.lblRoomID = New System.Windows.Forms.Label()
		Me.lblDate = New System.Windows.Forms.Label()
		Me.lblBookingID = New System.Windows.Forms.Label()
		Me.lblDescription = New System.Windows.Forms.Label()
		Me.btnSubmitBooking = New System.Windows.Forms.Button()
		Me.btnGoToBooking = New System.Windows.Forms.Button()
		Me.btnFindBooking = New System.Windows.Forms.Button()
		Me.txtFindBooking = New System.Windows.Forms.TextBox()
		Me.btnDeleteBooking = New System.Windows.Forms.Button()
		Me.btnUpdateBooking = New System.Windows.Forms.Button()
		Me.btnLastBooking = New System.Windows.Forms.Button()
		Me.btnNextBooking = New System.Windows.Forms.Button()
		Me.btnPrevBooking = New System.Windows.Forms.Button()
		Me.btnFirstBooking = New System.Windows.Forms.Button()
		Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
		Me.MenuToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
		Me.CustomerToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
		Me.RoomToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
		Me.BookingToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
		Me.ReportToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
		Me.GenerateNormalReportToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
		Me.GenerateBreakReportToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
		Me.HelpToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
		Me.AboutToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
		Me.GroupBox1.SuspendLayout()
		CType(Me.nudNumberOfDays, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.picErrorCheckinDate, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.picErrorNumOfGuests, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.picErrorRoomID, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.picErrorCustomerID, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.picErrorNumOfDays, System.ComponentModel.ISupportInitialize).BeginInit()
		Me.MenuStrip1.SuspendLayout()
		Me.SuspendLayout()
		'
		'btnGoToRoom
		'
		Me.btnGoToRoom.Location = New System.Drawing.Point(557, 131)
		Me.btnGoToRoom.Name = "btnGoToRoom"
		Me.btnGoToRoom.Size = New System.Drawing.Size(0, 0)
		Me.btnGoToRoom.TabIndex = 18
		Me.btnGoToRoom.Text = "Room Details"
		Me.btnGoToRoom.UseVisualStyleBackColor = True
		'
		'btnGoToCustomer
		'
		Me.btnGoToCustomer.Location = New System.Drawing.Point(557, 76)
		Me.btnGoToCustomer.Name = "btnGoToCustomer"
		Me.btnGoToCustomer.Size = New System.Drawing.Size(0, 0)
		Me.btnGoToCustomer.TabIndex = 17
		Me.btnGoToCustomer.Text = "Customer Details"
		Me.btnGoToCustomer.UseVisualStyleBackColor = True
		'
		'btnCloseBooking
		'
		Me.btnCloseBooking.Location = New System.Drawing.Point(687, 267)
		Me.btnCloseBooking.Name = "btnCloseBooking"
		Me.btnCloseBooking.Size = New System.Drawing.Size(75, 23)
		Me.btnCloseBooking.TabIndex = 16
		Me.btnCloseBooking.Text = "Close"
		Me.btnCloseBooking.UseVisualStyleBackColor = True
		'
		'btnNewBooking
		'
		Me.btnNewBooking.Location = New System.Drawing.Point(667, 76)
		Me.btnNewBooking.Name = "btnNewBooking"
		Me.btnNewBooking.Size = New System.Drawing.Size(105, 23)
		Me.btnNewBooking.TabIndex = 14
		Me.btnNewBooking.Text = "New"
		Me.btnNewBooking.UseVisualStyleBackColor = True
		'
		'GroupBox1
		'
		Me.GroupBox1.Controls.Add(Me.nudNumberOfDays)
		Me.GroupBox1.Controls.Add(Me.picErrorCheckinDate)
		Me.GroupBox1.Controls.Add(Me.picErrorNumOfGuests)
		Me.GroupBox1.Controls.Add(Me.picErrorRoomID)
		Me.GroupBox1.Controls.Add(Me.picErrorCustomerID)
		Me.GroupBox1.Controls.Add(Me.picErrorNumOfDays)
		Me.GroupBox1.Controls.Add(Me.dtpCheckinDate)
		Me.GroupBox1.Controls.Add(Me.dtpBookingDate)
		Me.GroupBox1.Controls.Add(Me.txtTotalPrice)
		Me.GroupBox1.Controls.Add(Me.lblTotalPrice)
		Me.GroupBox1.Controls.Add(Me.cboCustomerID)
		Me.GroupBox1.Controls.Add(Me.cboRoomID)
		Me.GroupBox1.Controls.Add(Me.txtComments)
		Me.GroupBox1.Controls.Add(Me.txtNumberOfGuests)
		Me.GroupBox1.Controls.Add(Me.txtBookingID)
		Me.GroupBox1.Controls.Add(Me.lblComments)
		Me.GroupBox1.Controls.Add(Me.lblCheckinDate)
		Me.GroupBox1.Controls.Add(Me.lblNumberofGuests)
		Me.GroupBox1.Controls.Add(Me.lblNumberOfDays)
		Me.GroupBox1.Controls.Add(Me.lblCustomerID)
		Me.GroupBox1.Controls.Add(Me.lblRoomID)
		Me.GroupBox1.Controls.Add(Me.lblDate)
		Me.GroupBox1.Controls.Add(Me.lblBookingID)
		Me.GroupBox1.Location = New System.Drawing.Point(3, 21)
		Me.GroupBox1.Name = "GroupBox1"
		Me.GroupBox1.Size = New System.Drawing.Size(430, 301)
		Me.GroupBox1.TabIndex = 13
		Me.GroupBox1.TabStop = False
		Me.GroupBox1.Text = "Booking Details"
		'
		'nudNumberOfDays
		'
		Me.nudNumberOfDays.Location = New System.Drawing.Point(138, 132)
		Me.nudNumberOfDays.Name = "nudNumberOfDays"
		Me.nudNumberOfDays.Size = New System.Drawing.Size(183, 20)
		Me.nudNumberOfDays.TabIndex = 99
		'
		'picErrorCheckinDate
		'
		Me.picErrorCheckinDate.Image = CType(resources.GetObject("picErrorCheckinDate.Image"), System.Drawing.Image)
		Me.picErrorCheckinDate.Location = New System.Drawing.Point(327, 191)
		Me.picErrorCheckinDate.Name = "picErrorCheckinDate"
		Me.picErrorCheckinDate.Size = New System.Drawing.Size(20, 19)
		Me.picErrorCheckinDate.TabIndex = 98
		Me.picErrorCheckinDate.TabStop = False
		Me.picErrorCheckinDate.Visible = False
		'
		'picErrorNumOfGuests
		'
		Me.picErrorNumOfGuests.Image = CType(resources.GetObject("picErrorNumOfGuests.Image"), System.Drawing.Image)
		Me.picErrorNumOfGuests.Location = New System.Drawing.Point(327, 161)
		Me.picErrorNumOfGuests.Name = "picErrorNumOfGuests"
		Me.picErrorNumOfGuests.Size = New System.Drawing.Size(20, 19)
		Me.picErrorNumOfGuests.TabIndex = 97
		Me.picErrorNumOfGuests.TabStop = False
		Me.picErrorNumOfGuests.Visible = False
		'
		'picErrorRoomID
		'
		Me.picErrorRoomID.Image = CType(resources.GetObject("picErrorRoomID.Image"), System.Drawing.Image)
		Me.picErrorRoomID.Location = New System.Drawing.Point(327, 71)
		Me.picErrorRoomID.Name = "picErrorRoomID"
		Me.picErrorRoomID.Size = New System.Drawing.Size(20, 19)
		Me.picErrorRoomID.TabIndex = 96
		Me.picErrorRoomID.TabStop = False
		Me.picErrorRoomID.Visible = False
		'
		'picErrorCustomerID
		'
		Me.picErrorCustomerID.Image = CType(resources.GetObject("picErrorCustomerID.Image"), System.Drawing.Image)
		Me.picErrorCustomerID.Location = New System.Drawing.Point(327, 102)
		Me.picErrorCustomerID.Name = "picErrorCustomerID"
		Me.picErrorCustomerID.Size = New System.Drawing.Size(20, 19)
		Me.picErrorCustomerID.TabIndex = 95
		Me.picErrorCustomerID.TabStop = False
		Me.picErrorCustomerID.Visible = False
		'
		'picErrorNumOfDays
		'
		Me.picErrorNumOfDays.Image = CType(resources.GetObject("picErrorNumOfDays.Image"), System.Drawing.Image)
		Me.picErrorNumOfDays.Location = New System.Drawing.Point(327, 131)
		Me.picErrorNumOfDays.Name = "picErrorNumOfDays"
		Me.picErrorNumOfDays.Size = New System.Drawing.Size(20, 19)
		Me.picErrorNumOfDays.TabIndex = 94
		Me.picErrorNumOfDays.TabStop = False
		Me.picErrorNumOfDays.Visible = False
		'
		'dtpCheckinDate
		'
		Me.dtpCheckinDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
		Me.dtpCheckinDate.Location = New System.Drawing.Point(137, 190)
		Me.dtpCheckinDate.Name = "dtpCheckinDate"
		Me.dtpCheckinDate.Size = New System.Drawing.Size(184, 20)
		Me.dtpCheckinDate.TabIndex = 93
		'
		'dtpBookingDate
		'
		Me.dtpBookingDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
		Me.dtpBookingDate.Location = New System.Drawing.Point(137, 44)
		Me.dtpBookingDate.Name = "dtpBookingDate"
		Me.dtpBookingDate.Size = New System.Drawing.Size(184, 20)
		Me.dtpBookingDate.TabIndex = 92
		'
		'txtTotalPrice
		'
		Me.txtTotalPrice.Enabled = False
		Me.txtTotalPrice.Location = New System.Drawing.Point(137, 212)
		Me.txtTotalPrice.Name = "txtTotalPrice"
		Me.txtTotalPrice.Size = New System.Drawing.Size(184, 20)
		Me.txtTotalPrice.TabIndex = 91
		'
		'lblTotalPrice
		'
		Me.lblTotalPrice.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.lblTotalPrice.Location = New System.Drawing.Point(29, 212)
		Me.lblTotalPrice.Name = "lblTotalPrice"
		Me.lblTotalPrice.Size = New System.Drawing.Size(102, 20)
		Me.lblTotalPrice.TabIndex = 90
		Me.lblTotalPrice.Text = "Total Price"
		Me.lblTotalPrice.TextAlign = System.Drawing.ContentAlignment.MiddleRight
		'
		'cboCustomerID
		'
		Me.cboCustomerID.BackColor = System.Drawing.Color.LightGray
		Me.cboCustomerID.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
		Me.cboCustomerID.ForeColor = System.Drawing.SystemColors.Window
		Me.cboCustomerID.FormattingEnabled = True
		Me.cboCustomerID.Location = New System.Drawing.Point(137, 102)
		Me.cboCustomerID.Name = "cboCustomerID"
		Me.cboCustomerID.Size = New System.Drawing.Size(184, 21)
		Me.cboCustomerID.TabIndex = 80
		'
		'cboRoomID
		'
		Me.cboRoomID.BackColor = System.Drawing.Color.LightGray
		Me.cboRoomID.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
		Me.cboRoomID.ForeColor = System.Drawing.SystemColors.Window
		Me.cboRoomID.FormattingEnabled = True
		Me.cboRoomID.Location = New System.Drawing.Point(137, 73)
		Me.cboRoomID.Name = "cboRoomID"
		Me.cboRoomID.Size = New System.Drawing.Size(184, 21)
		Me.cboRoomID.TabIndex = 80
		'
		'txtComments
		'
		Me.txtComments.Font = New System.Drawing.Font("Arial", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.txtComments.Location = New System.Drawing.Point(137, 238)
		Me.txtComments.Multiline = True
		Me.txtComments.Name = "txtComments"
		Me.txtComments.Size = New System.Drawing.Size(285, 56)
		Me.txtComments.TabIndex = 78
		'
		'txtNumberOfGuests
		'
		Me.txtNumberOfGuests.Location = New System.Drawing.Point(137, 161)
		Me.txtNumberOfGuests.Name = "txtNumberOfGuests"
		Me.txtNumberOfGuests.Size = New System.Drawing.Size(184, 20)
		Me.txtNumberOfGuests.TabIndex = 27
		'
		'txtBookingID
		'
		Me.txtBookingID.Enabled = False
		Me.txtBookingID.Location = New System.Drawing.Point(137, 17)
		Me.txtBookingID.Name = "txtBookingID"
		Me.txtBookingID.Size = New System.Drawing.Size(184, 20)
		Me.txtBookingID.TabIndex = 25
		'
		'lblComments
		'
		Me.lblComments.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.lblComments.Location = New System.Drawing.Point(29, 236)
		Me.lblComments.Name = "lblComments"
		Me.lblComments.Size = New System.Drawing.Size(102, 20)
		Me.lblComments.TabIndex = 24
		Me.lblComments.Text = "Comments"
		Me.lblComments.TextAlign = System.Drawing.ContentAlignment.MiddleRight
		'
		'lblCheckinDate
		'
		Me.lblCheckinDate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.lblCheckinDate.Location = New System.Drawing.Point(29, 190)
		Me.lblCheckinDate.Name = "lblCheckinDate"
		Me.lblCheckinDate.Size = New System.Drawing.Size(102, 20)
		Me.lblCheckinDate.TabIndex = 23
		Me.lblCheckinDate.Text = "Checkin Date"
		Me.lblCheckinDate.TextAlign = System.Drawing.ContentAlignment.MiddleRight
		'
		'lblNumberofGuests
		'
		Me.lblNumberofGuests.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.lblNumberofGuests.Location = New System.Drawing.Point(29, 161)
		Me.lblNumberofGuests.Name = "lblNumberofGuests"
		Me.lblNumberofGuests.Size = New System.Drawing.Size(102, 20)
		Me.lblNumberofGuests.TabIndex = 22
		Me.lblNumberofGuests.Text = "Number of Guests"
		Me.lblNumberofGuests.TextAlign = System.Drawing.ContentAlignment.MiddleRight
		'
		'lblNumberOfDays
		'
		Me.lblNumberOfDays.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.lblNumberOfDays.Location = New System.Drawing.Point(29, 132)
		Me.lblNumberOfDays.Name = "lblNumberOfDays"
		Me.lblNumberOfDays.Size = New System.Drawing.Size(102, 20)
		Me.lblNumberOfDays.TabIndex = 21
		Me.lblNumberOfDays.Text = "Number of Days"
		Me.lblNumberOfDays.TextAlign = System.Drawing.ContentAlignment.MiddleRight
		'
		'lblCustomerID
		'
		Me.lblCustomerID.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.lblCustomerID.Location = New System.Drawing.Point(29, 103)
		Me.lblCustomerID.Name = "lblCustomerID"
		Me.lblCustomerID.Size = New System.Drawing.Size(102, 20)
		Me.lblCustomerID.TabIndex = 20
		Me.lblCustomerID.Text = "CustomerID"
		Me.lblCustomerID.TextAlign = System.Drawing.ContentAlignment.MiddleRight
		'
		'lblRoomID
		'
		Me.lblRoomID.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.lblRoomID.Location = New System.Drawing.Point(29, 74)
		Me.lblRoomID.Name = "lblRoomID"
		Me.lblRoomID.Size = New System.Drawing.Size(102, 20)
		Me.lblRoomID.TabIndex = 19
		Me.lblRoomID.Text = "RoomID"
		Me.lblRoomID.TextAlign = System.Drawing.ContentAlignment.MiddleRight
		'
		'lblDate
		'
		Me.lblDate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.lblDate.Location = New System.Drawing.Point(29, 45)
		Me.lblDate.Name = "lblDate"
		Me.lblDate.Size = New System.Drawing.Size(102, 20)
		Me.lblDate.TabIndex = 18
		Me.lblDate.Text = "Date"
		Me.lblDate.TextAlign = System.Drawing.ContentAlignment.MiddleRight
		'
		'lblBookingID
		'
		Me.lblBookingID.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.lblBookingID.Location = New System.Drawing.Point(29, 16)
		Me.lblBookingID.Name = "lblBookingID"
		Me.lblBookingID.Size = New System.Drawing.Size(102, 20)
		Me.lblBookingID.TabIndex = 17
		Me.lblBookingID.Text = "BookingID"
		Me.lblBookingID.TextAlign = System.Drawing.ContentAlignment.MiddleRight
		'
		'lblDescription
		'
		Me.lblDescription.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.lblDescription.Location = New System.Drawing.Point(6, 236)
		Me.lblDescription.Name = "lblDescription"
		Me.lblDescription.Size = New System.Drawing.Size(102, 20)
		Me.lblDescription.TabIndex = 24
		Me.lblDescription.Text = "Comments"
		Me.lblDescription.TextAlign = System.Drawing.ContentAlignment.MiddleRight
		'
		'btnSubmitBooking
		'
		Me.btnSubmitBooking.Location = New System.Drawing.Point(667, 115)
		Me.btnSubmitBooking.Name = "btnSubmitBooking"
		Me.btnSubmitBooking.Size = New System.Drawing.Size(105, 23)
		Me.btnSubmitBooking.TabIndex = 15
		Me.btnSubmitBooking.Text = "Submit"
		Me.btnSubmitBooking.UseVisualStyleBackColor = True
		'
		'btnGoToBooking
		'
		Me.btnGoToBooking.Location = New System.Drawing.Point(557, 187)
		Me.btnGoToBooking.Name = "btnGoToBooking"
		Me.btnGoToBooking.Size = New System.Drawing.Size(0, 0)
		Me.btnGoToBooking.TabIndex = 19
		Me.btnGoToBooking.Text = "Booking Details"
		Me.btnGoToBooking.UseVisualStyleBackColor = True
		'
		'btnFindBooking
		'
		Me.btnFindBooking.BackColor = System.Drawing.Color.Transparent
		Me.btnFindBooking.Font = New System.Drawing.Font("Arial", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.btnFindBooking.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
		Me.btnFindBooking.Location = New System.Drawing.Point(933, 32)
		Me.btnFindBooking.Name = "btnFindBooking"
		Me.btnFindBooking.Size = New System.Drawing.Size(62, 25)
		Me.btnFindBooking.TabIndex = 108
		Me.btnFindBooking.Text = "Find"
		Me.btnFindBooking.UseVisualStyleBackColor = False
		'
		'txtFindBooking
		'
		Me.txtFindBooking.Font = New System.Drawing.Font("Arial", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.txtFindBooking.Location = New System.Drawing.Point(454, 35)
		Me.txtFindBooking.Multiline = True
		Me.txtFindBooking.Name = "txtFindBooking"
		Me.txtFindBooking.Size = New System.Drawing.Size(473, 20)
		Me.txtFindBooking.TabIndex = 107
		'
		'btnDeleteBooking
		'
		Me.btnDeleteBooking.Location = New System.Drawing.Point(667, 197)
		Me.btnDeleteBooking.Name = "btnDeleteBooking"
		Me.btnDeleteBooking.Size = New System.Drawing.Size(105, 23)
		Me.btnDeleteBooking.TabIndex = 113
		Me.btnDeleteBooking.Text = "Delete"
		Me.btnDeleteBooking.UseVisualStyleBackColor = True
		'
		'btnUpdateBooking
		'
		Me.btnUpdateBooking.Location = New System.Drawing.Point(667, 154)
		Me.btnUpdateBooking.Name = "btnUpdateBooking"
		Me.btnUpdateBooking.Size = New System.Drawing.Size(105, 23)
		Me.btnUpdateBooking.TabIndex = 112
		Me.btnUpdateBooking.Text = "Update"
		Me.btnUpdateBooking.UseVisualStyleBackColor = True
		'
		'btnLastBooking
		'
		Me.btnLastBooking.Location = New System.Drawing.Point(315, 328)
		Me.btnLastBooking.Name = "btnLastBooking"
		Me.btnLastBooking.Size = New System.Drawing.Size(70, 30)
		Me.btnLastBooking.TabIndex = 121
		Me.btnLastBooking.Text = ">|"
		Me.btnLastBooking.UseVisualStyleBackColor = True
		'
		'btnNextBooking
		'
		Me.btnNextBooking.Location = New System.Drawing.Point(219, 328)
		Me.btnNextBooking.Name = "btnNextBooking"
		Me.btnNextBooking.Size = New System.Drawing.Size(70, 30)
		Me.btnNextBooking.TabIndex = 120
		Me.btnNextBooking.Text = ">"
		Me.btnNextBooking.UseVisualStyleBackColor = True
		'
		'btnPrevBooking
		'
		Me.btnPrevBooking.Location = New System.Drawing.Point(125, 328)
		Me.btnPrevBooking.Name = "btnPrevBooking"
		Me.btnPrevBooking.Size = New System.Drawing.Size(70, 30)
		Me.btnPrevBooking.TabIndex = 119
		Me.btnPrevBooking.Text = "<"
		Me.btnPrevBooking.UseVisualStyleBackColor = True
		'
		'btnFirstBooking
		'
		Me.btnFirstBooking.Location = New System.Drawing.Point(34, 328)
		Me.btnFirstBooking.Name = "btnFirstBooking"
		Me.btnFirstBooking.Size = New System.Drawing.Size(70, 30)
		Me.btnFirstBooking.TabIndex = 118
		Me.btnFirstBooking.Text = "|<"
		Me.btnFirstBooking.UseVisualStyleBackColor = True
		'
		'MenuStrip1
		'
		Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.MenuToolStripMenuItem, Me.ReportToolStripMenuItem, Me.HelpToolStripMenuItem, Me.AboutToolStripMenuItem})
		Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
		Me.MenuStrip1.Name = "MenuStrip1"
		Me.MenuStrip1.Size = New System.Drawing.Size(1084, 24)
		Me.MenuStrip1.TabIndex = 122
		Me.MenuStrip1.Text = "MenuStrip1"
		'
		'MenuToolStripMenuItem
		'
		Me.MenuToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.CustomerToolStripMenuItem, Me.RoomToolStripMenuItem, Me.BookingToolStripMenuItem})
		Me.MenuToolStripMenuItem.Name = "MenuToolStripMenuItem"
		Me.MenuToolStripMenuItem.Size = New System.Drawing.Size(50, 20)
		Me.MenuToolStripMenuItem.Text = "Menu"
		'
		'CustomerToolStripMenuItem
		'
		Me.CustomerToolStripMenuItem.Name = "CustomerToolStripMenuItem"
		Me.CustomerToolStripMenuItem.Size = New System.Drawing.Size(126, 22)
		Me.CustomerToolStripMenuItem.Text = "Customer"
		'
		'RoomToolStripMenuItem
		'
		Me.RoomToolStripMenuItem.Name = "RoomToolStripMenuItem"
		Me.RoomToolStripMenuItem.Size = New System.Drawing.Size(126, 22)
		Me.RoomToolStripMenuItem.Text = "Room"
		'
		'BookingToolStripMenuItem
		'
		Me.BookingToolStripMenuItem.Name = "BookingToolStripMenuItem"
		Me.BookingToolStripMenuItem.Size = New System.Drawing.Size(126, 22)
		Me.BookingToolStripMenuItem.Text = "Booking"
		'
		'ReportToolStripMenuItem
		'
		Me.ReportToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.GenerateNormalReportToolStripMenuItem, Me.GenerateBreakReportToolStripMenuItem})
		Me.ReportToolStripMenuItem.Name = "ReportToolStripMenuItem"
		Me.ReportToolStripMenuItem.Size = New System.Drawing.Size(54, 20)
		Me.ReportToolStripMenuItem.Text = "Report"
		'
		'GenerateNormalReportToolStripMenuItem
		'
		Me.GenerateNormalReportToolStripMenuItem.Name = "GenerateNormalReportToolStripMenuItem"
		Me.GenerateNormalReportToolStripMenuItem.Size = New System.Drawing.Size(202, 22)
		Me.GenerateNormalReportToolStripMenuItem.Text = "Generate Normal Report"
		'
		'GenerateBreakReportToolStripMenuItem
		'
		Me.GenerateBreakReportToolStripMenuItem.Name = "GenerateBreakReportToolStripMenuItem"
		Me.GenerateBreakReportToolStripMenuItem.Size = New System.Drawing.Size(202, 22)
		Me.GenerateBreakReportToolStripMenuItem.Text = "Generate Break Report"
		'
		'HelpToolStripMenuItem
		'
		Me.HelpToolStripMenuItem.Name = "HelpToolStripMenuItem"
		Me.HelpToolStripMenuItem.Size = New System.Drawing.Size(44, 20)
		Me.HelpToolStripMenuItem.Text = "Help"
		'
		'AboutToolStripMenuItem
		'
		Me.AboutToolStripMenuItem.Name = "AboutToolStripMenuItem"
		Me.AboutToolStripMenuItem.Size = New System.Drawing.Size(52, 20)
		Me.AboutToolStripMenuItem.Text = "About"
		'
		'frmBooking
		'
		Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
		Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
		Me.ClientSize = New System.Drawing.Size(1084, 361)
		Me.Controls.Add(Me.MenuStrip1)
		Me.Controls.Add(Me.btnLastBooking)
		Me.Controls.Add(Me.btnNextBooking)
		Me.Controls.Add(Me.btnPrevBooking)
		Me.Controls.Add(Me.btnFirstBooking)
		Me.Controls.Add(Me.btnDeleteBooking)
		Me.Controls.Add(Me.btnUpdateBooking)
		Me.Controls.Add(Me.btnFindBooking)
		Me.Controls.Add(Me.txtFindBooking)
		Me.Controls.Add(Me.btnGoToBooking)
		Me.Controls.Add(Me.btnGoToRoom)
		Me.Controls.Add(Me.btnGoToCustomer)
		Me.Controls.Add(Me.btnCloseBooking)
		Me.Controls.Add(Me.btnSubmitBooking)
		Me.Controls.Add(Me.btnNewBooking)
		Me.Controls.Add(Me.GroupBox1)
		Me.Name = "frmBooking"
		Me.Text = "Booking"
		Me.GroupBox1.ResumeLayout(False)
		Me.GroupBox1.PerformLayout()
		CType(Me.nudNumberOfDays, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.picErrorCheckinDate, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.picErrorNumOfGuests, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.picErrorRoomID, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.picErrorCustomerID, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.picErrorNumOfDays, System.ComponentModel.ISupportInitialize).EndInit()
		Me.MenuStrip1.ResumeLayout(False)
		Me.MenuStrip1.PerformLayout()
		Me.ResumeLayout(False)
		Me.PerformLayout()

	End Sub
	Friend WithEvents btnGoToRoom As System.Windows.Forms.Button
    Friend WithEvents btnGoToCustomer As System.Windows.Forms.Button
    Friend WithEvents btnCloseBooking As System.Windows.Forms.Button
    Friend WithEvents btnNewBooking As System.Windows.Forms.Button
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents cboCustomerID As System.Windows.Forms.ComboBox
    Friend WithEvents cboRoomID As System.Windows.Forms.ComboBox
    Friend WithEvents txtComments As System.Windows.Forms.TextBox
    Friend WithEvents txtNumberOfGuests As System.Windows.Forms.TextBox
	Friend WithEvents txtBookingID As System.Windows.Forms.TextBox
	Friend WithEvents lblComments As System.Windows.Forms.Label
	Friend WithEvents lblCheckinDate As System.Windows.Forms.Label
	Friend WithEvents lblNumberofGuests As System.Windows.Forms.Label
	Friend WithEvents lblNumberOfDays As System.Windows.Forms.Label
	Friend WithEvents lblCustomerID As System.Windows.Forms.Label
	Friend WithEvents lblRoomID As System.Windows.Forms.Label
	Friend WithEvents lblDate As System.Windows.Forms.Label
	Friend WithEvents lblBookingID As System.Windows.Forms.Label
	Friend WithEvents txtTotalPrice As System.Windows.Forms.TextBox
	Friend WithEvents lblTotalPrice As System.Windows.Forms.Label
	Friend WithEvents dtpBookingDate As System.Windows.Forms.DateTimePicker
	Friend WithEvents dtpCheckinDate As System.Windows.Forms.DateTimePicker
	Friend WithEvents lblDescription As System.Windows.Forms.Label
	Friend WithEvents picErrorCheckinDate As System.Windows.Forms.PictureBox
	Friend WithEvents picErrorNumOfGuests As System.Windows.Forms.PictureBox
	Friend WithEvents picErrorRoomID As System.Windows.Forms.PictureBox
	Friend WithEvents picErrorCustomerID As System.Windows.Forms.PictureBox
	Friend WithEvents picErrorNumOfDays As System.Windows.Forms.PictureBox
	Friend WithEvents btnSubmitBooking As System.Windows.Forms.Button
	Friend WithEvents btnGoToBooking As System.Windows.Forms.Button
	Friend WithEvents btnFindBooking As Button
	Friend WithEvents txtFindBooking As TextBox
	Friend WithEvents btnDeleteBooking As Button
	Friend WithEvents btnUpdateBooking As Button
	Friend WithEvents btnLastBooking As Button
	Friend WithEvents btnNextBooking As Button
	Friend WithEvents btnPrevBooking As Button
	Friend WithEvents btnFirstBooking As Button
	Friend WithEvents nudNumberOfDays As NumericUpDown
	Friend WithEvents MenuStrip1 As MenuStrip
	Friend WithEvents MenuToolStripMenuItem As ToolStripMenuItem
	Friend WithEvents CustomerToolStripMenuItem As ToolStripMenuItem
	Friend WithEvents RoomToolStripMenuItem As ToolStripMenuItem
	Friend WithEvents BookingToolStripMenuItem As ToolStripMenuItem
	Friend WithEvents ReportToolStripMenuItem As ToolStripMenuItem
	Friend WithEvents GenerateNormalReportToolStripMenuItem As ToolStripMenuItem
	Friend WithEvents GenerateBreakReportToolStripMenuItem As ToolStripMenuItem
	Friend WithEvents HelpToolStripMenuItem As ToolStripMenuItem
	Friend WithEvents AboutToolStripMenuItem As ToolStripMenuItem
End Class
