﻿Option Explicit On
Option Strict On

Imports System.Text.RegularExpressions

' Name:        Validation.vb
' Description: Validation method
' Author:      Do Khanh Hoang
' Date:        28/03/2017



Public Class Validation
    'Create a function called isNumericVal

    Public Function isNumericVal(ByVal strVal As String) As Boolean

        'Handling Exceptions
        Try
            Return IsNumeric(strVal)
        Catch ex As Exception
            Debug.Print("Error: " & ex.Message)
            Return False
        End Try
    End Function

    'Create a function called isAlphaNumericVal
    'Ref:   http://stackoverflow.com/questions/336210/regular-expression-for-alphanumeric-and-underscores

    Public Function isAlphaNumericVal(ByVal strVal As String) As Boolean
        'the range of meta character
        Dim pattern As Regex = New Regex("[^a-zA-Z0-9 -_]")

        'input field is at least one character long
        If strVal.Length > 0 Then
            Return Not pattern.IsMatch(strVal)
        Else
            Return False
        End If
    End Function
    'Validate email
    Function isValidEmail(ByVal email As String) As Boolean
        Dim emailRegex As New System.Text.RegularExpressions.Regex(
            "\S+@\S+\.\S+")
        Dim emailMatch As System.Text.RegularExpressions.Match =
           emailRegex.Match(email)
        Return emailMatch.Success
    End Function
	'Validate phone number
	Public Function IsPhoneNumberValid(ByVal Number As String) As Boolean
		' the range of character
		Dim pattern As Regex = New Regex("[^0-9 -_]")

		'input field is at least one character long and less than 11 characters
		If Number.Length > 0 And Number.Length < 11 Then
			Return Not pattern.IsMatch(Number)
		Else
			Return False
		End If
	End Function

	'Validate Year 
	Public Function IsYearValid(ByVal Year As String) As Boolean
		' the range of character
		Dim CharYear As Regex = New Regex("[^(19|20)\d{2}$]")

		'input field is equal to 4 character long
		If Year.Length = 4 Then
			Return Not CharYear.IsMatch(Year)
		Else
			Return False
		End If
	End Function
End Class
