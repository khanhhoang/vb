﻿Option Explicit On
Option Strict On

Imports System.Data.OleDb
Imports System.IO
Imports System.Drawing

' Name:        Booking.vb
' Description: Form for booking details
' Author:      Do Khanh Hoang
' Date:        28/03/2017


Public Class frmBooking

    'Connect to database
    Public Const CONNECTION_STRING As String = _
     "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=HRRISDB.accdb"

    'Record values form input fields to database
    Private Sub btnSubmitBooking_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSubmitBooking.Click
        Dim bIsValid = bValidateFormData()
        If bIsValid Then
            Dim htData As Hashtable = New Hashtable

            htData("Date") = dtpBookingDate.Text
            htData("RoomID") = cboRoomID.Text
            htData("CustomerID") = cboCustomerID.Text
			htData("NumberOfDays") = nudNumberOfDays.Text
			htData("NumberOfGuests") = txtNumberOfGuests.Text
            htData("CheckinDate") = dtpCheckinDate.Text
            htData("TotalPrice") = txtTotalPrice.Text
            htData("Comments") = txtComments.Text

            Dim oBookingController As BookingController = New BookingController
            oBookingController.insert(htData)
        End If

    End Sub

	'Validate input value whether it's correct or not
	'Create function to validate data form
	Private Function bValidateFormData() As Boolean
		Dim bValidation As New Validation
		Dim bIsValid As Boolean
		Dim bAllFieldsValid As Boolean
		bAllFieldsValid = True
		Dim tt As New ToolTip()

		'RoomID
		bIsValid = bValidation.isNumericVal(cboRoomID.Text)
		If bIsValid Then
			picErrorRoomID.Visible = False
		Else
			picErrorRoomID.Visible = True
			tt.SetToolTip(picErrorRoomID, "Please select the room ID")
			bAllFieldsValid = False
		End If

		'CustomerID
		bIsValid = bValidation.isNumericVal(cboCustomerID.Text)
		If bIsValid Then
			picErrorCustomerID.Visible = False
		Else
			picErrorCustomerID.Visible = True
			tt.SetToolTip(picErrorCustomerID, "Please select the customer ID")
			bAllFieldsValid = False
		End If

		'Number of Days
		bIsValid = bValidation.isNumericVal(nudNumberOfDays.Text)
		If bIsValid Then
			picErrorNumOfDays.Visible = False
		Else
			picErrorNumOfDays.Visible = True
			tt.SetToolTip(picErrorNumOfDays, "Please enter the number of days you wish to stay")
			bAllFieldsValid = False
		End If
		'Number of Guests
		bIsValid = bValidation.isNumericVal(txtNumberOfGuests.Text)
		If bIsValid Then
			picErrorNumOfGuests.Visible = False
		Else
			picErrorNumOfGuests.Visible = True
			tt.SetToolTip(picErrorNumOfGuests, "Please enter the number of guests")
			bAllFieldsValid = False
		End If
		'Checkin Date
		If dtpCheckinDate.Value > DateTime.Now Then
			bIsValid = True
			picErrorCheckinDate.Visible = False
		Else
			picErrorCheckinDate.Visible = True
			tt.SetToolTip(picErrorCheckinDate, "Your checkin date must be in future")
			bAllFieldsValid = False
		End If

		'All fields are valid
		If bAllFieldsValid Then
			Debug.Print("All fields are valid")
		Else
			MsgBox("One of the fields was invalid")
		End If

		Return bAllFieldsValid

	End Function
	'Load all data to form Room
	Private Sub frmRoom_Load(sender As Object, e As EventArgs) Handles MyBase.Load
		Dim oController As RoomController = New RoomController
		lsData = oController.findAll()

		Dim htData As Hashtable
		Dim iIndex As Integer

		If iCurrentIndex = 0 Then
			btnPrevBooking.Enabled = False
		Else
			btnPrevBooking.Enabled = True
		End If
		If iCurrentIndex = lsData.Count - 1 Then
			btnNextBooking.Enabled = False
		Else
			btnNextBooking.Enabled = True
		End If
		htData = lsData.Item(iIndex)

		populateBookingFields(lsData.Item(0))
	End Sub
	'Declare global variables
	Dim lsData As New List(Of Hashtable)
	Dim iCurrentIndex As Integer
	'Get data
	Private Function getFormData() As Hashtable
		Dim htData As Hashtable = New Hashtable

		htData("BookingID") = txtBookingID.Text
		htData("Date") = dtpBookingDate.Text
		htData("RoomID") = cboRoomID.Text
		htData("CustomerID") = cboCustomerID.Text
		htData("NumberOfDays") = nudNumberOfDays.Value
		htData("NumberOfGuests") = txtNumberOfGuests.Text
		htData("CheckinDate") = dtpCheckinDate.Text
		htData("TotalPrice") = txtTotalPrice.Text
		htData("Comments") = txtComments.Text

		Return htData
	End Function

	'Populate data to fields
	Private Sub populateBookingFields(htdata As Hashtable)
		txtBookingID.Text = CStr(htdata("BookingID"))
		dtpBookingDate.Text = CStr(htdata("Date"))
		cboRoomID.Text = CStr(htdata("RoomID"))
		cboCustomerID.Text = CStr(htdata("CustomerID"))
		nudNumberOfDays.Value = CDec(CStr(htdata("NumberOfDays")))
		txtNumberOfGuests.Text = CStr(htdata("NumberOfGuests"))
		dtpCheckinDate.Text = CStr(htdata("CheckinDate"))
		txtTotalPrice.Text = CStr(htdata("TotalPrice"))
		txtComments.Text = CStr(htdata("Comments"))
	End Sub
	'Calling customerID and roomID from customer and room table to booking form
	Private Sub frmBooking_Load(sender As Object, e As EventArgs) Handles MyBase.Load

		Dim oController As BookingController = New BookingController
		Dim lsRoomData = oController.callRoomID()
		Dim lsCustomerData = oController.callCustomerID()
		'call room_id
		For Each room_id In lsRoomData
			cboRoomID.Items.Add(room_id)
		Next
		'call customer_id
		For Each customer_id In lsCustomerData
			cboCustomerID.Items.Add(CStr(customer_id))
		Next

		'Load all data to form
		Dim lsData = oController.findAll()
		Dim htData As Hashtable
		Dim iIndex As Integer
		If iCurrentIndex = 0 Then
			btnPrevBooking.Enabled = False
		Else
			btnPrevBooking.Enabled = True
		End If
		If iCurrentIndex = lsData.Count - 1 Then
			btnNextBooking.Enabled = False
		Else
			btnNextBooking.Enabled = True
		End If
		htData = lsData.Item(iIndex)


		populateBookingFields(lsData.Item(1))
	End Sub
	'Go to first record
	Private Sub btnFirstBooking_Click(sender As Object, e As EventArgs) Handles btnFirstBooking.Click
		Dim htData As Hashtable
		Dim iIndex As Integer
		iIndex = 0
		iCurrentIndex = iIndex

		If iCurrentIndex = 0 Then
			btnPrevBooking.Enabled = False
		Else
			btnPrevBooking.Enabled = True
		End If
		If iCurrentIndex = lsData.Count - 1 Then
			btnNextBooking.Enabled = False
		Else
			btnNextBooking.Enabled = True
		End If
		htData = lsData.Item(iIndex)

		populateBookingFields(lsData.Item(iIndex))
	End Sub
	'Go to previous record
	Private Sub btnPrevBooking_Click(sender As Object, e As EventArgs) Handles btnPrevBooking.Click
		Dim htData As Hashtable
		Dim iIndex As Integer
		iIndex = iCurrentIndex - 1
		iCurrentIndex = iIndex

		If iCurrentIndex = 0 Then
			btnPrevBooking.Enabled = False
		Else
			btnPrevBooking.Enabled = True
		End If
		If iCurrentIndex = lsData.Count - 1 Then
			btnNextBooking.Enabled = False
		Else
			btnNextBooking.Enabled = True
		End If

		htData = lsData.Item(iIndex)

		populateBookingFields(lsData.Item(iIndex))
	End Sub
	'Go to next record
	Private Sub btnNextBooking_Click(sender As Object, e As EventArgs) Handles btnNextBooking.Click
		Dim htData As Hashtable
		Dim iIndex As Integer
		iIndex = iCurrentIndex + 1
		iCurrentIndex = iIndex

		If iCurrentIndex = 0 Then
			btnPrevBooking.Enabled = False
		Else
			btnPrevBooking.Enabled = True
		End If
		If iCurrentIndex = lsData.Count - 1 Then
			btnNextBooking.Enabled = False
		Else
			btnNextBooking.Enabled = True
		End If
		htData = lsData.Item(iIndex)

		populateBookingFields(lsData.Item(iIndex))
	End Sub
	'Go to last record
	Private Sub btnLastBooking_Click(sender As Object, e As EventArgs) Handles btnLastBooking.Click
		Dim htData As Hashtable
		Dim iIndex As Integer
		iIndex = lsData.Count - 1
		iCurrentIndex = iIndex

		If iCurrentIndex = 0 Then
			btnPrevBooking.Enabled = False
		Else
			btnPrevBooking.Enabled = True
		End If
		If iCurrentIndex = lsData.Count - 1 Then
			btnNextBooking.Enabled = False
		Else
			btnNextBooking.Enabled = True
		End If

		htData = lsData.Item(iIndex)

		populateBookingFields(lsData.Item(iIndex))
	End Sub
	'Calculate total price
	Private Sub nudNumberOfDays_TextChanged(sender As Object, e As EventArgs) Handles nudNumberOfDays.ValueChanged
		'Declare the variables
		Dim oBookingController As BookingController = New BookingController
		Dim iNumDays As Integer = CInt(nudNumberOfDays.Value)
		Dim lsRoomID = oBookingController.callRoomID
		Dim lsPrice = oBookingController.getPrice

		'Get Room ID from the database
		For Each room_id As String In lsRoomID
			For Each price As String In lsPrice
				txtTotalPrice.Text = FormatCurrency(iNumDays * CDbl(price))
			Next
		Next
	End Sub
	'Update
	Private Sub btnUpdate_Click(sender As Object, e As EventArgs) Handles btnUpdateBooking.Click
		Dim oController As BookingController = New BookingController
		Dim iNumRows = oController.update(getFormData)

		If iNumRows = 1 Then
			MsgBox("The record was updated!")
			Debug.Print("The record was updated!")
		Else
			MsgBox("The record was not updated!")
			Debug.Print("The record was not updated!")
		End If
	End Sub
	'Delete
	Private Sub btnDelete_Click(sender As Object, e As EventArgs) Handles btnDeleteBooking.Click
		If MsgBox("Are you sure to delete this record?", MsgBoxStyle.YesNo, "Delete Confirmation") = MsgBoxResult.Yes Then
			Dim oController As BookingController = New BookingController
			Dim sBId = txtBookingID.Text
			Dim sRId = cboRoomID.Text
			Dim sCId = cboCustomerID.Text
			Dim iNumRows = oController.delete(sBId, sRId, sCId)

			If iNumRows = 1 Then
				clearForm()
				MsgBox("The record was deleted")
				Debug.Print("The record was deleted. Use the find button to check")
			Else
				MsgBox("The record was not deleted!")
				Debug.Print("The record was not deleted!")
			End If

		ElseIf CBool(MsgBoxResult.No) Then
			Me.Refresh()
		End If
	End Sub
	'Find data by bookingID
	Private Sub btnRead_Click(sender As Object, e As EventArgs) Handles btnFindBooking.Click
		Dim oController As BookingController = New BookingController

		Dim sID = txtFindBooking.Text
		Dim lsData = oController.findByID(sID)
		If lsData.Count = 1 Then
			populateBookingFields(lsData.Item(0))
		Else
			MsgBox("No records were found!")
			Debug.Print("No records were found")
		End If
	End Sub

	'Clear total price when change roomid
	Private Sub cboRoomID_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboRoomID.SelectedIndexChanged
		txtTotalPrice.Text = ""
		nudNumberOfDays.Value = 0
	End Sub

	'Refresh booking form
	Private Sub clearBookingFields()
		txtBookingID.Text = ""
		dtpBookingDate.Text = CStr(Date.Today)
		cboRoomID.SelectedIndex = -1
		cboCustomerID.SelectedIndex = -1
		nudNumberOfDays.Value = 0
		txtNumberOfGuests.Text = ""
		dtpCheckinDate.Text = CStr(Date.Today)
		txtTotalPrice.Text = ""
		txtComments.Text = ""

		picErrorRoomID.Visible = False
		picErrorCustomerID.Visible = False
		picErrorNumOfDays.Visible = False
		picErrorNumOfGuests.Visible = False
		picErrorCheckinDate.Visible = False
	End Sub
	'Clear form
	Private Sub clearForm()
		txtBookingID.Clear()
		dtpBookingDate.Text = CStr(Date.Today)
		cboRoomID.SelectedIndex = -1
		cboCustomerID.SelectedIndex = -1
		nudNumberOfDays.Value = 0
		txtNumberOfGuests.Clear()
		dtpCheckinDate.Text = CStr(Date.Today)
		txtTotalPrice.Clear()
		txtComments.Clear()
	End Sub
	'Refresh booking form
	Private Sub btnNewBooking_Click(sender As Object, e As EventArgs) Handles btnNewBooking.Click
		clearBookingFields()
	End Sub
	'Close form
	Private Sub btnCloseBooking_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnCloseBooking.Click
		'Confirmation before exiting the system
		If MsgBox("Are you sure to close this form?", MsgBoxStyle.YesNo, "Exit Confirmation") = MsgBoxResult.Yes Then
			'Close the form
			Me.Close()
		ElseIf CBool(MsgBoxResult.No) Then
			Me.Show()
		End If
	End Sub
	'Navigation
	'Go to customer in Menu
	Private Sub CustomerToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles CustomerToolStripMenuItem.Click
		frmCustomer.Show()
		Me.Hide()
	End Sub
	'Go to room on Menu
	Private Sub RoomToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles RoomToolStripMenuItem.Click
		frmRoom.Show()
		Me.Hide()
	End Sub

	'Go to booking on Menu
	Private Sub BookingToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles BookingToolStripMenuItem.Click
		Me.Refresh()
	End Sub
	'About page
	Private Sub AboutToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles AboutToolStripMenuItem.Click
		Dim oController As PageController = New PageController
		oController.createAboutPage()
	End Sub
	'Help page
	Private Sub HelpToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles HelpToolStripMenuItem.Click
		Dim oController As PageController = New PageController
		oController.createHelpPage()
	End Sub
	'Go to Generate normal report
	Private Sub ReportGenerationToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles GenerateNormalReportToolStripMenuItem.Click
		ReportGeneration.Show()
		Me.Hide()
	End Sub
	'Go to Generate Break Report
	Private Sub GenerateBreakReportToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles GenerateBreakReportToolStripMenuItem.Click
		BreakReport.Show()
		Me.Hide()
	End Sub
End Class