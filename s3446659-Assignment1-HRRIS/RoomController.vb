﻿Option Explicit On
Option Strict On

Imports System.Data.OleDb


' Name:        RoomController.vb
' Description: Intermediate between VB and access (Room)
' Author:      Do Khanh Hoang
' Date:        28/03/2017



Public Class RoomController
    'Connect to database
    Public Const CONNECTION_STRING As String = _
     "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=HRRISDB.accdb"
	'Insert value
	Public Function insert(ByVal htData As Hashtable) As Integer

        Dim oConnection As OleDbConnection = New OleDbConnection(CONNECTION_STRING)
        Dim iNumRows As Integer

        Try
            Debug.Print("Connection string: " & oConnection.ConnectionString)

            oConnection.Open()
            Dim oCommand As OleDbCommand = New OleDbCommand
            oCommand.Connection = oConnection
            'Insert to database
            oCommand.CommandText = _
               "INSERT INTO room (room_number, type, price, num_beds, availability, floor, description) VALUES (?, ?, ?, ?, ?, ?, ?);"

            oCommand.Parameters.Add("RoomNumber", OleDbType.Integer, 10)
            oCommand.Parameters.Add("Type", OleDbType.VarChar, 255)
            oCommand.Parameters.Add("Price", OleDbType.Double, 8)
            oCommand.Parameters.Add("NumberOfBeds", OleDbType.Integer, 10)
            oCommand.Parameters.Add("Availability", OleDbType.VarChar, 255)
            oCommand.Parameters.Add("Floor", OleDbType.Integer, 50)
            oCommand.Parameters.Add("Description", OleDbType.VarChar, 255)

            'Define value type
            oCommand.Parameters("RoomNumber").Value = CInt(htData("RoomNumber"))
            oCommand.Parameters("Type").Value = CStr(htData("Type"))
            oCommand.Parameters("Price").Value = CInt(htData("Price"))
            oCommand.Parameters("NumberOfBeds").Value = CInt(htData("NumberOfBeds"))
            oCommand.Parameters("Availability").Value = CStr(htData("Availability"))
            oCommand.Parameters("Floor").Value = CInt(htData("Floor"))
            oCommand.Parameters("Description").Value = CStr(htData("Description"))

            oCommand.Prepare()

            iNumRows = oCommand.ExecuteNonQuery()
            Debug.Print(CStr(iNumRows))

            Debug.Print("SQL: " & oCommand.CommandText)

            Debug.Print("The record was inserted.")
            MsgBox("The record was inserted")
        Catch ex As Exception
            Debug.Print("ERROR: " & ex.Message)
            MsgBox("An error occured. The record wasn't inserted.")
        Finally
            oConnection.Close()
        End Try

        Return iNumRows


	End Function
	'Find all
	Public Function findAll() As List(Of Hashtable)

		Dim oConnection As OleDbConnection = New OleDbConnection(CONNECTION_STRING)
		Dim lsData1 As New List(Of Hashtable)

		Try
			Debug.Print("Connection string: " & oConnection.ConnectionString)

			oConnection.Open()
			Dim oCommand As OleDbCommand = New OleDbCommand
			oCommand.Connection = oConnection

			oCommand.CommandText =
				"SELECT * FROM room ORDER BY room_id;"
			oCommand.Prepare()
			Dim oDataReader = oCommand.ExecuteReader()

			Dim htTempData As Hashtable
			Do While oDataReader.Read() = True
				htTempData = New Hashtable
				htTempData("RoomID") = CStr(oDataReader("room_id"))
				htTempData("Type") = CStr(oDataReader("type"))
				htTempData("RoomNumber") = CStr(oDataReader("room_number"))
				htTempData("NumberOfBeds") = CStr(oDataReader("num_beds"))
				htTempData("Floor") = CStr(oDataReader("floor"))
				htTempData("Price") = CStr(oDataReader("price"))
				htTempData("Availability") = CStr(oDataReader("availability"))
				htTempData("Description") = CStr(oDataReader("description"))
				lsData1.Add(htTempData)
			Loop

			Debug.Print("The records was found.")

		Catch ex As Exception
			Debug.Print("ERROR: " & ex.Message)
			MsgBox("An error occurred. The records could not be found!")
		Finally
			oConnection.Close()
		End Try

		Return lsData1

	End Function
	'Find by ID
	Public Function findByID(sID As String) As List(Of Hashtable)

		Dim oConnection As OleDbConnection = New OleDbConnection(CONNECTION_STRING)
		Dim lsData1 As New List(Of Hashtable)

		Try
			Debug.Print("Connection string: " & oConnection.ConnectionString)

			oConnection.Open()
			Dim oCommand As OleDbCommand = New OleDbCommand
			oCommand.Connection = oConnection

			oCommand.CommandText =
				"SELECT * FROM room WHERE room_id = ?;"
			oCommand.Parameters.Add("room_id", OleDbType.Integer, 8)
			oCommand.Parameters("room_id").Value = CInt(sID)
			oCommand.Prepare()
			Dim oDataReader = oCommand.ExecuteReader()

			Dim htTempData As Hashtable
			Do While oDataReader.Read() = True
				htTempData = New Hashtable
				htTempData("RoomID") = CStr(oDataReader("room_id"))
				htTempData("Type") = CStr(oDataReader("type"))
				htTempData("RoomNumber") = CStr(oDataReader("room_number"))
				htTempData("NumberOfBeds") = CStr(oDataReader("num_beds"))
				htTempData("Floor") = CStr(oDataReader("floor"))
				htTempData("Price") = CStr(oDataReader("price"))
				htTempData("Availability") = CStr(oDataReader("availability"))
				htTempData("Description") = CStr(oDataReader("description"))
				lsData1.Add(htTempData)
			Loop

			Debug.Print("Read the record successfully")

		Catch ex As Exception
			Debug.Print("ERROR: " & ex.Message)
			MsgBox("No records were found!")
		Finally
			oConnection.Close()
		End Try

		Return lsData1

	End Function
	'Update Function
	Public Function update(ByVal htData As Hashtable) As Integer

		Dim oConnection As OleDbConnection = New OleDbConnection(CONNECTION_STRING)
		Dim iNumRows As Integer

		Try
			Debug.Print("Connection string: " & oConnection.ConnectionString)

			oConnection.Open()
			Dim oCommand As OleDbCommand = New OleDbCommand
			oCommand.Connection = oConnection

			oCommand.CommandText =
				"UPDATE room SET type = ?, room_number = ?, price = ?, num_beds = ?, availability = ?, floor = ?, description = ? WHERE room_id = ?;"

			oCommand.Parameters.Add("Type", OleDbType.VarChar, 255)
			oCommand.Parameters.Add("RoomNumber", OleDbType.Integer, 10)
			oCommand.Parameters.Add("Price", OleDbType.Integer, 10)
			oCommand.Parameters.Add("NumberOfBeds", OleDbType.Integer, 10)
			oCommand.Parameters.Add("Availability", OleDbType.VarChar, 255)
			oCommand.Parameters.Add("Floor", OleDbType.Integer, 10)
			oCommand.Parameters.Add("Description", OleDbType.VarChar, 255)
			oCommand.Parameters.Add("RoomID", OleDbType.Integer, 1)


			'Define value type
			oCommand.Parameters("Type").Value = CStr(htData("Type"))
			oCommand.Parameters("RoomNumber").Value = CInt(htData("RoomNumber"))
			oCommand.Parameters("Price").Value = CInt(htData("Price"))
			oCommand.Parameters("NumberOfBeds").Value = CInt(htData("NumberOfBeds"))
			oCommand.Parameters("Availability").Value = CStr(htData("Availability"))
			oCommand.Parameters("Floor").Value = CInt(htData("Floor"))
			oCommand.Parameters("Description").Value = CStr(htData("Description"))
			oCommand.Parameters("RoomID").Value = CInt(htData("RoomID"))

			oCommand.Prepare()
			iNumRows = oCommand.ExecuteNonQuery()

			Debug.Print(CStr(iNumRows))
			Debug.Print("The record was updated.")

		Catch ex As Exception
			Debug.Print("ERROR: " & ex.Message)
			MsgBox("An error occurred. The record was not updated!")
		Finally
			oConnection.Close()
		End Try

		Return iNumRows
	End Function
	'Delete function
	Public Function delete(sId As String) As Integer
		Dim oConnection As OleDbConnection = New OleDbConnection(CONNECTION_STRING)
		Dim iNumRows As Integer

		Try
			Debug.Print("Connection string: " & oConnection.ConnectionString)

			oConnection.Open()
			Dim oCommand As OleDbCommand = New OleDbCommand

			oCommand.Connection = oConnection
			oCommand.CommandText =
			   "DELETE FROM booking WHERE room_id = ?;"
			oCommand.Parameters.Add("RoomID", OleDbType.Integer, 8)
			oCommand.Parameters("RoomID").Value = CInt(sId)
			oCommand.Prepare()
			iNumRows = oCommand.ExecuteNonQuery()

			oCommand.Connection = oConnection
			oCommand.CommandText =
			   "DELETE FROM room WHERE room_id = ?;"
			oCommand.Parameters.Add("RoomID", OleDbType.Integer, 8)
			oCommand.Parameters("RoomID").Value = CInt(sId)
			oCommand.Prepare()
			iNumRows = oCommand.ExecuteNonQuery()

			Debug.Print(CStr(iNumRows))
			Debug.Print("The record was deleted.")

		Catch ex As Exception
			Debug.Print("ERROR: " & ex.Message)
		Finally
			oConnection.Close()
		End Try

		Return iNumRows

	End Function
End Class

