﻿Option Explicit On
Option Strict On

Imports System.IO
Imports System.Data.OleDb

' Name:        PageController.vb
' Description: Class 
' Author:      Do Khanh Hoang
' Date:        28/04/2017

Public Class PageController
	'About Page
	Private Function AboutPage(ByVal sReportTitle As String) _
As String

		Debug.Print("AboutPage")

		Dim sPageContent As String

		' 1. Generate the start of the HTML file
		Dim sDoctype As String = "<!DOCTYPE html>"
		Dim sHtmlStartTag As String = "<html lang=""en"">"
		Dim sHeadTitle As String = "<head><title> About </title></head>"
		Dim sBodyStartTag As String = "<body>"
		Dim sReportHeading As String = "<p><strong>Student Name:</strong> Do Khanh Hoang<br>" &
										"<strong>Student number:</strong> s3446659 <br>" &
										"<strong>Semester:</strong> A2017<br>" &
										"<strong>Subject:</strong> ISYS2116: Information System Solutions and Design<br>" &
										"<strong>Tutor:</strong> Ashish Das - The most handsome, friendly and funny tutor I've ever seen<br>" &
										"<strong>Description:</strong>HRRIS assignment 2</p>"


		sPageContent = sDoctype & Environment.NewLine & sHtmlStartTag & Environment.NewLine _
		  & sHeadTitle & Environment.NewLine & sBodyStartTag & Environment.NewLine & sReportHeading & Environment.NewLine

		' 2. Generate the end of the HTML file
		Dim sBodyEndTag As String = "</body>"
		Dim sHTMLEndTag As String = "</html>"
		sPageContent &= sBodyEndTag & Environment.NewLine & sHTMLEndTag

		Return sPageContent

	End Function
	'Create about page
	Public Sub createAboutPage()
		Debug.Print("About Page")

		Dim sReportTitle = "About Page"
		Dim sReportFilename = "AboutPage.html"
		Dim sReportContent = AboutPage(sReportTitle)
		saveAboutPage(CStr(sReportContent), sReportFilename)
		Dim sParam As String = """" & Application.StartupPath & "\" & sReportFilename & """"
		Debug.Print("sParam: " & sParam)
		System.Diagnostics.Process.Start(sParam)
	End Sub
	'Help Page
	Private Function HelpPage(ByVal sReportTitle As String) _
As String

		Debug.Print("HelpPage")

		Dim sPageContent As String

		' 1. Generate the start of the HTML file
		Dim sDoctype As String = "<!DOCTYPE html>"
		Dim sHtmlStartTag As String = "<html lang=""en"">"
		Dim sHeadTitle As String = "<head><title> Help </title></head>"
		Dim sBodyStartTag As String = "<body>"
		Dim sReportHeading As String = "<h1>Screen Mockup</h1><br>" &
									   "<img src=""pic/Booking.png"">" &
									   "<img src=""pic/Customer.png"">" &
									   "<img src=""pic/Room.png"">" &
									   "<img src=""pic/GenerateNormalReport.png"">" &
									   "<img src=""pic/BreakReport.png"">" &
									   "<h1>ERD</h1><br>" &
									   "<img src=""img/ERD.png"">"



		sPageContent = sDoctype & Environment.NewLine & sHtmlStartTag & Environment.NewLine _
		  & sHeadTitle & Environment.NewLine & sBodyStartTag & Environment.NewLine & sReportHeading & Environment.NewLine

		' 2. Generate the end of the HTML file
		Dim sBodyEndTag As String = "</body>"
		Dim sHTMLEndTag As String = "</html>"
		sPageContent &= sBodyEndTag & Environment.NewLine & sHTMLEndTag

		Return sPageContent

	End Function
	'Create about page
	Public Sub createHelpPage()
		Debug.Print("Help Page")

		Dim sReportTitle = "Help Page"
		Dim sReportFilename = "HelpPage.html"
		Dim sReportContent = HelpPage(sReportTitle)
		saveAboutPage(CStr(sReportContent), sReportFilename)
		Dim sParam As String = """" & Application.StartupPath & "\" & sReportFilename & """"
		Debug.Print("sParam: " & sParam)
		System.Diagnostics.Process.Start(sParam)
	End Sub

	'Save Page
	Private Sub saveAboutPage(ByVal sReportContent As String, ByVal sReportFilename As String)
		Debug.Print("saveReport: " & sReportFilename)
		Dim oReportFile As StreamWriter = New StreamWriter(Application.StartupPath & "\" & sReportFilename)

		' Check if the file is open before starting to write to it
		If Not (oReportFile Is Nothing) Then
			oReportFile.Write(sReportContent)
			oReportFile.Close()
		End If
	End Sub
End Class
